import os, errno

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
#######____ Navigation Utilities ____#######
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

def create_folder(path):
    """
        Creates a directory at 'path'. Does nothing if it doesn't exist.
        :param path: directory name
    """

    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def enter_folder(path):
    """
        Enters a folder. Creates one if it doesn't exist.
        :param path: directory to navigate to
    """
    # Create directory if it doesn't exist
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    # Move to directory
    os.chdir(path)


def exit_folder():
    """
        Exits a folder.
    """
    os.chdir("..")