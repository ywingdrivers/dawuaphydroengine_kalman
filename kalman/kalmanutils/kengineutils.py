import numpy as np
import utils

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
#######____ Noise Utilities ____#######
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

def add_noise_to_array(rand_function, arr, filter_unknowns=False, clip=False, **kwargs):
    """
        Adds noise to a given numpy array using callback rand_function.

        :param rand_function: callback to add noise. Currently set up for np.random.normal().
        :param arr: array that noise shall be added to.
        :param filter_unknowns: Preserve unknown values. If not set to 'False', function will mask out the value set to this variable. Examples: -32767, np.nan, etc.
        :param clip: Clips values below whatever number this is set to. Default is False.
        :param **kwargs: These are passed into rand_function along with 'shape' and 'loc', which are hardcoded into the function.
        :return: Returns the array plus white noise.
    """
    if filter_unknowns != False:
        if isinstance(arr,np.ma.MaskedArray):
            unknown_indexes = np.ma.where(arr == filter_unknowns)[0]
        else:
            unknown_indexes = np.where(arr == filter_unknowns)[0]

    if isinstance(arr,np.ma.MaskedArray):
        result_arr = arr
        result_arr[~arr.mask] = rand_function(loc=arr.compressed(), size=arr.compressed().shape, **kwargs)
    else:
        result_arr = rand_function(loc=arr, size=arr.shape, **kwargs)

    if clip != False:
        result_arr = result_arr.clip(min=clip)

    if filter_unknowns != False:
        np.put(result_arr, unknown_indexes, filter_unknowns)

    return result_arr

def add_noise_to_array_instances(rand_function, arr, instances, filter_unknowns=False, clip=False, **kwargs):
    """
    Adds noise to an array via rand_function and broadcasts that over an array of instances.
    Returns in the order: my_array[instances,originalarray]
    :param rand_function: Random function, usually np.random.normal
    :param arr: 1 dimensional array of instances. Ex: [4,5,6,6,-1]
    :param instances: Number of perturbed arrays to return (# of rows)
    :param filter_unknowns: Preserve unknown values. If not set to 'False', function will mask out the value set to this variable. Default -32767
    :param clip: Clips values below whatever number this is set to. Default is False.
    :param **kwargs: These are passed into rand_function along with 'shape' and 'loc', which are hardcoded into the function.
    :return: A 2 dimensional array of perturbed 1D arrays.
    """
    if filter_unknowns != False:
        unknown_indexes = np.where(arr == filter_unknowns)[0]

    result_arr = rand_function(loc=arr[:, np.newaxis], size=(arr.size, instances), **kwargs)

    if clip != False:
        result_arr = result_arr.clip(min=clip)

    if filter_unknowns != False:
        # for i in range(instances):
        #     np.put(result_arr[:,i], unknown_indexes, filter_unknowns)
        result_arr[unknown_indexes,:] = filter_unknowns

    return result_arr

def add_scalar_to_array(rand_function, arr, clip=False, filter_unknowns=False, **kwargs):
    """
    :param rand_function: random function (currently tested with np.random.normal)
    :param arr: Array to add scalar to
    :param v: Variance
    :param clip: Clips values below whatever number this is set to. Default is False.
    :param filter_unknowns: Preserve unknown values. If not set to 'False', function will mask out the value set to this variable (ex. -32767). Default False.
    :param **kwargs: These are passed into rand_function along with 'shape' and 'loc', which are hardcoded into the function.
    :return: Modified numpy array

    Adds a randomly generated scalar to all elements of the given array
    """
    if filter_unknowns != False:
        unknown_indexes = np.where(arr == filter_unknowns)[0]

    rand_num = rand_function(loc=0, **kwargs)
    result_arr = arr + rand_num


    if clip != False:
        result_arr = arr.clip(min=clip)

    if filter_unknowns != False:
        np.put(result_arr, unknown_indexes, filter_unknowns)

    return result_arr

def find_nearest(arr, val, return_index = False):
    """
    Find the closest value in an array.
    :param arr: Array to search
    :param val: Value that should be the closest
    :param return_index: Return index of closest as a second variable. Default false.
    :return: The closest value
    """
    arr = np.asarray(arr)
    i = (np.abs(arr - val)).argmin()
    if return_index:
        return arr[i], i
    else:
        return arr[i]