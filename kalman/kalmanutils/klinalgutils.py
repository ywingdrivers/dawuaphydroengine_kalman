import numpy as np
# from numba import jit

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
#######____ Covariance Utilities ____#######
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

# @jit(nopython=True, parallel=True)
def cross_cov(arr1, arr2):
    """
    Calculates the cross covariance of two matricies.


    :param arr1: First array. Dimensions should be arr1[i,j]
    :param arr2: Second array. Dimensions should be arr2[i,k]
    :return: Numpy array
    """
    return np.dot((arr1 - arr1.mean(axis=0)).T, arr2 - arr2.mean(axis=0)) / (arr1.shape[0] - 1)

# @jit(nopython=True, parallel=True)
def cross_cov_sum(arr1, arr2):
    """
    Calculates the cross covariance of two matricies.

        {{{CHECK TO SEE IF THIS IS ACCURATE/FASTER}}}

    :param arr1: First array. Dimensions should be arr1[i,j]
    :param arr2: Second array. Dimensions should be arr2[i,k]
    :return: Numpy array
    """

    meanX = np.mean(arr1, axis=0)
    meanY = np.mean(arr2, axis=0)

    final_arr = np.zeros((arr1.shape[1], arr2.shape[1]))

    for i in range(arr1.shape[0]):
        X = np.array(arr1[i, :] - meanX)
        Y = np.array(arr2[i, :] - meanY)

        # final_arr += np.dot(X[:, np.newaxis], Y[np.newaxis,:])
        final_arr = final_arr + np.outer(X, Y)

    return final_arr / (arr1.shape[0] - 1)

# def cross_cov_individual(arr1, arr2, i):
#     """
#         Calculates cross covariance - but with respect to only one instance.
#         Expected values are still generated from ensemble.
#
#             CURRENTLY NOT USED.
#
#         :param arr1: First array. Dimensions should be arr1[i,j]
#         :param arr2: Second array. Dimensions should be arr2[i,k]
#         :param i: Specific member of ensemble to generate cross_cov matrix for.
#         :return: Numpy array
#     """
#
#     meanX = np.mean(arr1, axis=0)
#     meanY = np.mean(arr2, axis=0)
#
#     X = np.array(arr1[i,:] - meanX)
#     Y = np.array(arr2[i,:] - meanY)
#
#     return np.dot(X[:, np.newaxis], Y[np.newaxis,:]) / (arr1.shape[0] - 1)
#
#     # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
#     #######____ Kalman Gain Utilities ____#######
#     # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#


def calculate_K_matrix(state_cov, prediction_cov, R_error):

    """
    Calculate a Kalamn Gain matrix.
    :param state_cov: Covariance of state and prediction
    :param prediction_cov: Covariance of prediction
    :param R_error: Observation error
    :return: Nothing
    """

    # R_matrix = np.diag(np.full_like(prediction_cov[:,0], R_error))
    try:
        R_matrix = np.identity(prediction_cov.shape[0]) * R_error # Create a diagonal matrix with R as the variable
    except IndexError: # np.cov() returned one number for the covariance. This handles that possiblity.
        R_matrix = np.identity(1)

    state_cov = state_cov

    return state_cov.dot(np.linalg.inv(prediction_cov + R_matrix))
