#from __future__ import print_function
import numpy as np
import json, utils, argparse
from modifyParamRaster import Modify_Param_Raster

class Modify_Param_Vehicle():
    """
        Main class, contains methods to modify different geotiffs used
        by the model.
    """

    def __init__(self):
        self.stdDict = {}
        self.geotiff = None
        self.constant = 0

    def _get_json_data_(self, filename):
        """
            Grab JSON file and return it as a dictonary
            :param filename: file name
            :return: None
        """

        #Read JSON data into the self.json_data
        if filename:
            with open(filename, 'r') as f:
                json_data = json.load(f)

        return json_data

    def _get_list_stds_(self, data):
        """
            Returns the standard deviation of a list
            Uses matrix Q (currently 1x1) as standard deviation.
            For example problem.

            :param std: Standard Deviation
            :return: standard
        """
        self.stdDict = {}

        for key, node in data.iteritems():

            # Put everything in a list
            valueList = []
            for value in node['values'].itervalues():
                valueList.append(value['values'])

            std = np.std(valueList)
            mean = np.mean(valueList)

            nodeInfo = {'std': std,
                        'mean': mean}

            self.stdDict[key] = nodeInfo

        return self.stdDict

    def _apply_std_vals_(self, node_name, x, y):
        """
            Returns the STD specified in self.stdDict to a given node name.
            Meant to be passed to Modify_Param_Raster.modify_raster() as callback.
            Returns 0 if random number is below 0
            USED FOR build_swe_std_raster
                :param node_name: Node name
                :param x: not used
                :param y: not used
                :return: std from self.stdDict
        """

        std = self.stdDict[str(int(node_name))]['std']
        mean = self.stdDict[str(int(node_name))]['mean']

        return max(0, np.random.normal(mean, std))

    def _apply_difference_(self, value, x, y):
        """
            Returns absolute value of difference between two numbers.
            CALLBACK FOR build_raster_difference
            Other value is pulled from a second geotiff array at self.geotiff.
            :param value: value 1
            :param x: x array col
            :param y: array row
            :return: difference between self.geotiff and value
        """
        return abs(self.geotiff[0,x,y] - value)

    def _constant_val_(self, value, x, y):
        """
            Applies one value per pixel regardless of initial value.
            Callback for build_raster_constant
            :param value: value (not used)
            :param x: x array col (not used)
            :param y: array row (not used)
            :return: a constant value defined in self.constant
        """
        return self.constant

    def build_swe_std_raster(self, std_json_file = "data/swejson.txt", filename_in="data/sweObsRaster.tif", filename_out="data/vehicleRaster.tif"):
        """
            Takes a series of lists of values in JSON format, parses them by key,
            finds the mean and standard devation, and uses it to build a raster
            containing a random number at each correspoinding 'key' in a raster file.
            :param std_json_file: JSON file of keys containing array at x['value']
            :param filename_in: 2d array file with keys mapped to different pixels
            :param filename_out: name/location for output file.
            :return: Outputted raster array, for debugging
        """
        std_json = self._get_json_data_(std_json_file)
        std_vals = self._get_list_stds_(std_json)

        # Take JSON data and use it to modify RASTERIO file
        mpr = Modify_Param_Raster()

        return mpr.modify_raster(filename_in,filename_out,self._apply_std_vals_)

    def build_raster_difference(self, filename_in_1, filename_in_2, filename_out):
        """
            Takes the difference between the values in two different geotiffs.
            Both geotiffs (obviously) should be have the same size and coordinates.
            All numbers are positive. Useful for checking error.
            :param filename_in: 2d array file with keys mapped to different pixels
            :param filename_out: name/location for output file.
            :return: Outputted raster array, for debugging
        """
        raster_1 = utils.RasterParameterIO(filename_in_1)
        self.geotiff = raster_1.array

        mpr = Modify_Param_Raster()

        return mpr.modify_raster(filename_in_2,filename_out,self._apply_difference_)

    def build_raster_constant(self, filename_in, filename_out, constant=0):
        """
            Takes the difference between the values in two different geotiffs.
            Both geotiffs (obviously) should be have the same size and coordinates.
            All numbers are positive. Useful for checking error.
            :param filename_in: 2d array file with keys mapped to different pixels
            :param filename_out: name/location for output file.
            :return: Outputted raster array, for debugging
        """
        self.constant = constant

        mpr = Modify_Param_Raster()

        return mpr.modify_raster(filename_in,filename_out,self._constant_val_)

def main(args):
    vehicle = Modify_Param_Vehicle()

    if args.function == "const":
        # Param will be a constant value
        vehicle.build_raster_constant(args.file_in, args.file_out, constant=args.param)

    else:
        print "Function not known"

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Raster Modification Engine')
    parser.add_argument('function', help='Raster modification to run - const, difference, swe_std')
    parser.add_argument('file_in', help='File to modify')
    parser.add_argument('file_out', help='Output file')
    parser.add_argument('param', help="Parameter for raster function")
    args = parser.parse_args()
    main(args)
