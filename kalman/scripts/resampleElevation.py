import numpy as np
import utils

arr = utils.RasterParameterIO("data/MTDEM_filled.tif").array
other_arr = utils.RasterParameterIO("data/constant_6.tif")
arr[arr == -99999] = np.NaN
print arr