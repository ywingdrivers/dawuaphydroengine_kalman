# modifyParamRaster.py
#########
# General class that applies a function to different raster regions of a file
# and can output stuff
#########
# 2/6/2018
import numpy as np
import utils

class Modify_Param_Raster():
    """
        General class that applies a function to a raster file.
    """

    def modify_raster(self, from_filename, to_filename, callback):
        """
            Choose a new filename to grab observation data from.
            :param from_filename: name/location of .tif file to modify
            :param to_filename: name/location of final .tif file
            :param callback: function that takes a raster value, x, and y and returns a number based on them.
            :return: Raster array
        """
        raster_template = utils.RasterParameterIO(from_filename)
        raster_template_data = raster_template.array

        new_raster = np.zeros_like(raster_template_data[0,:,:])

        # Cycle through points, apply callback to values
        for (x,y), value in np.ndenumerate(new_raster):
            new_raster[x,y] = callback(raster_template_data[0,x,y], x,y)

        # Build new geotiff from array
        raster_template.write_array_to_geotiff(to_filename, new_raster)

        return new_raster


    # def modify_raster_from_2d_to_3d(self, from_filename, to_filename, callback):
    #     """
    #         Create a 3D raster object. Apply a different
    #         :param from_filename: name/location of (2D) .tif file to modify
    #         :param to_filename: name/location of final .tif file
    #         :param callback: function that takes a raster value and time index (0-364) and returns a number based on it.
    #         :return: Raster array
    #     """
    #     raster_template = utils.RasterParameterIO(from_filename)
    #     raster_template_data = raster_template.array
    #
    #     new_raster = np.zeros_like(raster_template_data[365,:,:])
    #
    #     # Cycle through points, apply callback to values
    #     for (x,y), value in np.ndenumerate(new_raster):
    #         new_raster[x,y] = callback(raster_template_data[0,x,y])
    #
    #     # Build new geotiff from array
    #     raster_template.write_array_to_geotiff(to_filename, new_raster)
    #
    #     return new_raster
