# partitionSweNodes.py
###
# Building a partition series of polygon for an arbitrary serieson of
# obersvation SWE sites. Takes a JSON file as created in HydroModelBrowser.py
# "script to allow us to create a script to subscript any arbitrary function" ??
# 1-15-2018
###
import math, json
import utils
import numpy as np

class Create_THIESSEN_Raster_Regions():
        """
            Creates a geotiff raster based on a series of JSON nodes.
            Each node represents the 'center' of a section of the geotiff.
            Currently calculates the areas of influence via 2-dimensional
            euclidean distance.
        """

        def __init__(self,
                     filename='sweJSON',
                     precip_size_file = 'precip_template.nc'):
            """
            :param filename: File name
            :param precip_size_file: Pre-existing file used to generate raster size and affine.
            """

            self.set_filename(filename)

            # Change this variable to shorten all node's 'areas of influence'.
            # Small values could potentially lead to null spaces (0's) in the geotiff.
            self.maxInfluence = 100 # max influence in lat/long distance unit.

            self.pp = utils.RasterParameterIO(precip_size_file)
            self.pp_affine = self.pp.transform
            self.pp_data = self.pp.array.clip(min=0)

        def set_filename(self, filename):
            """
                Choose a new filename to grab observation data from.
                :param filename: file name
                :return: None
            """
            #Read JSON data into the self.json_data
            if filename:
                with open(filename, 'r') as f:
                    self.json_data = json.load(f)

                    # Remove unnessesary information
                    for node in self.json_data:
                        del self.json_data[node]['values']

        def build_raster(self, filename="sweObsRaster.tif"):
            """
                Generates a raster based on the 'area of influece' of different nodes
                :param filename: Name for output raster file.
                :return: Numpy array of raster values. Used for testing.
            """

            thiessen_poly = np.zeros_like(self.pp_data[0, :, :])

            ### DO THINGS TO GET CORRECT NUMPY STATS! ###

            lon, lat = self.pp_affine * np.indices(self.pp_data[0, :, :].T.shape)

            lat = lat.T
            lon = lon.T

            # Cycle through points
            for (x,y), value in np.ndenumerate(thiessen_poly):

                pixel_position = {'lat': lat[x,y], 'long': lon[x,y]}
                thiessen_poly[x,y] = self._get_closest_node_name_(pixel_position)

            self.pp.write_array_to_geotiff(filename, thiessen_poly)

            return thiessen_poly

        # Modified from https://stackoverflow.com/questions/5407969/distance-formula-between-two-points-in-a-list
        def _compute_distance_(self, node1, node2):
            """
                General distance formula. Returns distance
                Maybe should be upgraded with more advanced 'geologic' formula soon.
                :param node1: Dictionary with 'lat' and 'long' entries
                :param node2: Dictionary with 'lat' and 'long' entries
            """
            return math.sqrt((node1['lat'] - node2['lat'])**2 + (node1['long'] - node2['long'])**2)


        def _get_closest_node_name_(self, node):
            """
                Returns name of the closest node using self.json_data
                :param node: Dictonary with 'lat' and 'long' entries
                :return: name of closest node
            """
            # Set initial varaibles
            shortest_dist = self.maxInfluence
            node_name = "NULL"
            for key, value in self.json_data.iteritems():

                distance = self._compute_distance_(node, value)

                if distance < shortest_dist:
                    shortest_dist = distance
                    node_name = key

            return node_name
