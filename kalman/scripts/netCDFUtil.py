# modifyParamRaster.py
#########
# General class that applies a function to different raster regions of a file
# and can output stuff
#########
# 2/6/2018
import numpy as np
import utils
from netCDF4 import Dataset

class NetCDF_Util():
    """
        Takes a rasterIO object and allows it to be written to disk as a NetCDF.
    """

    def __init__(self, path):
        self.rootgrp = Dataset(path, "r", format="NETCDF4")
        print "Yes"
        for dimobj in self.rootgrp.dimensions.values():
            print dimobj
        print self.rootgrp['day'][0]

    def split_netcdf(self):
        print "UNN:NG"
