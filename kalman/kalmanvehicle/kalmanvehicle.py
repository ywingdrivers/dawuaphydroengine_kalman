#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#
# kalmanvehicle.py
#
# Script that applies dualensemblekalmanfilter.py to the dawuahydroengine model
# Main script run by kftest.py
# -----------------------
# William Cook July 16th, 2017
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#

from __future__ import division
import datetime
from dateutil.parser import parse
import utils
import hydrovehicle
import json
import cPickle as pickle
import numpy as np, numpy.ma as ma
from kalman import kalmanutils as kutils
from kalman import Dual_Ensemble_Kalman_Filter
from kalman import kalmangraphs
from kalman import Kalman_NRR
from kgetterssetters import Dawuaphydroengine_Kalman_Getters_And_Setters
import kvehicleutils as kvutils

def main(param_config_file, uparam_config_file, obs_config_file, init_date, timesteps, data_folder="kalman_data/", run_state_correction=True, nrr_module_active=False, create_graphs=True, debug=False, **kwargs):

        ####>>>>>> PREPARE FILES <<<<<<####

        # Link to hydrovehicle
        f_hydrovehicle = hydrovehicle.main

        kutils.enter_folder(data_folder)

        ## Parameters ##
        theta_param_dict = {} # Get the initial theta parameter values - geoTIFF files to numpy arrays
        par_states = {} # The name of each parameter's associated states
        rasterio_template_file = "" # Random RasterIO file that will be used as a template

        with open(param_config_file) as json_file:
            par_files = json.load(json_file)
            for key, value in par_files.items():
                theta_param_dict[key] = utils.RasterParameterIO(value[0], 1).array.flatten()
                par_states[key] = value[1]
                rasterio_template_file = value[0]

        ## U_Vars ##
        uparam_dict = {}

        with open(uparam_config_file) as json_file: # Get the initial uparam values - netCDF files to numpy arrays
            uparam_files = json.load(json_file)
            for key, value in uparam_files.items():
                netCDF = utils.RasterParameterIO(value)
                uparam_dict[key] = netCDF.array

        # We need an initial dictionary of our u parameters to pass to the model
        initial_uparam_dict = {}
        for key, array in uparam_dict.iteritems():
            initial_uparam_dict[key] = uparam_dict[key][0,:,:].flatten()

        ## Observations ##
        obs_dict = {} # Observed values
        obs_pos_dict = {} # Each node's lng/lat

        with open(obs_config_file) as json_file: # Get the initial obs values - JSON files
            obs_files = json.load(json_file)

        # Parse SWE data with our custom parser in the kvutils library.
        with open(obs_files["swe"], 'r') as f:
            swe_obs_json = json.load(f)
            swe_arr, long_lat_swe_arr, swe_name_arr = kvutils.swe_parser(swe_obs_json, init_date, timesteps)
            obs_dict["swe"] = swe_arr
            obs_pos_dict["swe"] = long_lat_swe_arr

        # Parse streamflow data
        with open(obs_files["streamflow"], 'r') as f:
            sf_obs_json = json.load(f)
            sf_arr, long_lat_sf_arr, sf_name_arr = kvutils.streamflow_parser(sf_obs_json,init_date,timesteps)
            obs_dict["streamflow"] = sf_arr
            obs_pos_dict["streamflow"] = long_lat_sf_arr

        # Setup initial obs dict
        initial_obs_dict = {"swe": obs_dict["swe"][0,:],
                            "streamflow": obs_dict["streamflow"][0, :]}

        # Setup rasterIO object
        rasterIOObject = utils.RasterParameterIO(rasterio_template_file, 1)


        ##### Build H matricies #####
        H_dict = {}

        ### SWE - H matrix ###
        swe_ind = np.array([~rasterIOObject.transform * x for x in obs_pos_dict["swe"]],  dtype="int64")
        sf_ind = np.array([~rasterIOObject.transform * x for x in obs_pos_dict["streamflow"]],  dtype="int64")

        H_dict["swe"] = kvutils.prepare_H_matrix_raster(swe_ind, rasterIOObject)

        ### Streamflow - H matrix ###
        with open("streamflows.json", 'r') as f:
            sf_json = json.load(f)

        # Create streamflow H matrix (for state -> observations)
        H_dict["streamflow"] = kvutils.prepare_H_matrix_streamflow(sf_json, sf_obs_json)

        # Create streamflow H matrix (for theta prameters -> raster)
        H_dict_raster_sf = kvutils.prepare_H_matrix_raster(sf_ind, rasterIOObject)

        # Initial states
        initial_state_dict = {}

        # These states will be copied into every single instance.
        PICKLE_EXT = "../../kalman_data/initial_pickles/"
        required_files = [PICKLE_EXT + "pond.pickled", PICKLE_EXT + "sm.pickled", PICKLE_EXT + "soils.pickled", PICKLE_EXT + "streamflows.pickled", PICKLE_EXT + "swe.pickled"]

        # Grab initial Streamflow and SWE states from the pickles.
        initial_state_dict["swe"] = pickle.load(open("initial_pickles/swe.pickled", "rb")).flatten()
        initial_state_dict["streamflow"] = pickle.load(open("initial_pickles/streamflows.pickled", "rb")).flatten()

        # Grab MT elevation data
        MT_ELEV = utils.RasterParameterIO("MTDEM_CLIP.tif", 1).array


        # If the user has activated the NRR module, create an instance of the Kalman_NRR class and an array
        if nrr_module_active:
            nrr = Kalman_NRR()
            nrr_arr_dict = {}
            for key in initial_state_dict:
                nrr_arr_dict[key] = np.zeros(timesteps)

        # Get true DDF and pp_temp_thres parameters by multiplying via swe H matrix.
        theta_param_dict["ddf"] = theta_param_dict["ddf"].dot(H_dict["swe"].T)
        theta_param_dict["pp_temp_thres"] = theta_param_dict["pp_temp_thres"].dot(H_dict["swe"].T)
        theta_param_dict["aet_lp_param"] = theta_param_dict["aet_lp_param"].dot(H_dict_raster_sf.T)
        theta_param_dict["soil_beta"] = theta_param_dict["soil_beta"].dot(H_dict_raster_sf.T)
        theta_param_dict["soil_max_wat"] = theta_param_dict["soil_max_wat"].dot(H_dict_raster_sf.T)

        # Set min, max limits for parameters.
        tparam_mins = {"ddf": 0.01, "pp_temp_thres": -5, "aet_lp_param": 0.01, "soil_beta": 0.01, "soil_max_wat": 0.01}
        tparam_maxs = {"ddf": 10, "pp_temp_thres": 5, "aet_lp_param": 5, "soil_beta": 5, "soil_max_wat": 400}

        ####>>>>>> PREPARE ARGUMENTS <<<<<<####
        init_args = {
            'init_date': init_date,
            'precip': "precipmod.tif",
            'tmin': "tminmod.tif",
            'tmax': "tmaxmod.tif",
            'params': "../../kalman_data/wm_param_files_test.json",
            'network_file': "../../kalman_data/rivout.shp",
            'basin_shp': "../../kalman_data/subsout.shp",
            'econengine': None,
            'restart': True} # Restart is now always True because we are using the pickled states from a spun up model

        kutils.exit_folder()

        current_args = dict(init_args) # This dict will be modified per timestep
        init_date = init_args['init_date']

        # If init_date is not 9/1/2012, we need to figure out which timestep of the year to use.
        t_offset = 0
        if init_date != "9/1/2012":
            t_offset = (parse(init_date) - parse("09/1/2012")).days

        ####>>>>>> GETTERS AND SETTERS <<<<<<####

        # Set up our getters and setters contained in the 'Dawuaphydroengine_Kalman_Getters_And_Setters' class:
        dkgas = Dawuaphydroengine_Kalman_Getters_And_Setters(rasterIOObject, {"swe": H_dict["swe"], "streamflow": H_dict_raster_sf}, {"swe": MT_ELEV})

        # Initiate Dual Ensemble Kalman Filter
        dekf = Dual_Ensemble_Kalman_Filter(f_hydrovehicle, init_args, theta_param_dict, initial_uparam_dict, initial_obs_dict, initial_state_dict, H_dict, par_states, required_files=required_files, tparam_mins=tparam_mins, tparam_maxs=tparam_maxs, **kwargs)

        # Set up our getters and setters.
        dekf.set_state_getter(dkgas.get_state_swe, "swe")
        dekf.set_state_getter(dkgas.get_state_streamflow, "streamflow")

        dekf.set_uparam_modifyer(dkgas.save_u_precip, "precip")
        dekf.set_uparam_modifyer(dkgas.save_u_tmin, "tmin")
        dekf.set_uparam_modifyer(dkgas.save_u_tmax, "tmax")

        dekf.set_tparam_modifyer(dkgas.save_t_ddf, "ddf")
        dekf.set_tparam_modifyer(dkgas.save_t_pp_temp_thres, "pp_temp_thres")
        dekf.set_tparam_modifyer(dkgas.save_t_soil_beta, "soil_beta")
        dekf.set_tparam_modifyer(dkgas.save_t_aet_lp_param, "aet_lp_param")
        dekf.set_tparam_modifyer(dkgas.save_t_soil_max_wat, "soil_max_wat")

        dekf.set_state_modifyer(dkgas.save_state_swe, "swe")
        dekf.set_state_modifyer(dkgas.save_state_streamflow, "streamflow")

        # Prepare members - places all nesessary files within model run folders
        dekf.prepare_members()

        if create_graphs: # Allocate storage for graphs if the user wants them.
            # Graph things
            pri_t, post_t, pri_s, post_s, innov_pr, innov_st, pred_cov, pert_obs, pred_s = kvutils.graph_initialization_full(kwargs['instances'], timesteps, theta_param_dict, initial_state_dict, initial_obs_dict)
        else:
            post_t = {}
            for key, array in theta_param_dict.iteritems():
                post_t[key] = np.zeros((kwargs['instances'], array.flatten().shape[0], timesteps))
                post_t[key][:, :, 0] = array.flatten()

        ####>>>>>> KALMAN LOOP <<<<<<####
        for t in range(timesteps):
            # Calculate the date at the current timestep
            the_day = (parse(init_date) + t * datetime.timedelta(seconds=86400)).strftime(
                "%m/%d/%Y")

            print "Beginning " + the_day

            cur_obs_dict = {"swe": np.array(obs_dict["swe"][t, :], copy=True),
                                "streamflow": np.array(obs_dict["streamflow"][t, :], copy=True)}

            # Update current arguments for this date
            current_args['init_date'] = the_day

            # Setup U-params
            cur_uparam_dict = {}
            for key, array in uparam_dict.iteritems():
                cur_uparam_dict[key] = uparam_dict[key][t+t_offset, :, :].flatten()

            # Where do we perturb ddf params? Only where the snow is melting.
            if t != 0:
                cur_mask_ddf = np.where(obs_dict["swe"][t, :] < obs_dict["swe"][t - 1, :], 0, 1)
            else:
                cur_mask_ddf = np.ones(obs_dict["swe"][0,:].shape) # All nodes are masked

            # Where do we perturb temp params? Everywhere the snow changes.

            if t == 0 or kwargs["Q_errors"]["pp_temp_thres"] == 0: # 1st timestep or we want to shut off this parameter this run.
                cur_mask_pp_temp_thres = np.ones(obs_dict["swe"][0, :].shape)  # All nodes are masked
            else:
                cur_mask_pp_temp_thres = np.where(obs_dict["swe"][t, :] != obs_dict["swe"][t - 1, :], 0, 1)

            # Always allow temp params to perturb
            # cur_mask_pp_temp_thres = np.zeros(obs_dict["swe"][0, :].shape)  # All nodes are active
            # cur_mask_pp_temp_thres = np.ones(obs_dict["swe"][0, :].shape)  # All nodes deactivated


            dekf.set_param_mask_mat(cur_mask_ddf, cur_mask_ddf, "ddf")
            dekf.set_param_mask_mat(cur_mask_pp_temp_thres, cur_mask_pp_temp_thres, "pp_temp_thres")


            # Run regardlness, param correction is now controlled by masks.
            dekf.simulate(uparam_dict=cur_uparam_dict, arg_dict=current_args)  # Simulate step
            dekf.update(obs_dict=cur_obs_dict, state_filter_active=run_state_correction)  # Update step

            # Obtain info
            cur_pri_t, cur_post_t, cur_pri_s, cur_post_s, cur_pred_s, cur_innov_pr, cur_innov_st = dekf.get_state_and_param_info()

            if create_graphs: # Only do this if we need to
                # Obtain more info
                cur_pred_cov = dekf.get_pred_cov_info()
                cur_per_obs = dekf.get_perturbed_obs()

                # Place nan values back into innovation arrays (for now nan is just 0)
                if cur_innov_st["streamflow"].shape[1] < 83:
                    zeros_st = np.zeros((cur_pri_s["streamflow"].shape[0], obs_dict["streamflow"][t, :].shape[0]))
                    zeros_st[:, ~np.isnan(obs_dict["streamflow"][t, :])] = cur_innov_st["streamflow"]
                    cur_innov_st["streamflow"] = zeros_st

                # Update each graph dictionary
                pri_t = kvutils.update_dict(cur_pri_t, pri_t, t)
                post_t = kvutils.update_dict(cur_post_t, post_t, t)
                pri_s = kvutils.update_dict(cur_pri_s, pri_s, t)
                post_s = kvutils.update_dict(cur_post_s, post_s, t)
                innov_pr = kvutils.update_dict(cur_innov_pr, innov_pr, t)
                innov_st = kvutils.update_dict(cur_innov_st, innov_st, t)

                if cur_pred_cov["swe"].size == 1 or cur_pred_cov["swe"].shape[0] != obs_dict["swe"].shape[1]:
                    cov_mat = np.zeros((obs_dict["swe"].shape[1],obs_dict["swe"].shape[1]))
                    cov_mat[cur_mask_ddf == 0, :][:, cur_mask_ddf == 0] = cur_pred_cov["swe"]
                    cur_pred_cov["swe"] = cov_mat

                if cur_per_obs["swe"].shape[1] != obs_dict["swe"].shape[1]:
                    # if t == 0:
                    swe_obs = np.zeros((pert_obs["swe"].shape[0], pert_obs["swe"].shape[1]))
                    swe_obs[:] = np.nan

                    if run_state_correction:
                        swe_pd = np.zeros((pred_s["swe"].shape[0], pred_s["swe"].shape[1]))
                        swe_pd[:] = np.nan

                    swe_obs[:, cur_mask_ddf == 0] = cur_per_obs["swe"]
                    cur_per_obs["swe"] = swe_obs

                    if run_state_correction:
                        swe_pd[:, cur_mask_ddf == 0] = cur_pred_s["swe"]
                        cur_pred_s["swe"] = swe_pd

                if cur_pred_cov["streamflow"].shape[0] != obs_dict["streamflow"].shape[1]:
                    cov_mat = np.zeros((obs_dict["streamflow"].shape[1],obs_dict["streamflow"].shape[1]))
                    cov_mat[~np.isnan(obs_dict["streamflow"][t, :]), :][:, ~np.isnan(obs_dict["streamflow"][t, :])] = cur_pred_cov["streamflow"]
                    cur_pred_cov["streamflow"] = cov_mat

                if cur_per_obs["streamflow"].shape[1] != obs_dict["streamflow"].shape[1]:
                    # if t == 0:
                    str_obs = np.zeros((pert_obs["streamflow"].shape[0], pert_obs["streamflow"].shape[1]))
                    str_obs[:] = np.nan

                    if run_state_correction:
                        str_pd = np.zeros((pred_s["streamflow"].shape[0], pred_s["streamflow"].shape[1]))
                        str_pd[:] = np.nan

                    str_obs[:,~np.isnan(obs_dict["streamflow"][t, :])] = cur_per_obs["streamflow"]
                    cur_per_obs["streamflow"] = str_obs

                    if run_state_correction:
                        str_pd[:,~np.isnan(obs_dict["streamflow"][t, :])] = cur_pred_s["streamflow"]
                        cur_pred_s["streamflow"] = str_pd


                pred_cov = kvutils.update_dict(cur_pred_cov, pred_cov, t)
                pert_obs = kvutils.update_dict(cur_per_obs, pert_obs, t)
                pred_s = kvutils.update_dict(cur_pred_s, pred_s, t)
                # NRR Module
                if nrr_module_active:
                    for key in nrr_arr_dict:
                        nrr_arr_dict[key][t] = np.mean(nrr.calculate_NRR(pred_s[key], pert_obs[key]))

            else: # Only save what we care about - the post tparam state.
                post_t = kvutils.update_dict(cur_post_t, post_t, t)
        # NRR Module
        if nrr_module_active:
            print "NRR MODULE CONCLUSION"
            nrr_results_str = ""
            for key in nrr_arr_dict:
                nrr_results_str += "OVERALL NRR VALUE USING " + key + ": " + str(np.mean(nrr_arr_dict[key], axis=0))

            print nrr_results_str
            kvutils.write_to_file(nrr_results_str, "nrr_results")

        # Call final parameter function
        dkgas.generate_final_params(post_t, par_states=par_states)



        # Debug
        if debug:
            nodes_to_plot_swe = [3,4]
            nodes_to_plot_sf = [1,5]
        else:
            # Plot all nodes
            nodes_to_plot_swe = np.arange(90)
            nodes_to_plot_sf = np.arange(initial_obs_dict["streamflow"].size)

        node_names = {}

        for node in nodes_to_plot_sf:
            node_names[node] = sf_name_arr[node]

        for node in nodes_to_plot_swe:
            node_names[node] = swe_name_arr[node]

        # Save station info to array
        swe_info = np.array([swe_name_arr, long_lat_swe_arr[:,0], long_lat_swe_arr[:,1]])
        # np.savetxt("swestations.csv", swe_info, delimiter=",")

        if create_graphs:
            # Call graphing function
            kalmangraphs.main_prettygraphs_new(pri_t, post_t, pri_s, post_s, innov_pr, innov_st, obs_dict, H_dict, pred_cov, xlabel_time='Days starting at 9/1/2012', ylabels={"ddf": 'DDF', "pp_temp_thres": "Temperature Threshold", "swe": 'Snow Water Equivalent (mm)', "streamflow": 'Streamflow', "soil_beta": "Soil Beta", "aet_lp_param": "AET LP", "soil_max_wat": "Soil Max Water"}, plot_keys=[["swe", ["pp_temp_thres", "ddf"]],["streamflow", ["soil_beta", "aet_lp_param", "soil_max_wat"]]], nodes_to_plot={"swe": nodes_to_plot_swe, "streamflow": nodes_to_plot_sf}, node_names=node_names)

        if nrr_module_active:
            kalmangraphs.nrr_prettygraphs(nrr_arr_dict)





