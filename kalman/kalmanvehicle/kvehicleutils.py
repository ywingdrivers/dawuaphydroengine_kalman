import numpy as np
import time, datetime
from dateutil import tz
from dateutil.parser import parse
import json

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
#######____ Mask Utilities ____#######
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

def prepare_THIESSEN_matrix(mask_file, key_file):
    """
        Uses kalman_data/THIESSENRaster.tif and turns it into an array that maps stations to raster points.
        :param mask_file: A THIESSEN array that has obseration points as region markers. Ex. [[56,56,63],[56,63,63]]
        :param key_file: An array with indicies (which will be used to replace region markers) corresponding to key values. Ex. [56,63]
        :return: THIESSEN array. Using the above examples, we expect an array of [[0,0,1],[0,1,1]]
    """

    # Replace station ID with row
    row = 0
    for key in key_file:
        mask_file[mask_file == int(key)] = row
        row += 1

    return mask_file

def prepare_H_matrix_raster(ind, rasterIOObject):
    """

        Creates a matrix mask from long/lat coordinates and a RasterIO object with transform info.
        :param pos_obs: A list of lists of long/lat positions. Ex: [[-112,42],[-110,48]]
        :param rasterIOObject: A rasterIOObject with relevant transform information.
        :return: matrix observation mask
    """

    # Longitude/latitude arrays corresponding to model raster files
    #lon, lat = rasterIOObject.transform * np.indices(rasterIOObject.array.T.shape)

    # ind = [~rasterIOObject.transform * x for x in pos_obs]

    np_ind = np.array(ind, dtype="int64")
    count = np.unique(np_ind, axis=0, return_counts=True)[1]
    # while count.max() > 1:
    #     vals, index, count = np.unique(np_ind, axis=0, return_counts=True, return_index=True)
    #
    #     big_vals = vals[count > 1]

    for i, pos in enumerate(ind):
        H_row = np.zeros_like(rasterIOObject.array)
        H_row[int(ind[i,1]), int(ind[i,0])] = 1 # We don't round here because we care about the obs station being 'in the pixel'.

        if i == 0:  # First iteration, create matrix
            H = np.array(H_row.flatten())
        else:  # Append to matrix
            H = np.vstack((H, H_row.flatten()))

    # if np.sum(H, axis=0).max() > 1:
    #     raise ValueError('The SWE H matrix has two entries for a row, possibly because of a duplicate series of coordinates.')

    return H

def prepare_H_matrix_streamflow(streamnodes, obs_nodes):
    """
    Creates a matrix mask for streamflow compared to observed points.
    :param streamnodes: dict from streamflow.json file (dawuahydroengine)
    :param activegages: dict from active_gauge_2.5mi.json file (hydromodelbrowser)
    :return: H matrix
    """

    # iterator
    first_time = True

    for key in obs_nodes:
        H_row = np.zeros(len(streamnodes["nodes"]))

        for j, streamnode in enumerate(streamnodes["nodes"]):
            if int(key)-1 == j: # We subtract 1 for bad formatting reasons.
                H_row[j] = 1
                # print "MATCHED " + str(j)

        if first_time:  # First iteration, create matrix
            H = np.array(H_row.flatten())
            first_time = False
        else:  # Append to matrix
            H = np.vstack((H, H_row.flatten()))

    return H

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
#######____ Time Utilities ____#######
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

def date_to_local_unix_timestamp(date, timezone="America/Denver"):
    """
    Convert date string to local UNIX timestamp. Defaults to Mountain Time.
    :param date: Date in the format of 'Mm/Dd/YYYY'
    :return: UNIX time in Denver timezone
    """
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz(timezone)
    utc_date = parse(date).replace(tzinfo=from_zone)
    local_date = utc_date.astimezone(to_zone)
    return unicode(str(int(time.mktime((local_date).timetuple())) * 1000), "utf-8")

    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
    #######____ Graph Utilities ____#######
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

def graph_initialization_simple(instances, timesteps, tparam_dict, initial_state_dict, initial_obs_dict):
    """
    Allocates memory to save KF states for graphs.
    :return: Nothing
    """

    # Prior parameter arrays - initial slot holds initial estimates.
    graph_pri_tparam = {}
    for key, array in tparam_dict.iteritems():
        graph_pri_tparam[key] = np.zeros((instances, array.flatten().shape[0], timesteps + 1))
        graph_pri_tparam[key][:, :, 0]  = array.flatten()

    # Prior state arrays - initial slot is
    graph_pri_state = {}
    for key, array in initial_state_dict.iteritems():
        graph_pri_state[key] = np.zeros((instances, array.flatten().shape[0], timesteps + 1))
        graph_pri_state[key][:,:,0] = array.flatten()

    # Posterior parameter arrays - initial slot holds initial estimates.
    graph_post_tparam = {}
    for key, array in tparam_dict.iteritems():
        graph_post_tparam[key] = np.zeros((instances, array.flatten().shape[0], timesteps + 1))
        graph_post_tparam[key][:, :, 0] = array.flatten()

    # Posterior state arrays - initial slot assumed 0 for now {{FIX}}
    graph_post_state = {}
    for key, array in initial_state_dict.iteritems():
        graph_post_state[key] = np.zeros((instances, array.flatten().shape[0], timesteps + 1))
        graph_post_state[key][:, :, 0] = array.flatten()

    graph_innovation_pr = {}
    graph_innovation_st = {}
    for key, array in initial_obs_dict.iteritems():
        graph_innovation_pr[key] = np.zeros((instances, array.flatten().shape[0], timesteps))
        graph_innovation_st[key] = np.zeros((instances, array.flatten().shape[0], timesteps))

    return graph_pri_tparam, graph_post_tparam, graph_pri_state, graph_post_state, graph_innovation_pr, graph_innovation_st, graph_pred_cov

def graph_initialization_full(instances, timesteps, tparam_dict, initial_state_dict, initial_obs_dict):
    """
    Allocates memory to save KF states for graphs.
    :return: Nothing
    """

    # Prior parameter arrays - initial slot holds initial estimates.
    graph_pri_tparam = {}
    for key, array in tparam_dict.iteritems():
        graph_pri_tparam[key] = np.zeros((instances, array.flatten().shape[0], timesteps))
        graph_pri_tparam[key][:, :, 0]  = array.flatten()

    # Prior state arrays - initial slot is
    graph_pri_state = {}
    for key, array in initial_state_dict.iteritems():
        graph_pri_state[key] = np.zeros((instances, array.flatten().shape[0], timesteps))
        graph_pri_state[key][:,:,0] = array.flatten()

    # Posterior parameter arrays - initial slot holds initial estimates.
    graph_post_tparam = {}
    for key, array in tparam_dict.iteritems():
        graph_post_tparam[key] = np.zeros((instances, array.flatten().shape[0], timesteps))
        graph_post_tparam[key][:, :, 0] = array.flatten()

    # Posterior state arrays - initial slot assumed 0 for now {{FIX}}
    graph_post_state = {}
    for key, array in initial_state_dict.iteritems():
        graph_post_state[key] = np.zeros((instances, array.flatten().shape[0], timesteps))
        graph_post_state[key][:, :, 0] = array.flatten()

    graph_innovation_pr = {}
    graph_innovation_st = {}
    for key, array in initial_obs_dict.iteritems():
        graph_innovation_pr[key] = np.zeros((instances, array.flatten().shape[0], timesteps))
        graph_innovation_st[key] = np.zeros((instances, array.flatten().shape[0], timesteps))

    # Covariance (only for prediction, others are way too large)
    graph_pred_cov = {}
    for key, array in initial_obs_dict.iteritems():
        graph_pred_cov[key] = np.zeros((array.flatten().shape[0], array.flatten().shape[0], timesteps))

    graph_perterbed_obs = {}
    for key, array in initial_obs_dict.iteritems():
        graph_perterbed_obs[key] = np.zeros((instances, array.flatten().shape[0], timesteps))

    graph_prediction = {}
    for key, array in initial_obs_dict.iteritems():
        graph_prediction[key] = np.zeros((instances, array.flatten().shape[0], timesteps))



    return graph_pri_tparam, graph_post_tparam, graph_pri_state, graph_post_state, graph_innovation_pr, graph_innovation_st, graph_pred_cov, graph_perterbed_obs, graph_prediction

def update_dict(new_info, graph_dict, t):
    """
    Simple function to add info from a dict of 2D arrays to a dict of 3D arrays
    :param new_info: 2D dictionary of numpy arrays
    :param graph_dict: 3D dictionary of numpy arrays that will be modified
    :param t: 3rd dimension (timestep)
    :return: graph_dict with new information added to each member at point arr[:,:,t]
    """
    for key, array in new_info.iteritems():
        graph_dict[key][:,:,t] = array

    return graph_dict

    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% #
    #######____ Parsing Utilities ____#######
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% #

def swe_parser(swe_obs_json, init_date, timesteps):
    """
    Parses a SWE JSON file. Returns both an array of the SWE and a list of LAT and LONG coordinates for each station.
    :param swe_obs_json: JSON object of SWE stations from USDA (grabbed from a script in HydroModelBrowser, {{find it}}.)
    :param init_date: Initial date SWE file begins on.
    :param timesteps: Number of timesteps to grab.
    :return: numpy array of SWE stations, list of LAT, LONG tuples.
    """

    long_lat_list_swe = []

    # Link each row with a specific key (!! - this is really really important because there's no guarantee .items() will iterate over the dict the same way each time - !!)
    pos_list_swe = []
    for ind, (key, node) in enumerate(swe_obs_json.items()):
        pos_list_swe.append(key)
    # Loop through and parse SWE JSON file.
    for t in range(timesteps):
        the_day = (parse(init_date) + t * datetime.timedelta(seconds=86400)).strftime("%m/%d/%Y")

        unix_time = date_to_local_unix_timestamp(the_day, "America/Denver")

        obsList = []
        for key in pos_list_swe:


            if unix_time in swe_obs_json[key]['values']:
                obsList.append(swe_obs_json[key]['values'][unix_time]['values'] / 0.039370078740158)  # inches to mm

            else:
                print "MISSING SWE VALUE"
                obsList.append(np.nan)

            if t == 0:  # On the first iteration we'll build a list of lat and long points! <3
                # Append the long and lat of each node to an array of arrays
                long_lat_list_swe.append([float(swe_obs_json[key][u'long']), float(swe_obs_json[key][u'lat'])])

        if t == 0:  # First iteration, create matrix
            swe_arr = np.array(obsList)
        else:  # Append to matrix
            swe_arr = np.vstack((swe_arr, obsList))

    return swe_arr, np.array(long_lat_list_swe), np.array(pos_list_swe)

def streamflow_parser(sf_obs_json, init_date, timesteps):
    """

    :param sf_obs_json: JSON file of streamflow stations from USDA (grabbed from a script in HydroModelBrowser, {{find it}}.)
    :param init_date: Initial date SWE file begins on.
    :param timesteps: Number of timesteps to grab.
    :return: numpy array of SWE stations, list of LAT, LONG tuples.
    """

    long_lat_list_sf = []

    # Link each row with a specific key (!! - this is really really important because there's no guarantee .items() will iterate over the dict the same way each time - !!)
    pos_list_sf = []
    for ind, (key, node) in enumerate(sf_obs_json.items()):
        pos_list_sf.append(key)

    # Loop through and parse Streamflow JSON file.
    for t in range(timesteps):
        the_day = (parse(init_date) + t * datetime.timedelta(seconds=86400)).strftime("%m/%d/%Y")

        unix_time = date_to_local_unix_timestamp(the_day, "America/Denver")

        obsList = []
        for key in pos_list_sf:

            if unix_time in sf_obs_json[key]:
                obsList.append(sf_obs_json[key][unix_time]['value'])  # no conversion nessesary, already converted.

            else:
                obsList.append(np.nan)

            # Append the long and lat of each node to an array of arrays
            if t == 0:
                # long_lat_list_sf.append([float(node[u'long']), float(node[u'lat'])])
                long_lat_list_sf.append([float(sf_obs_json[key][u'long']), float(sf_obs_json[key][u'lat'])])
        if t == 0:  # First iteration, create matrix
            sf_arr = np.array(obsList)
        else:  # Append to matrix
            sf_arr = np.vstack((sf_arr, obsList))

    return sf_arr, np.array(long_lat_list_sf), np.array(pos_list_sf)


def write_to_file(string, file_name):
    """
    Writes any string to a .info file
    :param string:
    :param file_name:
    :return:
    """
    f = open(file_name + ".info", "w")
    f.write(string)
    f.close()

