from __future__ import division
import numpy as np
import scipy.interpolate as interp
import scipy.stats as stats
from kalman import kalmanutils as kutils
import cPickle as pickle

# For debugging
import matplotlib.pyplot as plt

class Dawuaphydroengine_Kalman_Getters_And_Setters:
    """
        A class that holds getters and setters to pass to the Dual_Ensemble_Kalman_Filter class
         so it can save info into dawuaphydroengine instances.
        :param rasterIOObject: Used to save to model GeoTIFFs
        :param H_dict: Masking matrix for states (for linear interpolation)
        :param ele_dict: Elevation for states (for physical interpolation, not currently implemented)
    """

    def __init__(self, rasterIOObject, H_dict, ele_dict):
        """
        :param rasterIOObject: Used to save to model GeoTIFFs
        :param H_dict: Masking matrix for states
        :param H_dict: Masking matrix for states (for linear interpolation)
        :param ele_dict: Elevation for states (for physical interpolation, not currently implemented)
        """

        self.rasterIOObject = rasterIOObject
        self.H_dict = H_dict
        self.ele_dict = ele_dict

        # Setup RBF interpolation info
        self.xcoords = {}
        self.ycoords = {}
        self.xc = {}
        self.yc = {}
        self.result_x = {}
        self.result_y = {}
        self.index = {}

        # set up variables for griddata interpolation.
        for key, H_interp in H_dict.iteritems():
            xcoords = []
            ycoords = []
            for i in range(H_interp.shape[0]):
                flat_ind = np.where(H_interp[i, :] == 1)[0][0]
                true_ind = np.unravel_index(flat_ind, self.rasterIOObject.array.shape)
                ycoords.append(true_ind[0])
                xcoords.append(true_ind[1])

            self.xcoords[key] = np.array(xcoords, dtype="float64")
            self.ycoords[key] = np.array(ycoords, dtype="float64")

            uniquecoords, self.index[key] = np.unique(zip(self.xcoords[key], self.ycoords[key]), axis=0, return_index=True)
            self.xc[key] = uniquecoords[:,0]
            self.yc[key] = uniquecoords[:,1]

            xspace = np.arange(self.rasterIOObject.array.shape[1], dtype="float64")
            yspace = np.arange(self.rasterIOObject.array.shape[0], dtype="float64")
            self.result_x[key], self.result_y[key] = np.meshgrid(xspace, yspace)


    # ********
    # Getters and Setters: UPARAMS
    # ********
    def save_u_precip(self, arr, i):
        """
        Saves geotiff to precipmod.tif
        :param arr: Array to save
        :return: Nothing
        """
        self._save_array_to_geotiff_(arr, "precipmod.tif")

    def save_u_tmin(self, arr, i):
        """
        Saves geotiff to tminmod.tif
        :param arr: Array to save
        :return: Nothing
        """
        self._save_array_to_geotiff_(arr, "tminmod.tif")

    def save_u_tmax(self, arr, i):
        """
        Saves geotiff to tmaxmod.tif
        :param arr: Array to save
        :return: Nothing
        """
        self._save_array_to_geotiff_(arr, "tmaxmod.tif")

    # ********
    # Getters and Setters: TPARAMS
    # ********

    def save_t_ddf(self, arr, i):
        """
        Saves geotiff to ddf_mod.tif
        :param arr: Array to save
        :return: Nothing
        """
        if arr.min() < 2:
            print "DANGER! DDF is less than 2."


        if arr.max() > 8:
            print "DANGER! DDF is greater than 8."

        self.save_array_to_interp(arr, "ddf_mod.tif", "swe")

    def save_t_pp_temp_thres(self, arr, i):
        """
        Saves geotiff to pp_temp_thres_mod.tif
        :param arr: Array to save
        :return: Nothing
        """

        self.save_array_to_interp(arr, "pp_temp_thres_mod.tif", "swe")

    def save_t_soil_beta(self, arr, i):
        """
        Saves geotiff to soil_beta_mod.tif
        :param arr: Array to save
        :return: Nothing
        """

        self.save_array_to_interp(arr, "soil_beta_mod.tif", "streamflow")

    def save_t_aet_lp_param(self, arr, i):
        """
        Saves geotiff to aet_lp_param_mod.tif
        :param arr: Array to save
        :return: Nothing
        """

        self.save_array_to_interp(arr, "aet_lp_param_mod.tif", "streamflow")

    def save_t_soil_max_wat(self, arr, i):
        """
        Saves geotiff to soil_max_wat_mod.tif
        :param arr: Array to save
        :return: Nothing
        """

        self.save_array_to_interp(arr, "soil_max_wat_mod.tif", "streamflow")

    # ********
    # Getters and Setters: STATES
    # ********
    def get_state_swe(self, i):
        """
        Opens swe.pickled
        :param i: Instance (not used)
        :return: flattened array
        """
        with open("swe.pickled", "rb") as f:
            info = pickle.load(f).flatten()

        return info

    def get_state_streamflow(self, i):
        """
        Opens streamflows.pickled
        :param i: Instance (not used)
        :return: flattened array
        """
        with open("streamflows.pickled", "rb") as f:
            info = pickle.load(f).flatten()
        return info

    def save_state_swe(self, arr, i):
        """
        Saves geotiff to swe.pickled
        :param arr: Array to save
        :return: Nothing
        """
        arr = arr.clip(min=0) # We don't want negative snow.
        self._save_rioshape_array_to_pickle_(arr, "swe.pickled")

    def save_state_streamflow(self, arr, i):
        """
        Saves geotiff to streamflows.pickled
        :param arr: Array to save
        :return: Nothing
        """
        with open("streamflows.pickled", "wb") as f:
            pickle.dump(arr, f)



    # ################################################################# #
    # WRAPPER METHODS - these will be used for all getters and setters. #
    # ################################################################# #
    def _save_array_to_geotiff_(self, arr, filename):
        """
        Reshapes and saves a GEOTIFF file (.tif) to disk from an array. Uses rasterIOObject as a template.
        :param arr: array to use.
        :param filename: Name to save file to.
        :return: Nothing
        """
        reshaped_arr = np.reshape(arr, self.rasterIOObject.array.shape)  # Change back to 2D array
        self.rasterIOObject.write_array_to_geotiff(filename, reshaped_arr)

    def _save_rioshape_array_to_pickle_(self, arr, filename):
        """
        Reshapes and saves a .pickle file to disk from an array. Uses rasterIOObject as a template.
        :param arr: array to use.
        :param filename: Name to save file to.
        :return: Nothing
        """
        with open(filename, "wb") as f:
            pickle.dump(arr.reshape(self.rasterIOObject.array.shape), f)

    def _build_interp(self, arr, st_key):
        val = arr[self.index[st_key]]

        # Build nearest neightbor grid for extrapolation
        extrap_arr = interp.griddata(np.array([self.xc[st_key], self.yc[st_key]]).T,val,
                                     (self.result_x[st_key], self.result_y[st_key]), method='nearest')  # default method is linear

        # Create linear interpolation
        result_arr = interp.griddata(np.array([self.xc[st_key], self.yc[st_key]]).T,val,
                                     (self.result_x[st_key], self.result_y[st_key]), method='linear')  # default method is linear

        # Suppliment the interpolation with the extrapolation
        result_arr[np.where(np.isnan(result_arr))] = extrap_arr[np.where(np.isnan(result_arr))]

        return result_arr

    def save_array_to_theissen(self, arr, filename):
        """
        Use a masking matrix and a theissen template to save a GEOTIFF file (.tif) to disk.
        Uses THEISSEN matrix.
        :param arr: array to use.
        :param filename: Name to save file to.
        :return: Nothing
        """
        newParamRaster = np.copy(self.THIESSEN_swe)
        for ind, val in enumerate(arr):
            newParamRaster[newParamRaster == ind] = val

        self.rasterIOObject.write_array_to_geotiff(filename, newParamRaster)

    def save_array_to_interp(self, arr, filename, st_key):
        """
        Interpolates between points on an array. All extrapolated points are controlled by the corresponding theissen.
        :param arr: array to use.
        :param filename: Name to save file to.
        :param st_key: The key's state.
        :return: Nothing
        """

        return self._save_array_to_geotiff_(self._build_interp(arr, st_key), filename)

    def save_array_to_ele_interp(self, arr, filename, st_key):
        """
        Use a linear regressive model to interpolate DDF by elevation. Saves as a GeoTiff.
        Really sketch right now, elevation and DDF do not seem to be correlated.
        :param arr: array to use.
        :param filename: Name to save file to.
        :param st_key: The key's state.
        :return: Nothing
        """
        T = .7

        ele_arr = self.ele_dict[st_key] # This holds this state's elevation info.

        ele_stations = self.ele_dict[st_key].flatten().dot(self.H_dict[st_key].T)

        slope, intercept, r_value, p_value, std_err = stats.linregress(arr, ele_stations)

        final_arr = np.copy(ele_arr)
        final_arr[ele_arr == -99999] = np.nan  # Change no data to nan

        final_arr[ele_arr != np.nan] = ele_arr[ele_arr != np.nan] * slope + intercept

        # plt.plot(arr, ele_stations, 'o', label='original data')
        # plt.plot(arr, intercept + slope * arr, 'r', label='fitted line')
        # plt.legend()
        # plt.show()

        # interp_arr_s = self._build_interp(arr,st_key)
        # interp_arr_z = self._build_interp(ele_stations,st_key)

        # interp_arr_s = self._build_interp(arr,st_key)
        # interp_arr_z = self._build_interp(ele_stations,st_key)
        #
        # ele_arr[ele_arr == -99999] = np.nan  # Change no data to nan
        # final_arr = np.copy(interp_arr_s)
        #
        # for i, a in np.ndenumerate(final_arr[ele_arr != np.nan]):
        #     gain = math.exp(-T * math.fabs(ele_arr[ele_arr != np.nan][i] - interp_arr_z[ele_arr != np.nan][i]))
        #     nearest_a = kutils.find_nearest(ele_stations, a)
        #     # final_arr[ele_arr != np.nan][i] = a * math.exp(T * (ele_arr[ele_arr != np.nan][i] - interp_arr_z[ele_arr != np.nan][i]))
        #     final_arr[ele_arr != np.nan][i] = a + gain*(nearest_a - a)


        return self._save_array_to_geotiff_(final_arr, filename)

    # ###################################################################### #
    # GENERATE FINAL PARAMS - Use this to obtain the final parameter rasters #
    # ###################################################################### #
    def generate_final_params(self, post_t_dict, par_states, destination="final_params"):
        """
        This uses all of the generated points to generate final parameters for the stuff. Results are stored in a folder.
        :param post_t_dict: Dictionary of final parameters over all timesteps (each 3D array organized as [inst,node,ts])
        :param par_states: The dictionary of which states correspond to each parameter.
        :param destination: The folder where final params should be generated. Default "final_params"
        :return: Nothing
        """

        kutils.enter_folder(destination)
        for key in post_t_dict:
            ens_mean = np.mean(post_t_dict[key], axis=0)

            for key_st in par_states[key]:
                # Save mean of last DDF data point
                self.save_array_to_interp(ens_mean[:,-1], "final_" + key + "_last_timestep.tif", key_st)

                # Save mean of all DDF values
                self.save_array_to_interp(np.mean(ens_mean, axis=1), "final_" + key + "_avg_all.tif", key_st)

        kutils.exit_folder()