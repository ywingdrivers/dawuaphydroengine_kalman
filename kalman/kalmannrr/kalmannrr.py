import numpy as np
import math


class Kalman_NRR:
    """
        This class evaluates the ensemble spread of the Dual_Ensemble_Kalman_Filter
        Using the math in https://www.sciencedirect.com/science/article/pii/S0309170804001605 as a template.
    """

    def __init__(self):
        print "Kalman NRR Evaluation Module Activated"

    def calculate_NRR(self, pred_mat, pert_obs_mat):
        """
         Calculates the Normalized RMSE Ratio from a DEKF ensemble array.
         :param pred_mat: 3-dimensional numpy array. Dimensions should be [instance,vector,time]
         :return: Ra
        """

        Ra = self._calculate_Ra_(pred_mat, pert_obs_mat)

        n = pred_mat.shape[0] # Get number of ensembles

        ex_Ra = math.sqrt((n+1)/(2.*n))

        return Ra/ex_Ra

    def _calculate_Ra_(self, pred_mat, pert_obs_mat):
        """

        :param pred_mat: 3-dimensional numpy array. Dimensions should be [instance,vector,time]
        :return: Ra
        """
        n = pred_mat.shape[0]
        t = pred_mat.shape[2]

        # r1_inside = np.square( np.mean(pred_mat-pert_obs_mat, axis=0) )
        r1_inside = np.mean( np.square(pred_mat-pert_obs_mat), axis=0)
        r1 = np.mean( np.sqrt(r1_inside), axis=1 )

        r2_inside = np.mean( np.square(pred_mat-pert_obs_mat), axis=2)
        r2 = np.mean( np.sqrt(r2_inside), axis=0 )

        return r1/r2
