import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import pickle, os, errno
from kalman import kalmanutils as kutils
import kgraphtypes as kgraphs

def main_prettygraphs(pri_t, post_t, pri_s, post_s, innov_pr, innov_st, obs, H_dict, pred_cov_dict, export_path = "pretty_plots_new", xlabel_time='Days starting at 9/1/2012', ylabels={}, plot_keys=[], nodes_to_plot=[0,40, 3], node_names={}):

    def plot_single_instance(pri_t_node, post_t_node, pri_s_node, post_s_node, obs_node, pred_cov_node, node_name, node_type="SWE", node_param_type="DDF", node_xlabel='Days starting at 9/1/2012', node_ylabel='Snow Water Equivalent (mm)', node_ylabel_param='DDF', plot_params=True):
        # Enter a folder of the node name
        kutils.enter_folder(node_name)

        # Plot node overall state
        kgraphs.create_stateplot(pri_s_node, post_s_node, obs_node, pred_cov_node, node_name + node_type, node_xlabel, node_ylabel)

        if plot_params:
            # Plot node overall param
            kgraphs.create_paramplot(pri_t_node, post_t_node, node_name + node_param_type, node_xlabel, node_ylabel_param)

        kutils.exit_folder()

    def plot_node_full(pri_t_node, post_t_node, pri_s_node, post_s_node, obs_node, innov_pr_node, innov_st_node, pred_cov_node, node_name, node_type = "SWE", node_param_type="DDF", node_xlabel='Days starting at 9/1/2012', node_ylabel='Snow Water Equivalent (mm)', node_ylabel_param='DDF', ens_list=[0,2], plot_params=True, post_t2=None, post_t2_name=""):
        # Enter a folder of the name of the node
        kutils.enter_folder(node_name)

        # Plot overall ensembles
        kgraphs.create_stateplot(np.mean(pri_s_node, axis=0), np.mean(post_s_node, axis=0), obs_node, pred_cov_node, node_name + node_type, node_xlabel, node_ylabel)

        # Plot all ensembles
        kgraphs.create_ens_stateplot(pri_s_node, post_s_node,obs_node, pred_cov_node, node_name + node_type, node_xlabel, node_ylabel)

        if plot_params:
            # Plot overall parapm
            kgraphs.create_paramplot(np.mean(pri_t_node, axis=0), np.mean(post_t_node, axis=0), node_name + node_param_type, node_xlabel, node_ylabel_param)

            # Plot all param journies
            kgraphs.create_ens_paramplot(pri_t_node,post_t_node, node_name+node_param_type,node_xlabel, node_ylabel_param)

            kgraphs.create_innovation(np.mean(innov_st_node, axis=0), name=node_name, xlabel=node_xlabel, ylabel="innovation", param_correction=True, innov_pr=np.mean(innov_pr_node, axis=0))

            if post_t2 is not None:
                kgraphs.create_scatterplot(np.mean(post_t_node, axis=0), np.mean(post_t2, axis=0), node_name, node_ylabel_param, post_t2_name)

        else:
            kgraphs.create_innovation(np.mean(innov_st_node, axis=0), name=node_name, xlabel=node_xlabel,
                                      ylabel="innovation")



        # Plot individual ensembles (default 0 and 2)
        for ens in ens_list:
            if plot_params:
                plot_single_instance(pri_t_node[ens,:], post_t_node[ens,:], pri_s_node[ens,:], post_s_node[ens,:], obs_node, pred_cov_node, node_name + "ENS" + str(ens), node_type, node_param_type, node_xlabel=node_xlabel, node_ylabel=node_ylabel, node_ylabel_param=node_ylabel_param, plot_params=plot_params)
            else:
                plot_single_instance(None, None, pri_s_node[ens, :], post_s_node[ens, :],
                                     obs_node, pred_cov_node, node_name + "ENS" + str(ens), node_type, node_param_type,
                                     node_xlabel=node_xlabel, node_ylabel=node_ylabel,
                                     node_ylabel_param=node_ylabel_param, plot_params=plot_params)


        kutils.exit_folder()

    plt.style.use('seaborn-darkgrid')

    print "Beginning prettygraphs..."

    EXPORT_PATH = export_path

    plt.switch_backend('agg')

    if not node_names: # If no node names are provided, just use the node indicies.
        for node in nodes_to_plot:
            node_names[node] = node

    kutils.enter_folder(EXPORT_PATH)

    # Plot keys are arranged as follows: [["statename","paramname"],"statename_2","paramname_2"]... "statename_i","paramname_i"]]
    for pair in plot_keys:

        kutils.enter_folder(pair[0])

        # These take forever to load
        nodes_arr_prs = H_dict[pair[0]].dot(pri_s[pair[0]])
        nodes_arr_pos = H_dict[pair[0]].dot(post_s[pair[0]])

        states_mean_pri = np.mean(nodes_arr_prs, axis=0)
        states_mean_post = np.mean(nodes_arr_pos, axis=0)

        # Variance for entire ensemble

        entire_ens_var = np.empty(pred_cov_dict[pair[0]].shape[2])
        for t in range(pred_cov_dict[pair[0]].shape[2]):
            diag = np.diag(pred_cov_dict[pair[0]][:,:,t])
            entire_ens_var[t] = np.mean(diag, axis=0)

        if len(pair) > 1: # One or more params is associated with this state
            t2_name = None # This will hold a second param if there is one.
            t2 = None
            if not isinstance(pair[1], (list,)): # Is there only one param?
                param_names = [pair[1]]
            else:
                param_names = pair[1]


            for i, pname in enumerate(param_names):
                if param_names[-1] != pname:
                    print param_names
                    t2_name = param_names[i+1]
                    t2 = post_t[t2_name]
                    t2_full = np.mean(t2, axis=1)
                else:
                    t2_name = None
                    t2 = None
                    t2_full = None


                plot_node_full(np.mean(pri_t[pname], axis=1), np.mean(post_t[pname], axis=1), states_mean_pri, states_mean_post, np.mean(obs[pair[0]], axis=1), np.mean(innov_pr[pair[0]], axis=1), np.mean(innov_st[pair[0]], axis=1), entire_ens_var, "all", node_xlabel=xlabel_time, node_ylabel=ylabels[pair[0]], node_param_type=pname, node_ylabel_param=ylabels[pname], post_t2=t2_full, post_t2_name=t2_name)

                # Plot specified individual nodes
                for node in nodes_to_plot[pair[0]]:
                    if param_names[-1] != pname:
                        t2_node = post_t2 = t2[:, node, :]
                    else:
                        t2_node = None


                    plot_node_full(pri_t[pname][:,node,:], post_t[pname][:,node,:], nodes_arr_prs[node,:,:], nodes_arr_pos[node,:,:], obs[pair[0]][:,node], innov_pr[pair[0]][:,node,:], innov_st[pair[0]][:,node,:], entire_ens_var, str(node_names[node]), node_xlabel=xlabel_time, node_ylabel=ylabels[pair[0]], node_param_type=pname, node_ylabel_param=ylabels[pname], post_t2=t2_node, post_t2_name=t2_name)

                kgraphs.create_param_node_journeys(np.mean(post_t[pname], axis=0), pname, xlabel_time, ylabel=ylabels[pname])
                kgraphs.create_param_node_journeys(post_t[pname][0,:,:], pname+"ENS0", xlabel_time, ylabel=ylabels[pname])


        else: # No param info exists for this state
            # Plot overall
            plot_node_full(None, None, states_mean_pri,
                           states_mean_post, np.mean(obs[pair[0]], axis=1), np.mean(innov_pr[pair[0]], axis=1),
                           np.mean(innov_st[pair[0]], axis=1), np.mean(np.diagonal(pred_cov_dict[pair[0]]), axis=0), "all", node_xlabel=xlabel_time,
                           node_ylabel=ylabels[pair[0]], plot_params=False)

            # Plot specified individual nodes
            for node in nodes_to_plot[pair[0]]:
                plot_node_full(None, None, nodes_arr_prs[node, :, :],
                               nodes_arr_pos[node, :, :], obs[pair[0]][:, node], innov_pr[pair[0]][:, node, :],
                               innov_st[pair[0]][:, node, :], pred_cov_dict[pair[0]][node,node,:], str(node_names[node]), node_xlabel=xlabel_time,
                               node_ylabel=ylabels[pair[0]], plot_params=False)

        kutils.exit_folder()

    kutils.exit_folder()



