import numpy as np
import math
import matplotlib
import matplotlib.pyplot as plt

#>>> Simple, overall plots <<<#

def create_stateplot(pri,post,obs, cov, name, xlabel="", ylabel=""):


    plt.figure()
    plt.plot(post, label='estimated SWE')
    plt.plot(pri, linestyle='dashed', label='prior estimated SWE')
    plt.plot(obs, linestyle='dotted', label='observations')
    plt.fill_between(np.arange(post.size), post + np.sqrt(cov), post - np.sqrt(cov), facecolor='grey', alpha=.5)


    plt.legend()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(name +  "OverallState")
    print name + "OVERALLSTATECREATED"

def create_scatterplot(t1, t2, name, xlabel="", ylabel=""):


    plt.figure()
    plt.scatter(t1, t2, label=xlabel + ylabel)

    plt.legend()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(name +  "Thetascompared")
    print name + "THETASCOMPAREDCREATED"


def create_paramplot(pri,post, name, xlabel="", ylabel=""):
    plt.figure(figsize=(10, 10))
    plt.plot(post, label='estimated DDF')
    plt.plot(pri, linestyle='dashed', label='prior estimated DDF')

    plt.legend()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(name + "overallParam")
    print name + "OVERALLPARAM CREATED"


#>>> Ensemble journies <<<#

def create_ens_stateplot(pri,post,obs,cov,name,xlabel="",ylabel=""):
    f, ax = plt.subplots(1, 2, figsize=(20, 10))
    for i in range(0, post.shape[0]):
        ax.flat[0].plot(post[i, :], color="grey", alpha=.5, label='Ensemble ' + str(i))
        ax.flat[1].plot(pri[i, :], linestyle='dashed', color="black", alpha=.5,
                        label='Ensemble ' + str(i) + ' Prior')

    ax.flat[0].plot(obs, linestyle='dotted', label='observations', color="green")
    ax.flat[1].plot(obs, linestyle='dotted', label='observations', color="green")

    ax.flat[0].plot(np.mean(post, axis=0), lw=4, label='mean', color="red")
    ax.flat[1].plot(np.mean(pri, axis=0), lw=4, label='mean', color="red")

    ax.flat[0].fill_between(np.arange(post[0,:].size), np.mean(post, axis=0) + np.sqrt(cov), np.mean(post, axis=0) - np.sqrt(cov), facecolor='grey', alpha=.5)
    ax.flat[1].fill_between(np.arange(post[0,:].size), np.mean(post, axis=0) + np.sqrt(cov), np.mean(post, axis=0) - np.sqrt(cov), facecolor='grey', alpha=.5)

    ax.flat[0].legend()
    ax.flat[1].legend()
    ax.flat[0].set_xlabel(xlabel)
    ax.flat[1].set_xlabel(xlabel)
    ax.flat[0].set_ylabel(ylabel + ' - Posterior')
    ax.flat[1].set_ylabel(ylabel + ' - Prior')
    plt.savefig(name + "EnsemblesState")
    print name + "ENSEMBLESSTATECREATED"

def create_ens_paramplot(pri,post, name, xlabel="", ylabel=""):
    f, ax = plt.subplots(1, 2, figsize=(20, 10))
    for i in range(0, post.shape[0]):
        ax.flat[0].plot(post[i, :], color="blue", alpha=.5, label='DDF Ensemble ' + str(i))
        ax.flat[1].plot(pri[i, :], linestyle='dashed', color="grey", alpha=.5,
                        label='DDF Ensemble' + str(i) + ' Prior')

    ax.flat[0].plot(np.mean(post, axis=0), lw=4, label='mean', color="red")
    ax.flat[1].plot(np.mean(pri, axis=0), lw=4, label='mean', color="red")

    ax.flat[0].legend()
    ax.flat[1].legend()
    ax.flat[0].set_xlabel(xlabel)
    ax.flat[1].set_xlabel(xlabel)
    ax.flat[0].set_ylabel(ylabel + ' - Posterior')
    ax.flat[1].set_ylabel(ylabel + ' - Prior')
    plt.savefig(name + "EnsemblesParam")
    print name + "ENSEMBLESPARAMCREATED"

def create_ens_paramplot_onlypost(pri,post, name, xlabel="", ylabel=""):
    plt.figure(figsize=(10, 10))
    for i in range(0, post.shape[0]):
        if i == 0:
            plt.plot(post[i, :], color="grey", alpha=.5, label='DDF Ensemble')
        else:
            plt.plot(post[i, :], color="grey", alpha=.5)

    plt.plot(np.mean(post, axis=0), lw=4, label='mean', color="red")

    plt.legend()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(name + "EnsemblesParamPost")
    print name + "ENSEMBLESPARAMPOSTCREATED"

#>>> Visualize node journies <<<#
def create_param_node_journeys(post,name, xlabel="", ylabel=""):

    font = {'family': 'normal',
            'weight': 'bold',
            'size': 18}
    matplotlib.rc('font', **font)

    number_of_plots = math.ceil(float(post.shape[0]) / 10.0)

    label = "DDF Nodes "

    f, ax = plt.subplots(int(math.ceil(float(number_of_plots)/2.0)), 2, figsize=(20, 20))

    for i in range(post.shape[0] / 10):
        begin = i * 10
        end = i * 10 + 10

        all_days = post[begin:end, :]
        for j in range(all_days.shape[0]):
            ax.flat[i].plot(all_days[j, :], label="Node " + str(j))
        ax.flat[i].set_ylabel(ylabel + ' Nodes ' + str(begin) + ' to ' + str(end))
        ax.flat[i].set_xlabel(xlabel)

    f.savefig(name + "nodejourneys")
    print name + "NODEJOURNEYSCREATED"

# Innovation graph
def create_innovation(innov_st, name, xlabel="", ylabel="", param_correction=False, innov_pr=None):
    plt.figure()

    if param_correction:
        plt.plot(innov_pr, color="purple", linestyle='dotted',
                 label="during parameter correction")

    plt.plot(innov_st, color="green", linestyle='dashed',
             label="during state correction")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.savefig(name + 'innovation')
    print name + "INNOVATIONCREATED"


def create_nrr_plot(nrr_arr, name = "NRR_timesteps"):


    plt.figure()
    for key, array in nrr_arr.iteritems():
        plt.plot(array, label='NRR: ' + key)

    plt.legend()
    plt.xlabel("Timesteps")
    plt.ylabel("NRR")
    plt.savefig(name)
    print name