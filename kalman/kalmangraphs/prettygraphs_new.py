import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import pickle, os, errno
from kalman import kalmanutils as kutils
import kgraphtypes as kgraphs

def main_prettygraphs_new(pri_t, post_t, pri_s, post_s, innov_pr, innov_st, obs, H_dict, pred_cov_dict, export_path = "pretty_plots_new", xlabel_time='Days starting at 9/1/2012', ylabels={}, plot_keys=[], nodes_to_plot=[0,40, 3], node_names={}):

    def plot_single_instance(pri_t_node, post_t_node, pri_s_node, post_s_node, obs_node, pred_cov_node, node_name, node_state="SWE", node_param_type="DDF", node_xlabel='Days starting at 9/1/2012', node_ylabel='Snow Water Equivalent (mm)', node_ylabel_param='DDF', plot_params=True):
        # Enter a folder of the node name
        kutils.enter_folder(node_name)

        # Plot node overall state
        kgraphs.create_stateplot(pri_s_node, post_s_node, obs_node, pred_cov_node, node_name + node_state, node_xlabel, node_ylabel)

        if plot_params:
            # Plot node overall param
            kgraphs.create_paramplot(pri_t_node, post_t_node, node_name + node_param_type, node_xlabel, node_ylabel_param)

        kutils.exit_folder()

    def plot_overall(pri_s_node, post_s_node, obs_node, innov_st_node, pred_cov_node, node_name, param_names= [], node_state = "swe", node_xlabel='Days starting at 9/1/2012', node_ylabel='Snow Water Equivalent (mm)', ens_list=[0,2]):
        # Enter a folder of the name of the node
        kutils.enter_folder(node_name)

        # Plot overall ensembles
        kgraphs.create_stateplot(np.mean(pri_s_node, axis=0), np.mean(post_s_node, axis=0), obs_node, pred_cov_node, node_name + node_state, node_xlabel, node_ylabel)

        # Plot all ensembles
        kgraphs.create_ens_stateplot(pri_s_node, post_s_node,obs_node, pred_cov_node, node_name + node_state, node_xlabel, node_ylabel)

        if len(param_names) > 0:
            # Plot innovation
            innov_pr_node = np.mean(innov_pr[node_state], axis=1)
            kgraphs.create_innovation(np.mean(innov_st_node, axis=0), name=node_name, xlabel=node_xlabel,
                                      ylabel="innovation", param_correction=True,
                                      innov_pr=np.mean(innov_pr_node, axis=0))

            for i, pname in enumerate(param_names):
                pri_t_node = np.mean(pri_t[pname], axis=1)
                post_t_node = np.mean(post_t[pname], axis=1)


                kgraphs.create_paramplot(np.mean(pri_t_node, axis=0), np.mean(post_t_node, axis=0),
                                         node_name + pname, node_xlabel, ylabels[pname])

                # Plot all param journies
                kgraphs.create_ens_paramplot_onlypost(pri_t_node, post_t_node, node_name + pname, node_xlabel,
                                                      ylabels[pname])

                kgraphs.create_param_node_journeys(np.mean(post_t[pname], axis=0), pname, xlabel_time,
                                                   ylabel=ylabels[pname])

                # Create correlation graphs
                for j, pname2 in enumerate(param_names):
                    if pname2[-1] != pname:
                        kgraphs.create_scatterplot(np.mean(post_t_node, axis=0), np.mean(np.mean(post_t[pname], axis=1), axis=0), node_name,
                                                   ylabels[pname], pname2)
        else:
            kgraphs.create_innovation(np.mean(innov_st_node, axis=0), name=node_name, xlabel=node_xlabel,
                                      ylabel="innovation")

        kutils.exit_folder()



    def plot_node(pri_s_node, post_s_node, obs_node, innov_st_node, pred_cov_node, node_name, param_names= [], node_i = None, node_state = "swe", node_xlabel='Days starting at 9/1/2012', node_ylabel='Snow Water Equivalent (mm)', ens_list=[0,2]):
        # Enter a folder of the name of the node
        kutils.enter_folder(node_name)

        # Plot overall ensembles
        kgraphs.create_stateplot(np.mean(pri_s_node, axis=0), np.mean(post_s_node, axis=0), obs_node, pred_cov_node, node_name + node_state, node_xlabel, node_ylabel)

        # Plot all ensembles
        kgraphs.create_ens_stateplot(pri_s_node, post_s_node,obs_node, pred_cov_node, node_name + node_state, node_xlabel, node_ylabel)

        if len(param_names) > 0:
            innov_pr_node = innov_pr[node_state][:, node_i, :]
            kgraphs.create_innovation(np.mean(innov_st_node, axis=0), name=node_name, xlabel=node_xlabel,
                                      ylabel="innovation", param_correction=True,
                                      innov_pr=np.mean(innov_pr_node, axis=0))

            for i, pname in enumerate(param_names):
                pri_t_node = pri_t[pname][:,node_i,:]
                post_t_node = post_t[pname][:,node_i,:]


                kgraphs.create_paramplot(np.mean(pri_t_node, axis=0), np.mean(post_t_node, axis=0),
                                         node_name + pname, node_xlabel, ylabels[pname])

                # Plot all param journies
                kgraphs.create_ens_paramplot_onlypost(pri_t_node, post_t_node, node_name + pname, node_xlabel,
                                                      ylabels[pname])


                # Create correlation graphs
                for j, pname2 in enumerate(param_names):
                    if pname2[-1] != pname:
                        kgraphs.create_scatterplot(np.mean(post_t_node, axis=0), np.mean(post_t[pname2][:,node_i,:], axis=0), node_name,
                                                   ylabels[pname], pname2)
        else:
            kgraphs.create_innovation(np.mean(innov_st_node, axis=0), name=node_name, xlabel=node_xlabel,
                                      ylabel="innovation")

        kutils.exit_folder()

    plt.style.use('seaborn-darkgrid')

    print "Beginning prettygraphs..."

    EXPORT_PATH = export_path

    plt.switch_backend('agg')

    if not node_names: # If no node names are provided, just use the node indicies.
        for node in nodes_to_plot:
            node_names[node] = node

    kutils.enter_folder(EXPORT_PATH)

    # Plot keys are arranged as follows: [["statename",["paramname1","paramname2"]],"statename_2","paramname_2"]... ["statename_i","paramname_i"]]
    for pair in plot_keys:

        kutils.enter_folder(pair[0])

        # These take forever to load
        nodes_arr_prs = H_dict[pair[0]].dot(pri_s[pair[0]])
        nodes_arr_pos = H_dict[pair[0]].dot(post_s[pair[0]])

        states_mean_pri = np.mean(nodes_arr_prs, axis=0)
        states_mean_post = np.mean(nodes_arr_pos, axis=0)

        # Variance for entire ensemble
        entire_ens_var = np.empty(pred_cov_dict[pair[0]].shape[2])
        individ_node_var = np.empty((pred_cov_dict[pair[0]].shape[1], pred_cov_dict[pair[0]].shape[2]))
        for t in range(pred_cov_dict[pair[0]].shape[2]):
            individ_node_var[:,t] = np.diag(pred_cov_dict[pair[0]][:,:,t])
            entire_ens_var[t] = np.mean(individ_node_var[:,t], axis=0)

        if len(pair) > 1: # One or more params is associated with this state
            if not isinstance(pair[1], (list,)): # Is there only one param?
                param_names = [pair[1]]
            else:
                param_names = pair[1]

        else:
            param_names = [] # No params for this state.


        plot_overall(states_mean_pri, states_mean_post, np.mean(obs[pair[0]], axis=1), np.mean(innov_st[pair[0]], axis=1), entire_ens_var, "all", param_names= param_names, node_state = pair[0], node_xlabel=xlabel_time, node_ylabel=ylabels[pair[0]])

        # Plot specified individual nodes
        for node in nodes_to_plot[pair[0]]:
            plot_node(nodes_arr_prs[node,:,:], nodes_arr_pos[node,:,:], obs[pair[0]][:,node], innov_st[pair[0]][:,node,:], individ_node_var[node,:], str(node_names[node]), node_i=node, param_names=param_names, node_state = pair[0], node_xlabel=xlabel_time, node_ylabel=ylabels[pair[0]])

        kutils.exit_folder()

    kutils.exit_folder()



