import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import pickle, os, errno
from kalman import kalmanutils as kutils
import kgraphtypes as kgraphs

def nrr_prettygraphs(nrr_arr, export_path = "nrr_plots"):

    plt.style.use('seaborn-darkgrid')

    print "Beginning nrr_prettygraphs..."

    EXPORT_PATH = export_path

    plt.switch_backend('agg')


    kutils.enter_folder(EXPORT_PATH)
    kgraphs.create_nrr_plot(nrr_arr)
    kutils.exit_folder()



