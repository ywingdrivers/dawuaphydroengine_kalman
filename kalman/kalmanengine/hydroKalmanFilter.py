#!/usr/bin/env python

# HydroKalmanFilter.py
# Main file for HydroKalmaFilter librarys
# --------------------
# William Cook 2017

import numpy as np
import time, datetime
from dateutil.parser import parse
from dateutil import tz
import matplotlib.pyplot as plt
import utils, json, os, errno, pickle
from kalman import kalmanutils as kvehicleutils
import multiprocessing as mp
import ctypes

plt.switch_backend('agg')

class Args:
    def __init__(self, kwargs):
        self.__dict__.update(kwargs)
print "THis file loads"

# Modified https://stackoverflow.com/questions/18516271/how-to-specify-a-local-working-directory-for-threading-thread-and-multiprocessin
# class Path_Thread(threading.Thread):
#     def __init__(self, path=None, group=None, target=None, name=None, args=(), kwargs={}, daemon=None):
#
#         super(Path_Thread, self).__init__(group=None, target=None, name=None, args=(), kwargs={})
#
#         if path is not None:
#             os.chdir(path)

class Kalman_Filter:
    """
        An Ensemble Kalman Filter.

        ====
        Math - Standard Kalman Filter
        ====

        **Update Steps (calculate priori from previous postori)**

        * *xpr* = Priori Estimate = :math:`A*xpo`
        * *Ppr* = Priori Error Cov. Matrix = :math:`A*Ppo*A^T + Q`

        **Correction Steps (calculate postori from the priori)**

        * *K* = Kalman Gain = :math:`(Pepr*C^T)/(C*Pepr*C^T+R)`
        * *xpo* = Postiori Estimate = :math:`xpr + K(y - Cxpr)`
        * *Ppo* = Postiori Error Cov Matrix = :math:`(I-K*C)*Ppr`

        ====
        Math - Ensemble Kalman Filter
        ====

        **Update Steps (calculate priori from previous postori)**

        {{ DO THIS }}

        **Correction Steps (calculate postori from the priori)**

        {{ DO THIS }}

    """

    # Init
    def __init__(self, f, Q_param, R, u_error, instances=50, timesteps=365, init_date = "09/1/2012", cooldown_steps=50, cov_timeframe = 5, parameter_file = "wm_param_files_test.json", generate_pickles = False):
        """
            :param f: Model Function - Should output geoTIF files
            :param Q_param: Parameter Error Matrix - Standard deviation of the parameters. {{FIX: Currently scalar value}}
            :param Q_state: State Error Matrix - Standard deviation of the states. {{FIX: Currently not used. Scalar}}
            :param R: Measurement Error Matrix - Amount of error for the observations. {{FIX: Currently scalar value}}
            :param u_error: Error matrix for U vars {{FIX: Currently scalar value}}
            :param instances: Number of instances in ensemble. Default 50.
            :param timesteps: Number of timesteps to undertake. Default 365.
            :param init_data: Date to begin model. {{FIX: Currently only pertains to this model}}
            :param cooldown_steps: Number of timesteps before the filter stops modifying parameters when there is no change in observed state.
            :param cov_timeframe: Number of timesteps the filter takes into account when calculating covariance.
        """

        # Set constant parameters
        self.TIMESTEPS = timesteps
        self.COOLDOWN_STEPS = cooldown_steps
        self.TIMEFRAME = cov_timeframe
        self.GENERATE_PICKLES = generate_pickles

        # Folder stuff
        self.ABS_PATH = os.getcwd()
        print "RUNNING IN: " + self.ABS_PATH
        self.DATA_FOLDER = "kalman_data/"
        self.PARAM_FILE = parameter_file
        self.DATA_FILE_SWE = "sweobs.json"
        self.RESULTS_FOLDER = "results" + str(int(time.time())) + "/"

        self.Q_param = Q_param
        #self.Q_state = Q_state # not used currently
        self.R_error = R # {{FIX: Currently a scalar}}
        self.u_error = u_error
        self.f = f  # function for running model
        self.INSTANCES = instances

        # Iterators
        self.cooldown = 0

        # Smoothing vectors - arbitrary values.
        self.param_a = .5
        self.param_h = 1


        # Base arguments for running model {{FIX: These should be an argument passed to the function. }}
        self.default_args = {
            #'init_date': "09/1/2012",  # Real date
            #'init_date': "12/25/2012",  # Development
            'init_date': init_date, # Production
            'params': "../" + self.DATA_FOLDER + self.PARAM_FILE,
            'network_file': "../" + self.DATA_FOLDER + "rivout.shp",
            'basin_shp': "../" + self.DATA_FOLDER + "subsout.shp",
            'restart': False}


        # Hardcoded u parameter files. {{FIX: Make this part of the code modular }}
        self.precip_file = self.DATA_FOLDER + "precip_F2012-09-01_T2013-08-31.nc"
        self.tempmin_file = self.DATA_FOLDER + "tempmin_F2012-09-01_T2013-08-31.nc"
        self.tempmax_file = self.DATA_FOLDER + "tempmax_F2012-09-01_T2013-08-31.nc"

        # These constants control the name of the folders containing split netCDF files {{FIX: Make this part of the code automatic }}
        self.PRECIP_FOLDER = "precip_tifs"
        self.TMIN_FOLDER = "tmin_tifs"
        self.TMAX_FOLDER = "tmax_tifs"


        # Split netCDF files. {{FIX: Make this part of the code modular }}
        self.hbv_pars = {}
        with open(self.DATA_FOLDER + self.PARAM_FILE) as json_file: # Get initial values of parameters we will be modifying (theta). Saved as GEOTIFF. This file is specified in the arguments.
            self.pars = json.load(json_file)
            for key, value in self.pars.items():
                self.hbv_pars[key] = utils.RasterParameterIO(self.DATA_FOLDER + value, 1)

        # Get SWE observations
        if self.DATA_FILE_SWE:
            with open(self.DATA_FOLDER + self.DATA_FILE_SWE, 'r') as f:
                self.swe_obs_json = json.load(f)

        # H matrix # {{FIX: Eventually we may have different H matricies }}
        self.H = None
        self._prepare_H_matrix_()

        # Place first row of obs in self.y_obs for the preparation of H and THIESSEN. {{FIX: Eventually we will have multiple obs. They should just be numpy arrays }}
        self.y_obs = np.empty((self.H.shape[0], self.TIMESTEPS))
        self._update_obs_(self.default_args['init_date'], 0)

        # THIESSEN matrix # {{FIX: Once again, this should probably not be hardcoded in __init__ since it only applies to SWE. }}
        self.THIESSEN = None
        self._prepare_THIESSEN_matrix_()

        # K matricies
        self.K_dict_param = {"swe": np.empty((self.INSTANCES,self.H.shape[0],self.H.shape[0]))}
        self.K_dict_state = {"swe": np.empty((self.INSTANCES, self.H.shape[1],self.H.shape[0]))}

        # Saving Cov. Matricies # {{ RE_EVALUATE: Not sure we should save all of these. It takes up alot of memory. Mainly useful for debugging. }}
        self.pr_cross_cov_param = {"swe": np.empty((self.H.shape[0],self.H.shape[0],self.TIMESTEPS))}
        self.pr_cross_cov_state = {"swe": np.empty((self.H.shape[1],self.H.shape[0],self.TIMESTEPS))}
        self.pr_cov_y = np.empty((self.H.shape[0], self.H.shape[0]))

        # Parameter arrays. These have an extra slot for the initial estimantes. {{FIX: These are currently hardcoded. They should be based off of their parameter names in self.PARAM_FILE }}
        self.dict_en = {"swe": np.empty((self.INSTANCES, self.H.shape[1], self.TIMESTEPS + 1))  # DDF
                        }

        # Set initial value for dict_en
        # self.dict_en["swe"][:, :, 0] = utils.RasterParameterIO(self.pars["ddf"], 1).array.flatten() # {{FIX: This could be modularized }}


        # Holds all posterior values for state
        self.xpr_dict = {"swe": np.empty((self.INSTANCES, self.H.shape[1], self.TIMESTEPS))} # {{FIX: This could be modularized }}

        # Holds all posterior values for parameters
        self.xpo_dict_param = {"swe": np.empty((self.INSTANCES, self.H.shape[0], self.TIMESTEPS+1))} # {{FIX: This could be modularized }}

        # Holds final posterior param values
        self.xpo_dict_param_final = {"swe": np.empty((self.INSTANCES, self.H.shape[1], self.TIMESTEPS+1))} # {{FIX: This could be modularized }}

        # Set initial value for dict_en and xpr_dict_param_final {{FIX: This could be modularized }}
        self.dict_en["swe"][:, :, 0] = utils.RasterParameterIO(self.pars["ddf"], 1).array.flatten()
        self.xpo_dict_param_final["swe"][:, :, 0] = self.dict_en["swe"][:, :, 0]
        self.xpo_dict_param["swe"][:, :, 0] = self.dict_en["swe"][:, :, 0].dot(self.H.T)

        # Dict that holds posterior states. {{FIX: This could be modularized }}
        self.xpo_dict = {"swe": np.empty((self.INSTANCES, self.H.shape[1], self.TIMESTEPS))}

        # Dict that holds perturbed Y observations per instance. {{FIX: This could be modularized }}
        self.y_obs_perturbed = np.empty((self.H.shape[0], self.INSTANCES, self.TIMESTEPS))

        # take the precip/temp netCDF files, split them into singular files. Save arrays, reshape so vectors are in same dimension. {{FIX: This could be modularized }}
        self.u_YEAR_START = "09/1/2012"
        self.precip_arr = self._split_netcdf_(self.precip_file, self.PRECIP_FOLDER).reshape((365, self.H.shape[1]))
        self.tmin_arr = self._split_netcdf_(self.tempmin_file, self.TMIN_FOLDER).reshape((365, self.H.shape[1]))
        self.tmax_arr = self._split_netcdf_(self.tempmax_file, self.TMAX_FOLDER).reshape((365, self.H.shape[1]))

        # Get number of timesteps that were skipped
        self.precip_offset = (parse(self.default_args['init_date']) - parse(self.u_YEAR_START)).days

        # RasterIO object - used for its writeToGEOTIFF() functionality.
        self.precip_rio = utils.RasterParameterIO(self.PRECIP_FOLDER + "/" + self.PRECIP_FOLDER + str(0) + ".tif", 1) # Used as a global RasterParameterIO object after load

        # Shared memory - Creates arrays in c code that can be used concurrently by parallel instances.
        self.shared_arr_ddf = mp.Array(ctypes.c_double, self.INSTANCES * self.H.shape[1])
        self.shared_arr_swe = mp.Array(ctypes.c_double, self.INSTANCES * self.H.shape[1])
        self.shared_arr_uvars = mp.Array(ctypes.c_double, self.INSTANCES * self.H.shape[1] * 3)

        # DEBUG info
        self.innovation_arr = np.zeros((self.H.shape[0], self.INSTANCES, self.TIMESTEPS))
        self.pre_innovation_arr = np.zeros((self.H.shape[0], self.INSTANCES, self.TIMESTEPS))
        self.uvars_arr = np.empty((self.INSTANCES, self.H.shape[1], self.TIMESTEPS, 3))
        self.xpr_dict_pre = {"swe": np.empty((self.INSTANCES, self.H.shape[1]))}

    ####>>> SYSTEM METHODS <<<####

    def __create_folder__(self, path):
        """
            Creates a directory at 'path'. Does nothing if it doesn't exist.
            :param path: directory name
        """

        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    def __enter_folder__(self, path):
        """
            Enters a folder. Creates one if it doesn't exist.
            :param path: directory to navigate to
        """
        # Create directory if it doesn't exist
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
        # Move to directory
        os.chdir(path)

    def __exit_folder__(self):
        """
            Exits a folder.
        """
        os.chdir("..")

    ####>>> INIT FUNCTIONS <<<####

    # {{ FIX: Make all y_obs numpy arrays given to the filter. }}
    def _update_obs_(self, date, t):
        """
            Gets observation points at a given date, stores them in global variable self.y_obs
            :param date: mm/dd/yyyy (same as init_date argument for hydrovehicle)
            :param t: timestep
        """

        # Convert date to local timestamp (FIX THIS IN THE JSON)
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('America/Denver')
        utc_date = parse(date).replace(tzinfo=from_zone)
        local_date = utc_date.astimezone(to_zone)

        unix_time = unicode(str(int(time.mktime((local_date).timetuple())) * 1000), "utf-8")
        print unix_time

        for ind, (key, node) in enumerate(self.swe_obs_json.items()):

            if unix_time in node['values']:
                self.y_obs[ind, t] = node['values'][unix_time]['values']/0.039370078740158 # inches to mm

            else:
                print "MISSING VALUE"
                self.y_obs[ind, t] = None

    def _prepare_H_matrix_(self):
        """
            Creates global matrix mask self.H
            {{FIX: Currently only does this for SWE observation nodes. }}
        """

        # Longitude/latitude arrays corresponding to model raster files
        lon, lat = self.hbv_pars['ddf'].affine * np.indices(self.hbv_pars['ddf'].array.T.shape)

        # Create swe observation vector from swe_JSON
        pos_obs = []
        pos_obs_pixels = []
        for key, node in self.swe_obs_json.items():
            pos_obs.append([float(node[u'long']), float(node[u'lat'])])

        ind = [~self.hbv_pars['ddf'].affine * x for x in pos_obs]

        for i, pos in enumerate(ind):
            H_row = np.zeros_like(self.hbv_pars['ddf'].array)
            H_row[int(round(ind[i][1])), int(round(ind[i][0]))] = 1

            if i == 0:  # First iteration, create matrix
                self.H = np.array(H_row.flatten())
            else:  # Append to matrix
                self.H = np.vstack((self.H, H_row.flatten()))

    def _prepare_THIESSEN_matrix_(self):
        """
            Uses kalman_data/THIESSENRaster.tif and turns it into an array that maps stations to raster points.
            Result stored in global variable self.THIESSEN.
        """
        TH = utils.RasterParameterIO("kalman_data/THIESSENRaster.tif", 1).array

        # Replace station ID with row
        row = 0
        for key in self.swe_obs_json.iterkeys():
            TH[TH == int(key)] = row
            row += 1

        self.THIESSEN = TH

    # {{ FIX: Make this modular }}
    def _grab_u_var_files(self, t):
        """
        Obtains all initial TIF files and stores them into time 0 on the posterior arrays
        :param t: timestep from which to grab from
        :return: None
        """
        self.precip_rio = utils.RasterParameterIO(self.PRECIP_FOLDER + "/" + self.PRECIP_FOLDER + str(t) + ".tif", 1)
        self.tmin_rio = utils.RasterParameterIO(self.TMIN_FOLDER + "/" + self.TMIN_FOLDER + str(t) + ".tif", 1)
        self.tmax_rio = utils.RasterParameterIO(self.TMAX_FOLDER + "/" + self.TMAX_FOLDER + str(t) + ".tif", 1)

    ####>>> GENERAL FUNCTIONS <<<####

    def _split_netcdf_(self, path, name):
        """
            Takes a 3D netcdf file, creates a folder of 2D geotiff files
            :return: netCDF array object
        """
        netCDF = utils.RasterParameterIO(path)
        #array = netCDF.array.clip(min=0)
        array = netCDF.array
        self.__enter_folder__(name)
        for i in range(array.shape[0]):
            netCDF.write_array_to_geotiff(name + str(i) + ".tif", array[i, :, :])
        self.__exit_folder__()

        return array

    def _prepare_member_(self):
        """
            Copies all hbv parameter files into the current folder for model ingestion.
        """
        for key, value in self.hbv_pars.items():
            value.write_array_to_geotiff(self.pars[key], value.array)

    def _add_noise_to_param_(self, rand_function, init_arr, save_path, v=.1, no_negatives=False, clip=0, filter_unknowns = True):

        if filter_unknowns == True:
            unknown_indexes = np.where(init_arr == -32767)[0]

        """
            Adds noise to a given .tif file using callback rand_function.

            :param rand_function: callback to add noise. Currently set up for np.random.normal().
            :param init_arr: Array to add noise to and save as a param
            :param save_path: name of the new GEOTIF file.
            :return: Returns array that was written to the GEOTIF file
        """
        arr = kvehicleutils.add_noise_to_array(rand_function, init_arr, v, no_negatives, clip)
        arr = np.reshape(arr,self.precip_rio.array.shape) # Change back to 2D array

        if filter_unknowns == True:
            np.put(arr, unknown_indexes, -32767)

        self.precip_rio.write_array_to_geotiff(save_path, arr)
        return arr

    def _add_scalar_to_param_(self, rand_function, init_arr, save_path, v=.1, no_negatives=False, clip=0, filter_unknowns = True):
        """
        :param rand_function: random function (currently tested with np.random.normal)
        :param init_arr: Array to add scalar to
        :param save_path: Path to save geotiff to
        :param v: Variance
        :param no_negatives: Removes negative entries
        :param clip: If no_negatives is chosen, this can be set to make the array clip at something other then 0.
        :param filter_unknowns: Preserve unknown values recorded as -32767
        :return: Modified numpy array

        Adds a randomly generated scalar to all elements of the given array
        """
        if filter_unknowns == True:
            unknown_indexes = np.where(init_arr == -32767)[0]

        """
            Adds noise to a given .tif file using callback rand_function.

            :param rand_function: callback to add noise. Currently set up for np.random.normal().
            :param init_arr: Array to add noise to and save as a param
            :param save_path: name of the new GEOTIF file.
            :return: Returns array that was written to the GEOTIF file
        """
        rand_num = rand_function(0, v)
        arr = init_arr + rand_num
        arr = np.reshape(arr,self.precip_rio.array.shape) # Change back to 2D array


        if no_negatives == True:
            arr = arr.clip(min=clip)

        if filter_unknowns == True:
            np.put(arr, unknown_indexes, -32767)

        self.precip_rio.write_array_to_geotiff(save_path, arr)

        return arr

    def _params_to_thiessen_(self, param_array):
        """
        :param param_array: Array of 90 observations
        :return:
        """
        newParamRaster = np.copy(self.THIESSEN)
        for ind, val in enumerate(param_array):
            newParamRaster[newParamRaster == ind] = val

        return newParamRaster

    ###>>> MAIN FUNCTIONS <<<###

    def _run_ensemble_kalman_filter_timesteps_(self, save_pickles=False):
        """
        Main protected method for running ensemble Kalman filter.
        Runs t iterations starting at the init_date.
        :param save_pickles: If true, saves pickle files in 'final_params' folder. WARNING: These files are really big.
        """

        resume_model = False
        for t in range(0, self.TIMESTEPS):

            if t > 0:
                resume_model = True  # Use pickled states.

            self._run_ensemble_kalman_filter_(t, resume_model)


        # The final results will be saved in the following folder:
        self.__enter_folder__("final_params")
        self._generate_final_rasters_()

        if save_pickles == True:
            self._generate_final_pickles_()
        self.__exit_folder__()

    ####>>> CHILD FUNCTIONS <<<####

    def _run_ensemble_kalman_filter_(self, t, resume_model=False):
        """
            Runs Ensemble Kalman Filter over 1 timestep. Main child function.

            :param t: Timestep to run.
            :param resume_model: Should the model load previous states?
        """

        # New arguments for the current day
        current_args = dict(self.default_args)

        # Calculate the date at the current timestep
        the_day = (parse(self.default_args['init_date']) + t * datetime.timedelta(seconds=86400)).strftime("%m/%d/%Y")
        print "Beginning " + the_day


        # Update current arguments for this date
        current_args['init_date'] = the_day
        current_args['precip'] = "precipmod.tif"  # U parameter + noise
        current_args['tmin'] = "tempminmod.tif"  # U parameter + noise
        current_args['tmax'] = "tempmaxmod.tif"  # U parameter + noise
        current_args['restart'] = resume_model
        args_obj = Args(current_args) # This Args object is what will be passed to the model (not current_args)

        # Grab this timestep's observations and create perterbed instances for each ensemble
        self._update_obs_(the_day, t) # {{FIX: This should just be a numpy array - unless we want to 'update' the filter}}
        self.y_obs_perturbed[:,:,t] = kvehicleutils.add_noise_to_array_instances(np.random.normal, self.y_obs[:, t], self.INSTANCES, scale=self.R_error, no_negatives=True)  # y_obs with random errors

        ##>> IF/ELSE - Only filter state if observations do not change. {{FIX: Current hardcoded stuff only applicable to SWE }} <<##
        if self.y_obs[:,t].max() == 0 and self.cooldown < 1:
        #if 1 == 0:

            # Copy last timestep's parameters to current timestep
            self.dict_en["swe"][:,:,t+1] = self.dict_en["swe"][:,:,t]
            self.xpo_dict_param["swe"][:, :, t + 1] = self.xpo_dict_param["swe"][:, :, t]
            self.xpo_dict_param_final["swe"][:, :, t + 1] = self.xpo_dict_param_final["swe"][:, :, t]

            # Run models in parallel!
            process_list = []
            for i in range(0, self.INSTANCES):
                p = mp.Process(name=str(i), target=self._run_model_get_state_u_perturb_, args=(i, args_obj, t, np.random.normal, resume_model))
                process_list.append(p)
                p.start()

            # Join all processes
            for i in range(0, self.INSTANCES):
                process_list[i].join(100)  # Return anyway if a process takes more then 100 seconds (is frozen)

        else:
            # Increment or reset cooldown
            if self.y_obs[:,t].max() == 0:
                self.cooldown = self.cooldown - 1
            else:
                self.cooldown = self.COOLDOWN_STEPS

            # Parallel Processes
            process_list = []
            for i in range(0, self.INSTANCES):
                p = mp.Process(name=str(i), target=self._member_predict_mp_wrapper, args=(np.random.normal, i, t, args_obj, resume_model))
                process_list.append(p)
                p.start()

            # Join all processes
            for i in range(0, self.INSTANCES):
                process_list[i].join(100) # Return anyway if a process takes more then 100 seconds (is frozen)

            # Get shared memory
            self.dict_en["swe"][:, :, t + 1] = np.frombuffer(self.shared_arr_ddf.get_obj()).reshape((self.INSTANCES, self.H.shape[1]))
            self.xpr_dict["swe"][:, :, t] = np.frombuffer(self.shared_arr_swe.get_obj()).reshape((self.INSTANCES, self.H.shape[1]))

            print "Max,Min DDF: "
            print self.dict_en["swe"][:,:,t+1].max()
            print self.dict_en["swe"][:,:,t+1].min()
            print self.dict_en["swe"][:,:,t+1].argmax()
            print self.dict_en["swe"][:,:,t+1].argmin()
            print "Max,Min SWE: "
            print self.xpr_dict["swe"][:,:,t].max()
            print self.xpr_dict["swe"][:,:,t].min()
            print self.xpr_dict["swe"][:,:,t].argmax()
            print self.xpr_dict["swe"][:,:,t].argmin()

            # Calculate parameter cross covariance, K

            # $$$$$$$$$$$
            # $ OLD WAY $
            # $$$$$$$$$$$

            #self._calculate_param_cov_mat_(t) # Old code

            # OVVER ENSSEMBLE
            # self.pr_cov_y = np.cov(self.H.dot(self.xpr_dict["swe"][:,:,t].T))
            # print "COV Y MAX/MIN"
            # print self.pr_cov_y["swe"][:, :, t].max()
            # print self.pr_cov_y["swe"][:, :, t].min()
            # print "#####\n\n"
            # # Over time
            # if t < self.TIMEFRAME:
            #     self.pr_cov_y = np.cov(self.H.dot(self.xpr_dict["swe"][:, :, t].T))
            # else:
            #     self.pr_cov_y = np.cov(self.H.dot(np.mean(self.xpr_dict["swe"][:, :, t - self.TIMEFRAME:t], axis=0)))
            # 

            # OLD COV CODE
            # self.pr_cov_y = self._calculate_cross_cov_(self.xpr_dict["swe"][:, :, t].dot(self.H.T),
            #                                            self.xpr_dict["swe"][:, :, t].dot(self.H.T))
            # self.pr_cross_cov_state["swe"][:, :, t] = self._calculate_cross_cov_(self.dict_en["swe"][:,:,t+1], self.xpr_dict["swe"][:, :, t].dot(self.H.T))


            # Update params - collective
            self.pr_cov_y = self._calculate_cross_cov_sum_(self.xpr_dict["swe"][:, :, t].dot(self.H.T),
                                                       self.xpr_dict["swe"][:, :, t].dot(self.H.T))
            self.pr_cross_cov_param["swe"][:, :, t] = self._calculate_cross_cov_sum_(self.dict_en["swe"][:,:,t+1].dot(self.H.T), self.xpr_dict["swe"][:, :, t].dot(self.H.T))

            self._calculate_K_param_(t)
            self._parameter_update_(t)

            # Update params - individual
            # for i in range(0, self.INSTANCES):
            #     self._calculate_K_param_individual_(t,i)
            #     self._parameter_update_individual_(t,i)

            # Update param files
            for i in range(self.INSTANCES):
                self.__enter_folder__(str(i))
                self._update_param_rasters_(i,t)
                self.__exit_folder__()

            # Reset Pickles (may not be nessesary)
            if t != 0:
                for i in range(self.INSTANCES):
                    self.__enter_folder__(str(i))
                    self._update_pickled_states_(i, t-1)
                    self.__exit_folder__()

            # Run models again!
            process_list = []
            for i in range(0, self.INSTANCES):
                p = mp.Process(name=str(i), target=self._run_model_get_state_, args=(i, args_obj))
                process_list.append(p)
                p.start()

            # Join all processes
            for i in range(0, self.INSTANCES):
                process_list[i].join(100) # Return anyway if a process takes more then 100 seconds (is frozen)

        # Get shared memory
        self.xpr_dict_pre["swe"] = self.xpr_dict["swe"][:, :, t]
        self.xpr_dict["swe"][:, :, t] = np.frombuffer(self.shared_arr_swe.get_obj()).reshape((self.INSTANCES, self.H.shape[1]))
        self.uvars_arr[:, :, t, :] = np.frombuffer(self.shared_arr_uvars.get_obj()).reshape(
            (self.INSTANCES, self.H.shape[1], 3))

        # if np.array_equal(self.xpr_dict_pre["swe"], self.xpr_dict["swe"][:,:,t]):
        #     print "ARRRRGH.\n!\n!\n!\n!\n!!\n!!!!!"

        # Over ensemble
        # self.pr_cov_y = np.cov(self.H.dot(self.xpr_dict["swe"][:, :, t].T))
        # self.pr_cov_y = self._calculate_cross_cov_(self.xpr_dict["swe"][:,:,t].dot(self.H.T), self.xpr_dict["swe"][:,:,t].dot(self.H.T))
        # print "COV Y MAX/MIN (PT2)"
        # print self.pr_cov_y.max()
        # print self.pr_cov_y.min()
        # print "#####\n\n"
        # Over time
        # if t < self.TIMEFRAME:
        #     self.pr_cov_y = np.cov(self.H.dot(self.xpr_dict["swe"][:, :, t].T))
        # else:
        #     self.pr_cov_y = np.cov(self.H.dot(np.mean(self.xpr_dict["swe"][:, :, t - self.TIMEFRAME:t], axis=0)))


        # Calculate state covariance, K
        #self._calculate_state_cov_mat_(t) # Old code


        # OLD COV CODE
        # self.pr_cov_y = self._calculate_cross_cov_(self.xpr_dict["swe"][:, :, t].dot(self.H.T),
        #                                            self.xpr_dict["swe"][:, :, t].dot(self.H.T))
        # self.pr_cross_cov_state["swe"][:, :, t] = self._calculate_cross_cov_(self.xpr_dict["swe"][:,:,t], self.xpr_dict["swe"][:,:,t].dot(self.H.T))

        # Update States Collectively
        self.pr_cov_y = self._calculate_cross_cov_sum_(self.xpr_dict["swe"][:, :, t].dot(self.H.T),
                                                   self.xpr_dict["swe"][:, :, t].dot(self.H.T))
        self.pr_cross_cov_state["swe"][:, :, t] = self._calculate_cross_cov_(self.xpr_dict["swe"][:,:,t], self.xpr_dict["swe"][:,:,t].dot(self.H.T))

        self._calculate_K_state_(t)
        self._member_update_(t)

        # Update States Individual
        # for i in range(0, self.INSTANCES):
        #     self._calculate_K_state_individual_(t, i)
        #     self._member_update_individual_(t, i)

        print "SWE MAX/MIN before"
        print self.xpr_dict["swe"][:,:,t].max()
        print self.xpr_dict["swe"][:,:,t].min()

        print "SWE MAX/MIN afterwards"
        print self.xpo_dict["swe"][:,:,t].max()
        print self.xpo_dict["swe"][:,:,t].min()

        # Update files
        for i in range(0, self.INSTANCES):

            self.__enter_folder__(str(i))
            self._update_pickled_states_(i,t)
            self.__exit_folder__()

    def _member_predict_mp_wrapper(self, rand_function, instance, t, args_obj, resume_model):
        """
            Wrapper for _member_predict_(). This function is redundant and should be combined with _member_predict_()
            :param rand_function: callback to function that generates white nosie. Right now this is assumed to be np.random.normal
            :param instance: the instance this Kalman filter is. Currently not used.
            :param t: Timestep of the model, with 0 being init_date and 364 being the last timestep.
            :param args_obj: Args() object containing all info needed for the model.
            :return: All priori varaibles as numpy arrays.
        """


        self.__enter_folder__(str(instance))
        print 'Starting:', mp.current_process().name
        if not resume_model:  # Reset all parameters
            self._prepare_member_()

        self._member_predict_(rand_function, instance, t, args_obj)
        self.__exit_folder__()
        print 'Exiting :', mp.current_process().name

    def _member_predict_(self, rand_function, instance, t, args_obj):
        """
            This function runs the model. Before doing so it changes the u_variables and the theta variables.
            This function is used for the initial run before the parameter correction.
            Runs per ensemble in parallel.
            :param rand_function: callback to function that generates white noise. Right now this is assumed to be np.random.normal
            :param instance: the instance this Kalman filter is.
            :param t: Timestep of the model, with 0 being init_date and 364 being the last timestep.
            :param args_obj: Args() object containing all info needed for the model.
            :return: Nothing

        """

        # This makes sure these random numbers are different from the other parallel processes
        np.random.seed()

        # File name
        cur_date = (parse(args_obj.init_date)).strftime("%Y%m%d")
        swe_file_name = "swe_" + cur_date

        np_wrapper_ddf_flat = np.frombuffer(self.shared_arr_ddf.get_obj())
        np_wrapper_ddf = np_wrapper_ddf_flat.reshape((self.INSTANCES, self.H.shape[1]))

        #SWE
        np_wrapper_swe_flat = np.frombuffer(self.shared_arr_swe.get_obj())
        np_wrapper_swe = np_wrapper_swe_flat.reshape((self.INSTANCES, self.H.shape[1]))
        if t != 0:
            np_wrapper_swe[instance, :] = self.xpo_dict["swe"][instance,:,t-1] # Store older info in array just in case this process freezes (rare)


        # Obtain mean of parameters noise
        noise_param = .97
        a_param = (noise_param*3 - 1)/(2*noise_param)
        h_param = 1 # Kernel Smoothing (?)

        # ddf_mean = np.mean(self.xpo_dict_param_final["swe"][:, :, t], axis=0)
        # ddf_mixed = a_param*self.xpo_dict_param_final["swe"][instance,:,t] + (1-a_param)* ddf_mean
        # ddf_var = np.var(self.dict_en["swe"][instance, :, 0:t+1], axis=1)*h_param**2
        #
        # ddf_var_final = self.Q_param * h_param # Can be scalar or matrix
        #
        # # Add Parameter Noise
        # np_wrapper_ddf[instance,:] = self._add_noise_to_param_(rand_function,
        #                             ddf_mixed,
        #                             self.pars["ddf"],
        #                             v=ddf_var_final, no_negatives=True, clip=1).flatten()


        # Now generating a THESIEN
        ddf_mean = np.mean(self.xpo_dict_param["swe"][:, :, t], axis=0)
        ddf_mixed = a_param*self.xpo_dict_param["swe"][instance,:,t] + (1-a_param)* ddf_mean

        ddf_var_final = self.Q_param * h_param # Can be scalar or matrix

        # Add Parameter Noise
        np_wrapper_ddf[instance,:] = self._add_noise_to_param_(rand_function,
                                    self._params_to_thiessen_(ddf_mixed),
                                    self.pars["ddf"],
                                    v=ddf_var_final, no_negatives=True, clip=1).flatten()


       # print np_wrapper_ddf[instance,:]

        # Calculate U var variances
        # precip_var = np.var(self.precip_arr[0:t+1, :], axis=0)
        # tmin_var = np.var(self.tmin_arr[0:t+1, :], axis=0)
        # tmax_var = np.var(self.tmax_arr[0:t+1, :], axis=0)
        # Add noise for U vars

        # Add noise for U vars
        pm = self._add_scalar_to_param_(rand_function, self.precip_arr[t+self.precip_offset, :], "precipmod.tif", self.u_error, no_negatives=True)
        tmn = self._add_scalar_to_param_(rand_function, self.tmin_arr[t+self.precip_offset, :], "tempminmod.tif", self.u_error, no_negatives=True)
        tmx = self._add_scalar_to_param_(rand_function, self.tmax_arr[t+self.precip_offset, :], "tempmaxmod.tif", self.u_error, no_negatives=True)

        # DEBUG: Save perturbed params
        np_wrapper_u_flat = np.frombuffer(self.shared_arr_uvars.get_obj())
        np_wrapper_u = np_wrapper_u_flat.reshape((self.INSTANCES, self.H.shape[1], 3))
        np_wrapper_u[instance, :, 0] = pm.flatten()
        np_wrapper_u[instance, :, 1] = tmn.flatten()
        np_wrapper_u[instance, :, 2] = tmx.flatten()

        self.run_model(args_obj)

        # Get model output

        # Code from before paralell processing
        # self.xpr_dict["swe"][instance,:,t] = utils.RasterParameterIO(swe_file_name + ".tif").array[0, :, :].flatten()

        np_wrapper_swe[instance, :] = utils.RasterParameterIO(swe_file_name + ".tif").array[0, :, :].flatten()

    def _run_model_get_state_(self, instance, args_obj):
        """
        This function runs the model. It uses all pre_existing info to run it (no theta or u variables are perturbed.)
        This function is used for the 2nd run of the model after parameter corrections have been made.
        Runs per ensemble in parallel.
        :param instance: instance of the ensemble to run
        :param args_obj: Argument object
        :return: Nothing
        """
        self.__enter_folder__(str(instance))

        # This makes sure these random numbers are different from the other parallel processes
        np.random.seed()

        # File name
        cur_date = (parse(args_obj.init_date)).strftime("%Y%m%d")
        swe_file_name = "swe_" + cur_date

        #SWE
        np_wrapper_swe_flat = np.frombuffer(self.shared_arr_swe.get_obj())
        np_wrapper_swe = np_wrapper_swe_flat.reshape((self.INSTANCES, self.H.shape[1]))
        # np_wrapper_swe[instance, :] = utils.RasterParameterIO(swe_file_name + ".tif").array[0, :, :].flatten() # Store older info in array just in case this process freezes (rare)


        self.run_model(args_obj)

        # Get model output


        # Code from before paralell processing
        # self.xpr_dict["swe"][instance,:,t] = utils.RasterParameterIO(swe_file_name + ".tif").array[0, :, :].flatten()
        np_wrapper_swe[instance, :] = utils.RasterParameterIO(swe_file_name + ".tif").array[0, :, :].flatten()
        self.__exit_folder__()
        print 'Exiting :', mp.current_process().name

    def _run_model_get_state_u_perturb_(self, instance, args_obj, t, rand_function, resume_model):
        """
        This function runs the model. Before doing so it changes the u_variables.
        This function is used when ONLY the state kalman filter is operating (no parameters.)
        Runs per ensemble in parallel.
        :param instance: instance of the ensemble to run
        :param args_obj: Argument object
        :param t: Timestep
        :param rand_function: Random function used to generate noise (usually np.random.normal)
        :param resume_model: True if this is NOT the first timestep.
        :return: Nothing
        """
        self.__enter_folder__(str(instance))

        # This makes sure these random numbers are different from the other parallel processes
        np.random.seed()

        if not resume_model:  # Reset all parameters
            self._prepare_member_()


        # File name
        cur_date = (parse(args_obj.init_date)).strftime("%Y%m%d")
        swe_file_name = "swe_" + cur_date

        # Add noise for U vars
        pm = self._add_scalar_to_param_(rand_function, self.precip_arr[t+self.precip_offset, :], "precipmod.tif", self.u_error, no_negatives=True)
        tmn = self._add_scalar_to_param_(rand_function, self.tmin_arr[t+self.precip_offset, :], "tempminmod.tif", self.u_error, no_negatives=True)
        tmx = self._add_scalar_to_param_(rand_function, self.tmax_arr[t+self.precip_offset, :], "tempmaxmod.tif", self.u_error, no_negatives=True)

        # DEBUG: Save perturbed params
        np_wrapper_u_flat = np.frombuffer(self.shared_arr_uvars.get_obj())
        np_wrapper_u = np_wrapper_u_flat.reshape((self.INSTANCES, self.H.shape[1], 3))
        np_wrapper_u[instance, :, 0] = pm.flatten()
        np_wrapper_u[instance, :, 1] = tmn.flatten()
        np_wrapper_u[instance, :, 2] = tmx.flatten()

        #SWE
        np_wrapper_swe_flat = np.frombuffer(self.shared_arr_swe.get_obj())
        np_wrapper_swe = np_wrapper_swe_flat.reshape((self.INSTANCES, self.H.shape[1]))
        if t != 0:
            np_wrapper_swe[instance, :] = self.xpo_dict["swe"][instance,:,t-1] # Store older info in array just in case this process freezes (rare)


        self.run_model(args_obj)

        # Get model output


        # Code from before paralell processing
        # self.xpr_dict["swe"][instance,:,t] = utils.RasterParameterIO(swe_file_name + ".tif").array[0, :, :].flatten()
        np_wrapper_swe[instance, :] = utils.RasterParameterIO(swe_file_name + ".tif").array[0, :, :].flatten()
        self.__exit_folder__()
        print 'Exiting :', mp.current_process().name

    # def _calculate_param_cov_mat_(self, t):
    #     """
    #         Takes all ensemble instances, calculates error covariance
    #         :param t: Timestep
    #         :return: None
    #     """
    #
    #     for key, xpr_state in self.xpr_dict.items():
    #         # xprH = xpr_state[:,:,t].dot(self.H.T)
    #         # ddfH = self.H.dot(self.dict_en[key][:,:,t].T)
    #         # xprH = np.mean(xpr_state[:,:,:t+1], axis=2).dot(self.H.T)
    #         # ddfH = self.H.dot(np.mean(self.dict_en[key][:,:,:t+1], axis=2).T)
    #
    #         xprH = xpr_state[:,:,t].dot(self.H.T)
    #         ddfH = self.H.dot(self.dict_en[key][:,:,t+1].T).T
    #
    #
    #         # Calculate mean of values
    #         # Calculate via time
    #         meanX = np.mean(xprH, axis=0)
    #         meanY = np.mean(ddfH, axis=0)
    #
    #         X = np.array(xprH - meanX)
    #         Y = np.array(ddfH - meanY)
    #
    #
    #         # self.pr_cross_cov_param[key] = np.dot(X.T,X) * 1./(self.INSTANCES-1)
    #         self.pr_cross_cov_param[key][:,:,t] = np.dot(Y.T, X) * 1. / (self.INSTANCES - 1.)

            # Add Q error
            #self.pr_cross_cov_param[key][:, :, t] = self.pr_cross_cov_param[key][:, :, t] + np.diag(np.full_like(self.pr_cross_cov_param[key][0, :, t], self.Q_state))

    # def _calculate_state_cov_mat_(self, t):
    #     """
    #         Takes all ensemble instances, calculates error covariance
    #         :param t: Timestep
    #         :return: None
    #     """
    #
    #     for key, xpr_state in self.xpr_dict.items():
    #         # x_arr = np.mean(xpr_state[:,:,:t+1], axis=0).T
    #         # y_arr = np.mean(xpr_state[:,:,:t+1], axis=0).T.dot(self.H.T)
    #
    #         # Q err
    #         #Q = np.full_like(self.y_obs[:, 0], self.Q_state)
    #
    #         x_arr = xpr_state[:,:,t].T
    #         y_arr = self.H.dot(xpr_state[:,:,t].T)
    #
    #         # cross_cov["ddf"] = np.cov(xprH.T, ddfH)
    #
    #
    #         # Calculate mean of values
    #         meanX = np.mean(x_arr, axis=1)
    #         meanY = np.mean(y_arr, axis=1)
    #
    #         X = np.array(x_arr - meanX[:, np.newaxis])
    #         Y = np.array(y_arr - meanY[:, np.newaxis])
    #
    #         self.pr_cross_cov_state[key][:,:,t] = np.dot(X, Y.T) * (1./ (self.INSTANCES - 1.))
    #         print "State p mat. done"
    #         print "STATE P MAX/MIN"
    #         print self.pr_cross_cov_state["swe"][:,:,t].max()
    #         print self.pr_cross_cov_state["swe"][:,:,t].min()

    def _calculate_cross_cov_ind_(self, arr1, arr2, i):
        """
            Calculates cross covariance. First dimension is concat dimension (i).
            Expected values generated from ensemble.
            CURRENTLY NOT USED.
            :param arr1: First array (full ensemble).
            :param arr2: Second array (full ensemble.
            :param i: Specific member of ensemble to generate cross_cov matrix for.
            :returns: Cross covariance matrix.
        """

        meanX = np.mean(arr1, axis=0)
        meanY = np.mean(arr2, axis=0)

        X = np.array(arr1[i,:] - meanX)
        Y = np.array(arr2[i,:] - meanY)

        return np.dot(X[:, np.newaxis], Y[np.newaxis,:]) / (arr1.shape[0] - 1)

    # def _calculate_cross_cov_ind_withtime_(self, arr1, arr2, i):
    #     """
    #         Calculates cross covariance. First dimension is concat dimension (i).
    #         Third dimension is time (t).
    #         Expected values generated from ensemble and over time.
    #         :param arr1: First array [full ensemble, info, however many dimensions of time to average].
    #         :param arr2: Second array [full ensemble, info, however many dimensions of time to average].
    #         :param i: Specific member of ensemble to generate cross_cov matrix for.
    #         :returns: Cross covariance matrix.
    #     """
    #
    #     meanX = np.mean( np.mean(arr1, axis=0), axis=1)
    #     meanY = np.mean(np.mean(arr2, axis=0), axis=1)
    #
    #     X = np.array(arr1[i, :] - meanX)
    #     Y = np.array(arr2[i, :] - meanY)
    #
    #     return np.dot(X[:, np.newaxis], Y[np.newaxis, :]) / (arr1.shape[0] - 1)

    def _calculate_cross_cov_sum_(self, arr1, arr2):
        """
            Calculates cross covariance. First dimension is concat dimension (i).
            Expected values generated from ensemble.
            CURRENTLY THIS IS THE ONE THAT IS USED.
            :param arr1: First array (full ensemble).
            :param arr2: Second array (full ensemble.
            :param i: Specific member of ensemble to generate cross_cov matrix for.
            :returns: Cross covariance matrix.
        """

        meanX = np.mean(arr1, axis=0)
        meanY = np.mean(arr2, axis=0)

        final_arr = np.zeros((arr1.shape[1], arr2.shape[1]))

        for i in range(arr1.shape[0]):
            X = np.array(arr1[i, :] - meanX)
            Y = np.array(arr2[i, :] - meanY)

            # final_arr += np.dot(X[:, np.newaxis], Y[np.newaxis,:])
            final_arr = final_arr + np.outer(X,Y)

        return final_arr / (arr1.shape[0] - 1)

    def _calculate_cross_cov_(self, arr1, arr2):
        """
            Calculates cross covariance. First dimension is concat dimension (i).
            Expected values generated from ensemble.
            CURRENTLY NOT USED. However, this should technically work just as well as the _calculate_cross_cov_sum_
            :param arr1: First array (full ensemble).
            :param arr2: Second array (full ensemble.
            :param i: Specific member of ensemble to generate cross_cov matrix for.
            :returns: Cross covariance matrix.
        """

        newcov = np.dot((arr1 - arr1.mean(axis=0)).T, arr2 - arr2.mean(axis=0)) / (arr1.shape[0] - 1)

        return newcov

    def _calculate_K_param_(self,t):
        """
        Calculate K matrix for parameter
        :param t: Timestep
        :return: Nothing
        """

        # Calculate K: K = PH^Tself.self.pr_cross_cov_param[key].dot(np.linalg.inv(in_parenthesis))(HPH+R) -q
        for key, value in self.xpr_dict.items():

            R = np.diag(np.full_like(self.y_obs[:,t], self.R_error)) # NOW R IS yobs_var

            in_parenthesis = self.pr_cov_y + R
            self.K_dict_param[key] = self.pr_cross_cov_param[key][:,:,t].dot(np.linalg.inv(in_parenthesis))


            print "Param K mat. done"
            print "K PARAM MAX/MIN"
            print self.K_dict_param["swe"].max()
            print self.K_dict_param["swe"].min()

    def _calculate_K_state_(self,t):
        """
        Calculate K matrix for state
        :param t: Timestep
        :return: Nothing
        """

        # Calculate K: K = PH^T(HPH+R) -q
        for key, value in self.xpr_dict.items():

            R = np.diag(np.full_like(self.y_obs[:,t], self.R_error)) # NOW R IS yobs_var

            in_parenthesis = self.pr_cov_y + R
            self.K_dict_state[key] = self.pr_cross_cov_state[key][:,:,t].dot(np.linalg.inv(in_parenthesis))

            print "State K mat. done"
            print "K STATE MAX/MIN"
            print self.K_dict_state["swe"].max()
            print self.K_dict_state["swe"].min()

    def _calculate_K_param_individual_(self,t, i):
        """
        Calculates an individual K matrix for each ensemble's parameters. Currently not used.
        :param t: Timestep
        :param i: Instance
        :return: Nothing
        """

        # Calculate K: K = PH^Tself.self.pr_cross_cov_param[key].dot(np.linalg.inv(in_parenthesis))(HPH+R) -q
        for key, xpr_state in self.xpr_dict.items():
            current_cross_cov = self._calculate_cross_cov_ind_(self.dict_en[key][:,:,t+1].dot(self.H.T), xpr_state[:, :, t].dot(self.H.T), i)
            #current_pr_cov_y = np.cov(self.H.dot(xpr_state[i, :, t].T))
            current_pr_cov_y = self._calculate_cross_cov_ind_(xpr_state[:, :, t].dot(self.H.T),
                                                          xpr_state[:, :, t].dot(self.H.T), i)
            R = np.diag(np.full_like(self.y_obs[:,t], self.R_error)) # NOW R IS yobs_var

            print "Param K mat. done"
            in_parenthesis = current_pr_cov_y + R

            self.K_dict_param[key][i,:,:] = current_cross_cov.dot(np.linalg.inv(in_parenthesis))

            print "K PARAM MAX/MIN"
            print self.K_dict_param["swe"].max()
            print self.K_dict_param["swe"].min()

            print "KPARAM " + str(i) + " CALCULATED"

    def _calculate_K_state_individual_(self,t, i):
        """
        Calculates an individual K matrix for each ensemble member's state. Currently not used.
        :param t: Timestep
        :param i: Instance
        :return: Nothing
        """

        # Calculate K: K = PH^T(HPH+R) -q
        for key, xpr_state in self.xpr_dict.items():
            current_cross_cov = self._calculate_cross_cov_ind_(xpr_state[:,:,t],xpr_state[:,:,t].dot(self.H.T), i)

            current_pr_cov_y = self._calculate_cross_cov_ind_(xpr_state[:,:,t].dot(self.H.T),xpr_state[:,:,t].dot(self.H.T), i)

            R = np.diag(np.full_like(self.y_obs[:,t], self.R_error))

            in_parenthesis = current_pr_cov_y + R
            self.K_dict_state[key][i,:,:] = current_cross_cov.dot(np.linalg.inv(in_parenthesis))
            print "State K mat. done"

            print "K STATE MAX/MIN"
            print self.K_dict_state["swe"].max()
            print self.K_dict_state["swe"].min()

            print "KSTATE " + str(i) + " CALCULATED"

    def _parameter_update_(self, t):
        """
        Paramter correction function.
        :param t: Timestep
        :return: Nothing
        """


        for key, xpr_state in self.xpr_dict.items():

            self.xpo_dict_param[key][:,:,t+1] = (self.H.dot(self.dict_en[key][:,:,t+1].T) + self.K_dict_param[key].dot(
                self.y_obs_perturbed[:, :, t] - self.H.dot(xpr_state[:,:,t].T))).T

        self.xpo_dict_param["swe"][:, :, t+1] = self.xpo_dict_param["swe"][:, :, t+1].clip(min=1.1) # Some arbitrary number
        self.xpo_dict_param["swe"][:, :, t+1] = self.xpo_dict_param["swe"][:, :, t+1].clip(max=20) # Some arbitrary number

        self.pre_innovation_arr[:, :, t] = self.y_obs_perturbed[:, :, t] - self.H.dot(xpr_state[:, :, t].T)

    def _member_update_(self, t):
        """
        State correction function.
        :param t: Timestep
        :return: Nothing
        """

        for key, xpr_state in self.xpr_dict.items():

            self.xpo_dict[key][:,:,t] = (xpr_state[:,:,t].T + self.K_dict_state[key].dot(
                self.y_obs_perturbed[:, :, t] - self.H.dot(xpr_state[:,:,t].T))).T


            # This shouldn't be nessesary but I'm running out of ideas.
            # for i in range(self.INSTANCES):
            #     self.xpo_dict[key][i, :, t] = xpr_state[i, :, t] + self.K_dict_state[key].dot(
            #         self.y_obs_perturbed[:, i, t] - self.H.dot(xpr_state[i, :, t]))

            # For debugging purposes
            self.innovation_arr[:, :, t] = self.y_obs_perturbed[:,:,t] - self.H.dot(xpr_state[:,:,t].T)

        print "State member update done"
        if self.xpo_dict["swe"][:, :, t].min() < 0:
            print "$$$$$$\nDANGER WILL ROBINSON DANGER!!!!"
        self.xpo_dict["swe"][:, :, t] = self.xpo_dict["swe"][:, :, t].clip(min=0)
        #self.xpo_dict_param["swe"][:, :, t] = self.xpo_dict_param["swe"][:, :, t].clip(max=100000) # Some arbitrary number

    def _parameter_update_individual_(self, t, i):
        """
        Allows each parameter to be corrected by an an individual Kalman Gain. Currently not used.
        :param t: Timestep
        :param i: Instance to correct
        :return: Nothing
        """
        for key, xpr_state in self.xpr_dict.items():

            self.xpo_dict_param[key][i,:,t+1] = (self.H.dot(self.dict_en[key][i,:,t+1].T) + self.K_dict_param[key][i,:,:].dot(
                self.y_obs_perturbed[:, i, t] - self.H.dot(xpr_state[i,:,t].T))).T

        self.xpo_dict_param["swe"][i, :, t+1] = self.xpo_dict_param["swe"][i, :, t+1].clip(min=2)
        self.xpo_dict_param["swe"][i, :, t+1] = self.xpo_dict_param["swe"][i, :, t+1].clip(max=20) # Some arbitrary number

    def _member_update_individual_(self, t, i):
        """
        Allows each state to be corrected by an an individual Kalman Gain. Currently not used.
        :param t: Timestep
        :param i: Instance to correct
        :return: Nothing
        """

        for key, xpr_state in self.xpr_dict.items():

            self.xpo_dict[key][i,:,t] = xpr_state[i,:,t] + self.K_dict_state[key][i,:,:].dot(
                self.y_obs_perturbed[:, i, t] - self.H.dot(xpr_state[i,:,t]))

            # For debugging purposes
            self.innovation_arr[:, i, t] = self.y_obs_perturbed[:,i,t] - self.H.dot(xpr_state[i,:,t].T)
        print "State member update done"

        self.xpo_dict["swe"][i, :, t] = self.xpo_dict["swe"][i, :, t].clip(min=0)

    def _update_param_rasters_(self, i,t):
        """
            Updates an ensemble instance's .tif raster files. In the filter, this function is placed in a loop
            and updates all parameters before the state filter runs.
        :param i: The ensemble instance
        :param t: The timestep
        :return: Nothing
        """

        for key, param_array in self.xpo_dict_param.items():

            newParamRaster = self._params_to_thiessen_(param_array[i,:,t+1])
            self.hbv_pars["ddf"].write_array_to_geotiff(self.pars["ddf"], newParamRaster) # Saves geotiff
            self.xpo_dict_param_final[key][i,:,t+1] = newParamRaster.flatten()

    def _generate_final_rasters_(self):
        """
        Saves all final states of the rasters.
        Eventually, this function will be expanded save a variety of different final parameters (averaging over time, etc).
        :return: Nothing
        """

        for key, param_array in self.xpo_dict_param.items():
            final_ddf = np.mean(self.xpo_dict_param_final[key][:,:,self.TIMESTEPS], axis=0)
            self.hbv_pars["ddf"].write_array_to_geotiff(self.pars["ddf"], final_ddf.reshape(self.THIESSEN.shape))

    def _generate_final_pickles_(self):
        """
        This function saves all relevant variables as pickles. It is useful for debugging but is storage-intensive (around 50GB sometimes)
        :return: Nothing
        """
        for key, param_array in self.xpo_dict_param.items():
            pickle.dump(self.xpo_dict[key], open(key + "_xpo_dict.pickled", "wb"))
            pickle.dump(self.xpo_dict_param[key], open(key + "xpo_dict_param.pickled", "wb"))
            pickle.dump(self.xpr_dict[key], open(key + "xpr_dict.pickled", "wb"))
            pickle.dump(self.dict_en[key], open(key + "dict_en.pickled", "wb"))
            pickle.dump(self.innovation_arr, open("innovation_arr.pickled", "wb"))
            pickle.dump(self.pr_cross_cov_param[key], open(key + "pr_cross_cov_param.pickled", "wb"))
            pickle.dump(self.pr_cross_cov_state[key], open(key + "pr_cross_cov_state.pickled", "wb"))
            pickle.dump(self.y_obs, open("yobs.pickled", "wb"))
            pickle.dump(self.y_obs_perturbed, open("y_obs_perturbed.pickled", "wb"))

    def _update_pickled_states_(self, i,t):
        """
            Updates pickled states for an ensemble instance. In the filter, this function is placed in a loop
            and updates all states at the end of the timestep.
            :param i: current instance
            :param t: current timestep
            :return: Nothing
        """

        # rioObject.write_array_to_geotiff(save_path, arr)
        # observationPoints = self.xpo_dict["swe"][i]
        #self.xpo_dict["swe"][:, :, t].clip(min=0)

        for key in self.xpo_dict.iterkeys():
            pickle.dump(self.xpo_dict[key][i,:,t].reshape(self.THIESSEN.shape), open(key + ".pickled", "wb"))

    ####>>> USER FUNCTIONS <<<#### | functions available to the user

    # %% REDO THIS %%
    def start_filter_timesteps(self, dump_array_vals=True, save_pickles=False):
        """
        Begins kalman filtering process over self.TIMESTEPS.
        Plots innovation and stuff.

        :param dump_array_vals: Print arrays to console after the filter completes.
        :return: self.xpr_dict["swe"], self.xpo_dict["swe"], self.pr_cross_cov_state["swe"], self.dict_en["swe"], self.xpo_dict_param["swe"], self.pr_cross_cov_param["swe"], self.y_obs_perturbed, self.y_obs, self.innovation_arr, self.H, self.pre_innovation_arr, self.uvars_arr
        """

        self._run_ensemble_kalman_filter_timesteps_(save_pickles)

        if dump_array_vals:
            self._dump_array_vals_()

        return self.xpr_dict["swe"], self.xpo_dict["swe"], self.pr_cross_cov_state["swe"], self.dict_en["swe"], self.xpo_dict_param["swe"], self.pr_cross_cov_param["swe"], self.y_obs_perturbed, self.y_obs, self.innovation_arr, self.H, self.pre_innovation_arr, self.uvars_arr

    def run_model(self, args):
        """
            Run the model. Uses the function supplied at __init__
        """
        print "UUNNNGHHHH " + args.init_date
        return self.f(args)

    ####>>> DATA VISULIZATION FUNCTIONS <<<#### | NOTE: Most of these have been moved over to prettygraphs.py

    def _dump_array_vals_(self):
        """
        Prints all gobal numpy arrays to console.
        :return: Nothing
        """
        print "Y OBS PERTURBED"
        print self.y_obs_perturbed

        print "INNOVATION ARRAY"
        print self.innovation_arr

        print "ESTIMATES FOR DDF"
        print self.dict_en["swe"]

        print "POST ESTIMATES FOR SWE"
        print self.xpo_dict_param["swe"]

        print "PRIOR ESTIMATES FOR STATE"
        print self.xpr_dict["swe"]

        print "POST ESTIMATES FOR STATE"
        print self.xpo_dict["swe"]

        print "PRECIP_ARR"
        print self.precip_arr

