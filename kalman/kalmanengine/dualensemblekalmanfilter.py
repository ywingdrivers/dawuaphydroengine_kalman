#!/usr/bin/env python

#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#
# dualensemblekalmanfilter.py
#
# Main class for the ensemble kalman filter
# -----------------------
# William Cook July 16th, 2017
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#

import numpy as np, numpy.ma as ma
import time
import matplotlib.pyplot as plt
import warnings, os
from kalman import kalmanutils as kutils
import multiprocessing as mp
import ctypes
from shutil import copyfile
import math

plt.switch_backend('agg')

class Args:
    """
        This simple class holds a dictionary of kargs that are passed to a model object.
        (this functionality is nesessary for dawuaphydroengine)
    """
    def __init__(self, kwargs):
        self.__dict__.update(kwargs)


class Dual_Ensemble_Kalman_Filter:
    """
        Ensemble Kalman Filter - main class.

        ====
        Math - Standard Kalman Filter
        ====

        **Update Steps (calculate priori from previous postori)**

        * *xpr* = Priori Estimate = :math:`A*xpo`
        * *Ppr* = Priori Error Cov. Matrix = :math:`A*Ppo*A^T + Q`

        **Correction Steps (calculate postori from the priori)**

        * *K* = Kalman Gain = :math:`(Pepr*C^T)/(C*Pepr*C^T+R)`
        * *xpo* = Postiori Estimate = :math:`xpr + K(y - Cxpr)`
        * *Ppo* = Postiori Error Cov Matrix = :math:`(I-K*C)*Ppr`

        ====
        Math - Ensemble Kalman Filter
        ====

        **Update Steps (calculate priori from previous postori)**

        {{ DO THIS }}

        **Correction Steps (calculate postori from the priori)**

        {{ DO THIS }}

    """

    ####>>> INITIALIZATION METHODS <<<####

    def __init__(self, f, default_model_args, tparam_dict, initial_uparam_dict, initial_obs_dict, initial_state_dict, H_dict, tparam_states_dict, Q_errors = {}, R_errors = {}, U_errors = {}, smoothing_params = {"noise": .97, "h": 1}, tparam_mins = None, tparam_maxs= None, min_var=0, instances=50, timesteps=365, cooldown_steps=50, generate_pickles = False, unknown_placeholder_tparams=False, unknown_placeholder_uparams=False, unknown_placeholder_obs=False, send_instance_number_to_model=False, required_files=[], run_in_this_folder=None):
        """

        :param f: Model Function - Runs the model for a timestep. The function is passed an Args() object.
        :param default_model_args: Default arguments that will be passed to the Model Function.
        :param tparam_dict: Dictionary of all theta parameter numpy arrays. Will be flattened to 1D. Ex. mythetas["ddf"] = [67,32,56,75]
        :param initial_uparam_dict: Dictionary of all u parameter numpy arrays. Will be flattened to 1D. Ex. myuparams["temp"] = [1,3,0,2]
        :param initial_obs_dict: Dictionary of initial numpy observation arrays. Will be flattened to 1D. Ex. myobs["swe"] = [56,35,56,40]
        :param initial-states_dict: Dictionary of initial numpy uparam arrays. Will be flattened to 1D. Ex. myobs["precip"] = [0,0.3,1.2,0.8]
        :param H_dict: H mask matricies
        :param tparam_states_dict: A dictonary that links each tparam with one or more states. Ex. myparstates["ddf"] = ["swe","streamflow"]
        :param Q_errors: Parameter Error - Standard deviation of the theta parameters. Dictionary of scalars - one for each theta parameter.
        :param R_errors: Measurement Error - Amount of error for the observations. Dictionary of scalars - one for each measurement.
        :param U_errors: Error matrix for U vars. Dictnoary of scalars - one for each u parameter.
        :param smoothing_vectors: Dictionary of smoothing vectors. Valid entries are "noise" and "h". Defaults are {"noise": .97, "h": 1}
        :param tparam_mins: Dictonary of minimum limits for each tparam. These will be hard limits.
        :param tparam_maximum: Dictonary of maximum limits for each tparam. These will be hard limits.
        :param min_var: A minimum varaiance that will always be calculated. Default 0.
        :param instances: Number of instances in ensemble. Default 50.
        :param timesteps: Number of timesteps. Default 365.
        :param cooldown_steps: Number of timesteps before the filter stops modifying parameters when there is no change in observed state.
        :param generate_pickles: Default false. Saves stuff.
        :param unknown_placeholder_tparams: The variable that represents unknown data in uparam arrays. Set to False if this does not apply.
        :param unknown_placeholder_uparams: The variable that represents unknown data in uparam arrays. Set to False if this does not apply.
        :param unknown_placeholder_obs: The variable that represents unknown data in observation arrays. Set to False if this does not apply.
        :param send_instance_number_to_model: This adds the 'i' argument as the last call to the model function. Default false.
        :param required_files: file_list: Allows user to specify files that should be copied into each model instance. List of file locations. Ex. ["data/mynessesaryfile.txt","data/husky.jpeg"]
        :param run_in_this_folder: Puts all ensemble instances in this folder. Defaults to the current directory.
        """



        # Set constant parameters
        self.INSTANCES = instances
        self.TIMESTEPS = timesteps
        self.COOLDOWN_STEPS = cooldown_steps
        self.GENERATE_PICKLES = generate_pickles
        self.RUN_IN_THIS_FOLDER = run_in_this_folder
        self.SEND_INSTANCE_NUMBER_TO_MODEL = send_instance_number_to_model
        self.MIN_VAR = min_var

        # Unknown placeholders - these values represent unknown data in arrays.
        self.UNKNOWN_PLACEHOLDER_TPARAMS = unknown_placeholder_tparams
        self.UNKNOWN_PLACEHOLDER_UPARAMS = unknown_placeholder_uparams
        self.UNKNOWN_PLACEHOLDER_OBS = unknown_placeholder_obs

        # Folder stuff
        self.ABS_PATH = os.getcwd()
        print "RUNNING IN: " + self.ABS_PATH
        self.RESULTS_FOLDER = "results" + str(int(time.time())) + "/"

        # Iterators
        self.cooldown = 0

        # Initialize parameters.
        self.Q_errors = Q_errors
        self.R_errors = R_errors
        self.U_errors = U_errors
        self.f = f  # function for running model

        # Smoothing vectors - arbitrary values.
        # self.param_noise = smoothing_params["noise"]
        # self.param_a = (self.param_noise*3 - 1)/(2*self.param_noise)
        self.param_a = smoothing_params["a"]
        self.param_h = smoothing_params["h"]

        # These will eventually hold dictionaries of functions that manipulate files the model returns.
        self.state_getter = {} # User-supplied functions. Gets state of model after run. Must return a numpy array.
        self.uparam_modifyer = {} # User-supplied functions. Modifys uparameter file before model run.
        self.tparam_modifyer = {} # User-supplied functions. Modifys tparameter file before model run.
        self.state_modifyer = {} # User-supplied functions. Modifys state file after update function.

        # Base arguments for running model
        self.default_args = default_model_args

        # Same as above, but will be modified.
        self.args = default_model_args

        # Global u parameter dictonary (values are arrays.)
        self.uparam_dict = initial_uparam_dict

        # Global theta parameter dictonary (values are arrays.)
        self.tparam_dict = tparam_dict

        # Global observation dictonary (values are arrays.)
        self.obs_dict = initial_obs_dict

        # H matrix - uses lat/long dict and affine info to create H matricies.
        self.H_dict_init = H_dict
        self.H_dict = dict(self.H_dict_init) # This array holds H dicts for current timestep - for trunkated H dicts.

        # Links tparams with their states
        self.tparam_states_dict = tparam_states_dict

        # Initial states
        self.initial_state_dict = initial_state_dict

        # Min and Maxes for parameters
        if tparam_mins:
            self.tparam_mins = tparam_mins
            for tname in self.tparam_dict:
                if not tname in self.tparam_mins:
                    self.tparam_mins[tname] = np.NINF
        else:
            self.tparam_mins = {}
            for tname in self.tparam_dict:
                self.tparam_mins[tname] = np.NINF
        if tparam_maxs:
            self.tparam_maxs = tparam_maxs
            for tname in self.tparam_dict:
                if not tname in self.tparam_maxs:
                    self.tparam_maxs[tname] = np.Inf
        else:
            self.tparam_maxs = {}
            for tname in self.tparam_dict:
                self.tparam_maxs[tname] = np.Inf


        # K matricies - will hold a K matrix for the timestep
        self.K_param_dict = {} # We assign this later since these are dependent on which parameters are linked with which states.

        self.K_state_dict = {}
        for key in self.obs_dict:
            self.K_state_dict[key] = np.zeros((self.H_dict[key].shape[1],self.H_dict[key].shape[0]))

        # Cov. Matricies - hold 1 matrix per timestep. We save these.
        self.cross_cov_param_dict = {} # Param-ob cross cov

        for tparam_name, tparam_array in self.tparam_dict.iteritems():
            for state_name in self.tparam_states_dict[tparam_name]:
                self.cross_cov_param_dict[tparam_name+state_name] = np.zeros((tparam_array.shape[0], self.H_dict[state_name].shape[0]))
                self.K_param_dict[tparam_name+state_name] = np.zeros((self.H_dict[key].shape[0], self.H_dict[key].shape[0]))

        self.cross_cov_state_dict = {} # State-predicition cross cov
        self.cov_pred_dict = {} # Prediction cov

        for key in self.obs_dict:
            self.cross_cov_state_dict[key] = np.zeros((self.H_dict[key].shape[1], self.H_dict[key].shape[0]))
            self.cov_pred_dict[key] = np.zeros((self.H_dict[key].shape[0], self.H_dict[key].shape[0]))


        # Prior parameter arrays - initially holds initial estimates.
        self.pri_tparam = {}
        for key, array in self.tparam_dict.iteritems():
            self.pri_tparam[key] = np.empty((self.INSTANCES, array.flatten().shape[0]))
            self.pri_tparam[key][:, :] = array.flatten()

        # Prior state arrays - initially slot holds nothing.
        self.pri_state = {}
        for key, array in self.initial_state_dict.iteritems():
            self.pri_state[key] = np.empty((self.INSTANCES, array.flatten().shape[0]))
            self.pri_state[key][:, :] = array.flatten()

        # Predicted state arrays - initially slot holds nothing.
        self.pred_state = {}
        for key, array in self.obs_dict.iteritems():
            self.pred_state[key] = np.zeros((self.INSTANCES, array.flatten().shape[0]))

        # Posterior parameter arrays - initially slot holds initial estimates.
        self.post_tparam = {}
        for key, array in self.tparam_dict.iteritems():
            self.post_tparam[key] = np.empty((self.INSTANCES, array.flatten().shape[0]))
            self.post_tparam[key][:,:] = kutils.add_noise_to_array_instances(np.random.normal,array, self.INSTANCES, scale=math.sqrt(self.Q_errors[key])).T.clip(min=self.tparam_mins[key], max=self.tparam_maxs[key])

        # Posterior state arrays - initially slot holds initial estimates.
        self.post_state = {}
        for key, array in self.initial_state_dict.iteritems():
            self.post_state[key] = np.empty((self.INSTANCES, array.flatten().shape[0]))
            self.post_state[key][:, :] = initial_state_dict[key]

        # Perturbed observations - will hold perturbed observations. Initally honds nothing.
        self.perturbed_obs_dict = {}
        for key, array in self.obs_dict.iteritems():
            self.perturbed_obs_dict[key] = np.zeros((self.INSTANCES, array.flatten().shape[0]))

        # Parameter deactivation mask - this holds a mask for every parameter.
        self.tparam_mask_dict = {}
        self.state_mask_dict = {}
        for key, array in self.tparam_dict.iteritems():
            self.tparam_mask_dict[key] = np.zeros((array.flatten().shape[0])) # Initialized as all unmasked (zeros)
            for state_key in self.tparam_states_dict[key]:
                self.state_mask_dict[key+state_key] = np.zeros((self.obs_dict[state_key].flatten().shape[0]))

        # # State deactivation mask - this holds a mask for every state that would be deactivated in parameter correction.
        # self.state_mask_dict = {}
        # for key, array in self.obs_dict.iteritems():
        #     self.state_mask_dict[key] = np.zeros((array.flatten().shape[0])) # Initialized as all unmasked (zeros)



        # Shared memory - Arrays in c code that can be used concurrently by multiprocessing library instances.
        self.shared_arr_params_dict = {}
        for key, array in self.tparam_dict.iteritems():
            self.shared_arr_params_dict[key] = mp.Array(ctypes.c_double, self.INSTANCES * array.flatten().shape[0])

        self.shared_arr_states_dict = {}
        for key, array in self.initial_state_dict.iteritems():
            self.shared_arr_states_dict[key] = mp.Array(ctypes.c_double, self.INSTANCES * array.flatten().shape[0])

        self.shared_arr_uparams_dict = {} # It's questionable that we need this for anything besides debugging.
        for key, array in self.uparam_dict.iteritems():
            self.shared_arr_uparams_dict[key] = mp.Array(ctypes.c_double, self.INSTANCES * array.shape[0])


        # This may hold a list of the location of files to copy to each model istance
        self.required_files = required_files

        # Bonus information. These variables hold information the user may want to store.
        self.innovation_pr_loop = {}
        self.innovation_st_loop = {}




    # +"+"+"+"+"+"+"+"+"+"+"+"+" #
    # >    Public Functions    < #
    # _+_+_+_+_+_+_+_+_+_+_+_+_+ #


    # &@@@            @@@& #
    # >> Main functions << #
    # &@@@            @@@& #

    def simulate(self, uparam_dict=None, arg_dict=None, uparams_mode="perturb", perturb_tparams=True):
        """
        Runs the model(s), calculates averages, and returns the prediction for the next timestep.
        :param uparam_dict: Optional. Allows new uparams to be passed to model for this iteration.
        :param arg_dict: Optional. Allows arguments passed to model to be modified (same as set_arguments().)
        :param perturb_uparams: If set to "perturb", perturbs uparams. If set to "unmodified", copies normal uparams into folder. If set to "none" (or anything else,) will not copy the params.  Default "perturb".
        :param perturb_tparams: Default True. If set to 'False', the tparams will not be perturbed. Only use this if you do not want to do parameter correction during update().
        :return: Predictions for all states
        """

        if uparam_dict != None: # Set new uparams if specified
            self.uparam_dict = uparam_dict

        if arg_dict != None: # Set args if specified
            self.set_arguments(arg_dict)

        # Parallel Processes
        process_list = []
        for i in range(0, self.INSTANCES):
            p = mp.Process(name="Instance " + str(i), target=self._model_member_mp_,
                           args=(i, np.random.normal, uparams_mode, perturb_tparams))
            process_list.append(p)
            p.start()

        # Join all processes
        for i in range(0, self.INSTANCES):
            process_list[i].join(100) # Return anyway if a process takes more then 100 seconds (is frozen)

        # Get shared memory
            for key, array in self.pri_tparam.iteritems():
                if perturb_tparams:
                    self.pri_tparam[key][:, :] = np.frombuffer(self.shared_arr_params_dict[key].get_obj()).reshape((self.INSTANCES, array.shape[1]))
                else:
                    self.pri_tparam[key][:, :] = self.post_tparam[key]


        for key, array in self.pri_state.iteritems():
            self.pri_state[key][:, :] = np.frombuffer(self.shared_arr_states_dict[key].get_obj()).reshape((self.INSTANCES, array.shape[1]))


    def update(self, obs_dict, arg_dict=None, param_filter_active=True, state_filter_active=True):
        """
        Takes observations for each state and uses them to correct parameters. Model is rerun and
        with new parameters and the states corrected via the observations.
        :param obs_dict: Dictionary with 1D numpy arrays of observations for each state (only 1 timestep).
        :param arg_dict: Optional. Allows arguments passed to model to be modified (same as set_arguments().)
        :param param_filter_active: Default True. If set to 'False', only the state correction will take place.
        :param param_filter_active: Default True. If set to 'False', no state correction will take place.
        :return: prior_param_dict, posterior_param_dict, prior_state_dict, posterior_state_dict.
        """

        if arg_dict != None: # Set args if specified
            self.set_arguments(arg_dict)

        # Set H_dicts_cur to H_dicts
        self.H_dict = dict(self.H_dict_init)

        # !!!FUTURE IMPROVEMENT!!! - allow user to input custom H matrix.
        # if H_dict == None:
        #     self.H_dict = dict(self.H_dict_init)
        # else:
        #     self.H_dict = H_state_dict

        # Update observations
        self.obs_dict = obs_dict

        # for key, array in self.obs_dict.iteritems():
        #     if np.?????(array): # Are there any unknown values?
        #         unknown_indexes = np.where(array==self.UNKNOWN_PLACEHOLDER_OBS)
        #         self.obs_dict[key] = np.delete(self.obs_dict[key],unknown_indexes)
        #         self.H_dict[key] = np.delete(self.H_dict[key],unknown_indexes, axis=0)
        unknown_indexes_dict = {}
        for key, array in self.obs_dict.iteritems():
            if np.isnan(array).any(): # Are there any nan values?
                unknown_indexes_dict[key] = np.isnan(array)
                self.obs_dict[key] = array[~unknown_indexes_dict[key]]
                self.H_dict[key] = self.H_dict[key][~unknown_indexes_dict[key],:]
            else:
                unknown_indexes_dict[key] = np.full(self.obs_dict[key].shape[0], False)

        # Instead of deleting H stuff, update state_mask
        # for key, array in self.obs_dict.iteritems():
        #     if np.isnan(array).any(): # Are there any nan values?
        #         unknown_indexes = np.isnan(array)
        #         self.obs_dict[key] = array[~unknown_indexes]
        #         self.H_dict[key] = self.H_dict[key][~unknown_indexes,:]

        # Perturb observations
        for key, array in self.obs_dict.iteritems():
            self.perturbed_obs_dict[key] = kutils.add_noise_to_array_instances(np.random.normal,array, self.INSTANCES, clip=0, scale=math.sqrt(self.R_errors[key])).T


        # ------@@@@@@@@@@@@@@@@------ #
        # >>>>> PARAMETER FILTER <<<<< #
        # ------@@@@@@@@@@@@@@@@------ #


        if param_filter_active: # Do we run both filters or not?

            # Calculate covariances and K matricies
            for key_st, array_st in self.pri_state.iteritems():


                for key_tp, array_tp in self.pri_tparam.iteritems():

                    if (key_tp+key_st) in self.cross_cov_param_dict: # Check to see if we need to calculate this covariance combination

                        array_st_H_c = ma.masked_array(array_st.dot(self.H_dict[key_st].T),
                                                       np.broadcast_to(self.state_mask_dict[key_tp+key_st][~unknown_indexes_dict[key_st]],
                                                                       (array_st.shape[0],self.state_mask_dict[key_tp+key_st][~unknown_indexes_dict[key_st]].shape[
                                                                           0]))).compressed().reshape(array_st.shape[0],
                                                                                                      self.H_dict[
                                                                                                          key_st].shape[
                                                                                                          0] - np.count_nonzero(
                                                                                                          self.state_mask_dict[
                                                                                                              key_tp + key_st][~unknown_indexes_dict[key_st]]))

                        self.cov_pred_dict[key_st] = np.cov(array_st_H_c.T)  # Generate prediction covariance

                        array_tp_c = ma.masked_array(array_tp, np.broadcast_to(self.tparam_mask_dict[key_tp],
                                                                             array_tp.shape)).compressed().reshape(array_tp.shape[0],array_tp.shape[1] - np.count_nonzero(self.tparam_mask_dict[key_tp]))

                        self.cross_cov_param_dict[key_tp+key_st] = kutils.cross_cov(array_tp_c, array_st_H_c) # Generate param-prediction cross covariance
                        self.K_param_dict[key_tp+key_st] = kutils.calculate_K_matrix(self.cross_cov_param_dict[key_tp+key_st], # Update Kalman Gain Matrix
                                                                                 self.cov_pred_dict[key_st],
                                                                                 self.R_errors[key_st])

            # Update posterior parameter estimations
            for key_tp, array_tp in self.pri_tparam.iteritems():
                for key_st in self.tparam_states_dict[key_tp]:

                    # Create masked arrays
                    array_tp_c = ma.masked_array(array_tp, np.broadcast_to(self.tparam_mask_dict[key_tp], array_tp.shape))
                    array_st_H = ma.masked_array(self.H_dict[key_st].dot(self.pri_state[key_st].T).T, np.broadcast_to(self.state_mask_dict[key_tp+key_st][~unknown_indexes_dict[key_st]], (array_st.shape[0], self.H_dict[key_st].shape[0])))
                    array_pert_obs = ma.masked_array(self.perturbed_obs_dict[key_st], np.broadcast_to(self.state_mask_dict[key_tp+key_st][~unknown_indexes_dict[key_st]], (array_st.shape[0], self.H_dict[key_st].shape[0]))).compressed().reshape(array_st.shape[0],self.H_dict[key_st].shape[0] - np.count_nonzero(self.state_mask_dict[key_tp+key_st][~unknown_indexes_dict[key_st]]))

                    self.innovation_pr_loop[key_st] = array_pert_obs - array_st_H.compressed().reshape(array_st.shape[0],self.H_dict[key_st].shape[0] - np.count_nonzero(self.state_mask_dict[key_tp+key_st][~unknown_indexes_dict[key_st]]))
                    self.post_tparam[key_tp][:, :] = self.pri_tparam[key_tp][:,:] # Set to previous, this way masked array values stay unchanged
                    self.post_tparam[key_tp][~array_tp_c.mask] = (array_tp_c.compressed().reshape(array_tp.shape[0],array_tp.shape[1] - np.count_nonzero(self.tparam_mask_dict[key_tp])).T + self.K_param_dict[key_tp+key_st].dot(self.innovation_pr_loop[key_st].T)).T.flatten()
                    self.post_tparam[key_tp] = self.post_tparam[key_tp].reshape(array_tp.shape)

                    # Fill in the rest of the innovations as 0's.
                    nan_matrix = np.zeros_like(self.perturbed_obs_dict[key_st])
                    nan_matrix_all = np.zeros((self.perturbed_obs_dict[key_st].shape[0], self.H_dict_init[key_st].shape[0]))
                    nan_matrix[~array_st_H.mask] = self.innovation_pr_loop[key_st].flatten()
                    nan_matrix_all[:,~unknown_indexes_dict[key_st]] = nan_matrix
                    self.innovation_pr_loop[key_st] = nan_matrix_all.reshape((self.perturbed_obs_dict[key_st].shape[0], self.H_dict_init[key_st].shape[0]))

                    # NEW CODE - Do not update if the boundaries would be exceeded.
                    if key_tp in self.tparam_mins and (self.tparam_mins[key_tp] > self.post_tparam[key_tp].min()):
                        warnings.warn("Parameter %s posterior has NOT been updated (MIN limit exceeded.)" % key_tp,
                                      RuntimeWarning)
                        self.post_tparam[key_tp][:,:] = self.pri_tparam[key_tp][:,:]

                    if key_tp in self.tparam_maxs and (self.tparam_maxs[key_tp] < self.post_tparam[key_tp].max()):
                        warnings.warn("Parameter %s posterior has NOT been updated (MAX limit exceeded.)" % key_tp,
                                      RuntimeWarning)
                        self.post_tparam[key_tp][:,:] = self.pri_tparam[key_tp][:,:]

                    # OLD CODE - Clip tparams at min/max boundary
                    # if key_tp in self.tparam_mins and (self.tparam_mins[key_tp] > self.post_tparam[key_tp].min()):
                    #     print "WARNING: Parameter " + key_tp + " posterior has been clipped (min limit exceeded.)"
                    #     self.post_tparam[key_tp] = self.post_tparam[key_tp].clip(min=self.tparam_mins[key_tp])
                    #
                    # if key_tp in self.tparam_mins and (self.tparam_maxs[key_tp] < self.post_tparam[key_tp].max()):
                    #     print "WARNING: Parameter " + key_tp + " posterior has been clipped (max limit exceeded.)"
                    #     self.post_tparam[key_tp] = self.post_tparam[key_tp].clip(max=self.tparam_maxs[key_tp])

            # Update every model's parameter files and reset every model's state files for the next filter run.
            for i in range(self.INSTANCES):

                if self.RUN_IN_THIS_FOLDER != None:
                    kutils.enter_folder(self.RUN_IN_THIS_FOLDER)
                kutils.enter_folder(str(i))

                for key, modifyer in self.tparam_modifyer.iteritems():
                    modifyer(self.post_tparam[key][i,:], i) # Save new parameters into file.

                for key, modifyer in self.state_modifyer.iteritems():
                    modifyer(self.post_state[key][i, :], i)  # Save old states into file.

                kutils.exit_folder()
                if self.RUN_IN_THIS_FOLDER != None:
                    kutils.exit_folder()

            if state_filter_active:
                # Rerun the models (parallel) with the new parameters. We don't want to repertub the u or t params this time.
                process_list = []
                for i in range(self.INSTANCES):
                    p = mp.Process(name="Instance " + str(i), target=self._model_member_mp_,
                                   args=(i, np.random.normal, "none", False))
                    process_list.append(p)
                    p.start()

                # Join all processes
                for i in range(0, self.INSTANCES):
                    process_list[i].join(100)  # Return anyway if a process takes more then 100 seconds (is frozen)

                # Get shared memory - new pri_states
                for key in self.pri_state:
                    self.pri_state[key][:, :] = np.frombuffer(self.shared_arr_states_dict[key].get_obj()).reshape(
                        (self.INSTANCES, self.H_dict[key].shape[1]))


        else:
            # Copy prior values for parameters to posterior values.
            for key, array in self.pri_tparam.iteritems(): # Store prior for posterior
                self.post_tparam[key][:,:] = array

            # for key in self.pri_state: # Store NaN values for state innovation
            #     self.innovation_pr_loop[key][:] = np.nan

        # ------@@@@@@@@@@@@@@@@-- #
        # >>>>> STATE FILTER <<<<< #
        # ------@@@@@@@@@@@@@@@@-- #

        if state_filter_active:



            # Calculate covariances and K matricies
            for key_st, array_st in self.pri_state.iteritems():

                # Calculate predicted state
                self.pred_state[key_st] = array_st.dot(self.H_dict[key_st].T)

                self.cov_pred_dict[key_st] = np.cov(self.pred_state[key_st].T) # Generate prediction covariance
                self.cross_cov_state_dict[key_st] = kutils.cross_cov(array_st, self.pred_state[key_st]) # Generate state-prediction cross covariance
                self.K_state_dict[key_st] = kutils.calculate_K_matrix(self.cross_cov_state_dict[key_st], # Update State Kalman Gain Matrix
                                                                           self.cov_pred_dict[key_st],
                                                                           self.R_errors[key_st])


            # Update posterior state estimations
            for key_st, array_st in self.pri_state.iteritems():
                # unknown_indexes = np.where(self.perturbed_obs_dict[key_st] == np.nan)[0]
                # np.put(self.perturbed_obs_dict[key_st], unknown_indexes, -32767) # Fill these values with arbitary number
                self.innovation_st_loop[key_st] = self.perturbed_obs_dict[key_st] - self.pred_state[key_st]
                self.post_state[key_st][:,:] = (array_st.T + self.K_state_dict[key_st].dot(self.innovation_st_loop[key_st].T)).T

            # Update every model's state files.
            for i in range(self.INSTANCES):
                for key, modifyer in self.state_modifyer.iteritems():

                    if self.RUN_IN_THIS_FOLDER != None:
                        kutils.enter_folder(self.RUN_IN_THIS_FOLDER)
                    kutils.enter_folder(str(i))

                    modifyer(self.post_state[key][i,:], i) # Save new states into file.

                    kutils.exit_folder()
                    if self.RUN_IN_THIS_FOLDER != None:
                        kutils.exit_folder()
        else:
            # Copy prior values for parameters to posterior values.
            for key, array in self.pri_state.iteritems():
                self.post_state[key][:,:] = array
                self.innovation_st_loop[key] = np.zeros_like(self.perturbed_obs_dict[key])

                # Calculate predicted state
                for key_st, array_st in self.pri_state.iteritems():
                    self.pred_state[key_st] = array_st.dot(self.H_dict_init[key_st].T)

        return self.pri_tparam, self.post_tparam, self.pri_state, self.post_state

    def run_model(self, args=None):
        """
            Run the model. Uses the function supplied at __init__
            :param args: The arguments that will be passed to the function (wrapped in an Args() class). If set to none, will run with current model supplied args.
        """
        if args == None:
            return self.f(Args(self.args)) # Wrap in Args() object

        else:
            return self.f(Args(args)) # Wrap in Args() object

    # &@@@                 @@@& #
    # >> Getters and Setters << #
    # &@@@                 @@@& #


    def get_state_and_param_info(self, print_to_console = False):
        """
        Returns prior and posterior values for the last update iteration.
        :param print_to_console: Prints all info to console.
        :return: prior_param_dict, posterior_param_dict, prior_state_dict, posterior_state_dict, innovation_during_param_correction, innovation_during_state_correction
        """

        if print_to_console == True:
            print "PRIOR STATES:"
            for key, array in self.pri_state.iteritems():
                print key + ":"
                print array

            print "\nPRIOR THETAS:"
            for key, array in self.pri_tparam.iteritems():
                print key + ":"
                print array

            print "\nPOST STATES:"
            for key, array in self.post_state.iteritems():
                print key + ":"
                print array

            print "\nPOST THETAS:"
            for key, array in self.post_tparam.iteritems():
                print key + ":"
                print array

        return self.pri_tparam, self.post_tparam, self.pri_state, self.post_state, self.pred_state, self.innovation_pr_loop, self.innovation_st_loop


    def get_perturbed_obs(self, print_to_console = False):
        """
        Returns covariance for predicted state variables
        :param print_to_console: Prints all info to console. Default false
        :return: cov_pred_dict
        """

        if print_to_console == True:
            print "STATE COVS:"
            for key, array in self.perturbed_obs_dict.iteritems():
                print key + ":"
                print array

        return self.perturbed_obs_dict

    def get_pred_cov_info(self, print_to_console = False):
        """
        Returns covariance for predicted state variables
        :param print_to_console: Prints all info to console. Default false
        :return: cov_pred_dict
        """

        if print_to_console == True:
            print "STATE COVS:"
            for key, array in self.cov_pred_dict.iteritems():
                print key + ":"
                print array

        return self.cov_pred_dict


    def get_cov_info(self, print_to_console=False):
        """
        Get the current covariance matricies and K matricies for the last update iteration.
        :param print_to_console: Prints the matricies to console.
        :return: cross_cov_param_dict, cross_cov_state_dict, cov_prediction_dict, K_param_dict, K_state_dict
        """

        if print_to_console == True:
            print "THETAS CROSS COV:"
            for key, array in self.cross_cov_param_dict.iteritems():
                print key + ":"
                print array

            print "\nSTATES CROSS COV:"
            for key, array in self.cross_cov_state_dict.iteritems():
                print key + ":"
                print array

            print "\nPREDICTED COVS:"
            for key, array in self.cov_pred_dict.iteritems():
                print key + ":"
                print array

            print "\nK THETAS:"
            for key, array in self.K_param_dict.iteritems():
                print key + ":"
                print array

            print "\nK STATES:"
            for key, array in self.K_state_dict.iteritems():
                print key + ":"
                print array

        return self.cross_cov_param_dict, self.cross_cov_state_dict, self.cov_pred_dict, self.K_param_dict, self.K_state_dict

    def set_arguments(self, arg_dict):
        """
        Modifies the arguments passed to the function.
        :param arg_dict:
        :return: Nothing.
        """
        for key, value in arg_dict.iteritems():
            self.default_args[key] = value

    def set_state_getter(self, f, state_name):
        """
        Sets a function that will run inside each model directory to inject the modified state parameters into each model instance.
        Will be passed no arguments.
        :param f: Function to modify state files.
        :return: Nothing.
        """
        self.state_getter[state_name] = f

    def set_uparam_modifyer(self, f, uparam_name):
        """
        Sets a function that will run inside each model directory to inject the modified u-parameters into each model instance.
        ARGUMENTS PASSED TO USER FUNCTION - (array: array of values)
        :param f: Function to modify uparam files.
        :return: Nothing.
        """
        self.uparam_modifyer[uparam_name] = f

    def set_tparam_modifyer(self, f, tparam_name):
        """
        Sets a function that will run inside each model directory to inject the modified theta into each model instance.
        ARGUMENTS PASSED TO USER FUNCTION - (array: array of values)
        :param f: Function to modify tparam files.
        :return: Nothing.
        """
        self.tparam_modifyer[tparam_name] = f

    def set_state_modifyer(self, f, state_name):
        """
        Sets a function that will run inside each model directory to inject the modified state parameters into each model instance.
        ARGUMENTS PASSED TO USER FUNCTION - (array: array of values)
        :param f: Function to modify state files.
        :return: Nothing.
        """
        self.state_modifyer[state_name] = f


    def set_param_mask_mat(self, mask_mat_param, mask_mat_state, param_name):
        """
        Allows user to modify a theta parameter's mask matrix. Masked indicies are left untouched by both
        perturbation and correction.
        :param mask_mat_parameter: A matrix of 0's (unmasked) and 1's (masked) of same size as the parameter matrix
        :param mask_mat_state: A matrix of 0's (unmasked) and 1's (masked) of same size as the state matrix associated with the parameter
        :param param_mane: Parameter to which the masking matrix belongs.
        :return: Nothing.
        """
        self.tparam_mask_dict[param_name] = mask_mat_param
        self.state_mask_dict[param_name] = mask_mat_state

    def prepare_members(self, perterb_tparams=True):
        """
        Propogates nessesary files into every instance via self.required_files. Can be used to setup or reset the filter.
        :return: Nothing
        """
        for instance in range(self.INSTANCES): # Create or enter each instance
            # First, we enter the folder we wish to run the instanceS in
            if self.RUN_IN_THIS_FOLDER != None:
                kutils.enter_folder(self.RUN_IN_THIS_FOLDER)
            kutils.enter_folder(str(instance))
            for loc in self.required_files:
                copyfile(loc, loc.split('/')[-1]) # Copy file into current directory {{ MAYBE THIS IS BAD CODING SOMEHOW }}

            kutils.exit_folder()
            if self.RUN_IN_THIS_FOLDER != None:
                kutils.exit_folder()

        if perterb_tparams:
            # Perturb every model's parameter files
            for i in range(self.INSTANCES):

                if self.RUN_IN_THIS_FOLDER != None:
                    kutils.enter_folder(self.RUN_IN_THIS_FOLDER)
                kutils.enter_folder(str(i))

                for key, modifyer in self.tparam_modifyer.iteritems():
                    modifyer(self.post_tparam[key][i, :], i)  # Save new parameters into file.

                kutils.exit_folder()
                if self.RUN_IN_THIS_FOLDER != None:
                    kutils.exit_folder()


    # %%%%%%%%%%%%%%%%%%%%%%%%%%% #
    # >    Child Functions    < #
    # %%%%%%%%%%%%%%%%%%%%%%%%%%% #

    def _model_member_mp_(self, i, rand_f=np.random.normal, uparams_mode="perturb", perturb_tparams=True):
        """
        Runs a single instance of the model and collects results. Perturbs U-param and T-params beforehand unless otherwise specified.
        :param i: Instance of the model being run
        :param rand_f: Random function that generates the mean
        :param perturb_uparams: If set to true, runs the uparam_modifyer to modify uparams. Default True.
        :param perturb_tparams: If set to "perturb", perturbs uparams. If set to "unmodified", copies normal uparams into folder. If set to "none" (or anything else,) will not copy the params.  Default "perturb".
        :return: Nothing
        """

        # This makes sure these random numbers are different from the other parallel processes
        np.random.seed()

        # First, we enter the folder we wish to run the instance in
        if self.RUN_IN_THIS_FOLDER != None:
            kutils.enter_folder(self.RUN_IN_THIS_FOLDER)
        kutils.enter_folder(str(i))

        # Create numpy wrappers for our parallel computing arrays. Then assign placeholder arrays in case the thread locks up.

        np_wrappers_tparams = {}
        for key, array in self.tparam_dict.iteritems():
            np_wrappers_tparams[key] = np.frombuffer(self.shared_arr_params_dict[key].get_obj()).reshape((self.INSTANCES, array.flatten().shape[0]))
            np_wrappers_tparams[key][i, :] = self.post_tparam[key][i,:]

        np_wrappers_states = {}
        for key, array in self.initial_state_dict.iteritems():
            np_wrappers_states[key] = np.frombuffer(self.shared_arr_states_dict[key].get_obj()).reshape((self.INSTANCES, array.flatten().shape[0]))
            np_wrappers_states[key][i, :] = self.post_state[key][i, :]

        # Prepare uparams
        if uparams_mode == "perturb":
            for key, array in self.uparam_dict.iteritems():
                self.uparam_modifyer[key](kutils.add_scalar_to_array(rand_f, array, clip=0, filter_unknowns=self.UNKNOWN_PLACEHOLDER_UPARAMS, scale=self.U_errors[key]), i)
        elif uparams_mode == "unmodified":
            for key, array in self.uparam_dict.iteritems():
                self.uparam_modifyer[key](array, i)

        # Prepare tparams
        if perturb_tparams == True:
            for key, array in self.post_tparam.iteritems():


                # Kernel smoothing - Add in some of the mean of the ensemble.
                tparam_mean = np.mean(array, axis=0)
                tparam_mixed = self.param_a * array[i,:] + (1 - self.param_a) * tparam_mean

                array_var = np.var(array, axis=0) # This is no longer a global variable

                # Make anything lower then self.MIN_VAR that value, then mask
                var_final = ma.masked_array((array_var * self.param_h * self.param_h).clip(min=self.MIN_VAR), self.tparam_mask_dict[key])

                # Masked array
                ma_array = ma.masked_array(tparam_mixed, self.tparam_mask_dict[key])

                # Add Parameter Noise
                np_wrappers_tparams[key][i, :] = array[i,:]
                perturbed_tparams = kutils.add_noise_to_array(rand_f, ma_array, clip=0, filter_unknowns=self.UNKNOWN_PLACEHOLDER_TPARAMS, scale=var_final.compressed()).clip(min=self.tparam_mins[key],max=self.tparam_maxs[key])
                np_wrappers_tparams[key][i, :][~perturbed_tparams.mask] = perturbed_tparams.compressed()
                self.tparam_modifyer[key](np_wrappers_tparams[key][i, :], i)

        if self.SEND_INSTANCE_NUMBER_TO_MODEL:
            # Run model, send i argument
            self.this_objects_args = dict(self.args)
            self.this_objects_args["i"] = i
            self.run_model(self.this_objects_args)
        else:
            # Run model
            self.run_model()

        # Get model output.
        for key, array in self.initial_state_dict.iteritems():
            np_wrappers_states[key][i, :] = self.state_getter[key](i)

    # %%%%%%%%%%%%%%%%%%%%%%%%%%%% #
    # >    Graphing Functions    < #
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%% #


