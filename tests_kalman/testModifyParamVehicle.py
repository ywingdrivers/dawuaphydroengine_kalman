import nose
import numpy as np

# ~~~~~~~~~~~
# testModifyParamVehicle.py
# Testing the Modify_Param_Vehicle() class from kalman scripts. Currently unused.
# Revisited 7/24/2018
# ~~~~~~~~~~~

# from context import Modify_Param_Vehicle, utils
#
# class Test_Modify_Param_Raster(object):
#     def setUp(self):
#         self.mpv = Modify_Param_Vehicle()
#         self.raster_difference = utils.RasterParameterIO("test_data/differenceRaster.tif").array[0,:,:]
#         self.raster_constant = utils.RasterParameterIO("test_data/constant_4.tif").array[0,:,:]
#
#     def test_build_swe_std_raster(self):
#         raster = self.mpv.build_swe_std_raster("test_data/sweJSON.txt","test_data/sweObsRaster.tif","test_data/vehicleRaster.tif")
#         #np.testing.assert_array_equal(self.raster, raster)
#
#     def test_build_raster_difference(self):
#         raster = self.mpv.build_raster_difference("test_data/swe_20121204.tif","test_data/vehicleRaster.tif","test_data/differenceRaster.tif")
#         np.testing.assert_array_equal(self.raster_distance, raster)
#
#     def test_build_raster_difference(self):
#         raster = self.mpv.build_raster_constant("test_data/sweObsRaster.tif","test_data/constant_4.tif",5)
#         np.testing.assert_array_equal(self.raster_constant, raster)
