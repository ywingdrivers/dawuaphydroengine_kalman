import nose
import numpy as np
import utils
import json
# ~~~~~~~~~~~
# testkalmanvehicleutils.py
# Testing the kalmanvehcileutils library in kalman/kalmanvehcile.
# Revisited 7/24/2018
# ~~~~~~~~~~~

from context import prepare_THIESSEN_matrix, prepare_H_matrix_swe, prepare_H_matrix_streamflow, date_to_local_unix_timestamp, graph_initialization_simple, graph_initialization_full, update_dict

class test_kvehicleutils(object):
    def setUp(self):


        # Variables for testing prepare THIESSEN matrix
        self.test_THIESSEN = np.array([[56,56,63],[56,63,63]])
        self.test_key_list = [56,63]

        # Variables for testing prepare H matrix for SWE
        self.test_SWE_positions = [[-113.32868,48.30305]]
        self.test_SWE_on_20130419 = utils.RasterParameterIO("test_data/swe_20130419.tif", 1)


        # Variables for testing prpare H matrix for Streamflow
        with open("test_data/streamflows_test.json", 'r') as f:
            self.sf_json = json.load(f)
            self.sf_json_node_timestep_242_formatted = []
            for item in self.sf_json["nodes"]:
                self.sf_json_node_timestep_242_formatted.append(item["dates"][242]["flow"])

        with open("kalman_data/streamobs.json", 'r') as f:
            sf_obs_json = json.load(f)
            self.test_sf_obs_json = {"181": sf_obs_json["181"]}

        # Variables for 'date to local timestamp'
        self.test_date = "5/05/2015"


        # Variables for setup graph 'simple' and 'full'
        self.test_theta_param_dict = {"test_param" : np.array([[3,3,3],[3,3,3]])}
        self.test_initial_state_dict = {"test_state": np.array([[0, 0, 0, 0],[0, 0, 0, 0]])}
        self.test_initial_obs_dict = {"test_state": np.array([[2, 1, 4],[5, 3, 1]])}

        # Variables for update dict
        self.test_dict_info = {"arr1" : np.ones((2,3)), "arr2" : np.ones((2,1))}
        self.test_dict = {"arr1" : np.zeros((2,3,12)), "arr2" : np.zeros((2,1,12))}

        # Variables for generate final params dict
        self.test_final_dict = {"ddf" : np.arange(27).reshape((3,3,3))}
        with open("kalman_data/sweobs.json", 'r') as f:
            swe_obs_json = json.load(f)


        # Correct answers
        self.correct_prepared_THIESSEN = np.array([[0, 0, 1], [0, 1, 1]])
        self.correct_Hswe = 469.43756103515625
        self.correct_Hsf = self.sf_json["nodes"][180]["dates"][242]["flow"] # This is like 5.11ish
        self.correct_timestamp = 1430784000000
        self.correct_theta_param = np.zeros((10,6,30))
        self.correct_theta_param[:,:,0] = self.test_theta_param_dict["test_param"].flatten()
        self.correct_initial_state = np.zeros((10,8,30))
        self.correct_innovation = np.zeros((10,6,30))
        self.correct_dict = {"arr1": np.array(self.test_dict["arr1"]), "arr2": np.array(self.test_dict["arr2"])}
        self.correct_dict["arr1"][:,:,10] = self.test_dict_info["arr1"]
        self.correct_dict["arr2"][:,:,10] = self.test_dict_info["arr2"]

    def test_prepare_THIESSEN_matrix(self):

        test_THIESSEN_matrix = prepare_THIESSEN_matrix(self.test_THIESSEN,self.test_key_list)
        np.testing.assert_array_equal(test_THIESSEN_matrix, self.correct_prepared_THIESSEN)

    def test_prepare_H_matrix_swe(self):
        H_mat = prepare_H_matrix_swe(self.test_SWE_positions, self.test_SWE_on_20130419)
        prediction = H_mat.dot(self.test_SWE_on_20130419.array.flatten())
        np.testing.assert_array_equal(self.correct_Hswe, prediction)

    def test_prepare_H_matrix_streamflow(self):
        H_mat = prepare_H_matrix_streamflow(self.sf_json, self.test_sf_obs_json)
        prediction = H_mat.dot(np.array(self.sf_json_node_timestep_242_formatted))

        # Test against affine generated H array
        # test_H_mat_affine = self._test_affine_of_sf_(self.test_sf_obs_json, self.test_SWE_on_20130419)
        # np.testing.assert_array_equal(H_mat, test_H_mat_affine)

        # Test that the correct value is pulled up
        np.testing.assert_array_equal(self.correct_Hsf, prediction)

    def test_date_to_local_unix_timestamp(self):
        local_timestamp = int(date_to_local_unix_timestamp(self.test_date))
        np.testing.assert_array_equal(local_timestamp, self.correct_timestamp)

    # def test_graph_initilization_simple(self):
    #     pri_t, post_t, pri_s, post_s, innov_pr, innov_st = graph_initialization_simple(10,
    #                                                                                          30,
    #                                                                                          self.test_theta_param_dict,
    #                                                                                          self.test_initial_state_dict,
    #                                                                                          self.test_initial_obs_dict)
    #
    #     np.testing.assert_array_equal(self.correct_theta_param, pri_t["test_param"])
    #     np.testing.assert_array_equal(self.correct_theta_param, post_t["test_param"])
    #     np.testing.assert_array_equal(self.correct_initial_state, pri_s["test_state"])
    #     np.testing.assert_array_equal(self.correct_initial_state, post_s["test_state"])
    #     np.testing.assert_array_equal(self.correct_innovation, innov_st["test_state"])
    #     np.testing.assert_array_equal(self.correct_innovation, innov_pr["test_state"])


    def test_graph_initilization_full(self):
        pri_t, post_t, pri_s, post_s, innov_pr, innov_st, pred_cov = graph_initialization_full(10,
                                                                                             30,
                                                                                             self.test_theta_param_dict,
                                                                                             self.test_initial_state_dict,
                                                                                             self.test_initial_obs_dict)

        np.testing.assert_array_equal(self.correct_theta_param, pri_t["test_param"])
        np.testing.assert_array_equal(self.correct_theta_param, post_t["test_param"])
        np.testing.assert_array_equal(self.correct_initial_state, pri_s["test_state"])
        np.testing.assert_array_equal(self.correct_initial_state, post_s["test_state"])
        np.testing.assert_array_equal(self.correct_innovation, innov_st["test_state"])
        np.testing.assert_array_equal(self.correct_innovation, innov_pr["test_state"])

    def test_update_dict(self):
        update_dict(self.test_dict_info, self.test_dict,10)

        np.testing.assert_array_equal(self.test_dict["arr1"], self.correct_dict["arr1"])
        np.testing.assert_array_equal(self.test_dict["arr2"], self.correct_dict["arr2"])