import nose
import numpy as np

# ~~~~~~~~~~~
# testArgs.py
# Testing the Modify_Param_Raster() class from kalman scripts.
# Revisited 7/24/2018
# ~~~~~~~~~~~

from context import Modify_Param_Raster, utils

class Test_Modify_Param_Raster(object):
    def setUp(self):
        self.pt = Modify_Param_Raster()
        self.raster = utils.RasterParameterIO("test_data/compareRaster.tif").array[0,:,:]

    def test_modify_raster(self):
        raster = self.pt.modify_raster("test_data/sweObsRaster.tif","test_data/testNewRaster.tif",self._test_callback_)
        np.testing.assert_array_equal(self.raster, raster)


    def _test_callback_(self, value, x, y):
        """
        Adds 1 to a given value, returns. Doesn't do anything with x or y.
        :param value: Value to add 1 to.
        :param x: Does nothing (for callback)
        :param y: Does nothing (for callback)
        :return: value + 1
        """
        return int(value)+1

if __name__ == '__main__':
    my_pt = Test_Modify_Param_Raster()
    my_pt.setUp()
    my_pt.test_modify_raster()
