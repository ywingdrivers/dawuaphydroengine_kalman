import sys, os


# ~~~~~~~~~~~
# testArgs.py
# Testing the Args() class from kalman/kalmanengine/dualhydrokalmanfilter.py
# Revisited 7/24/2018
# ~~~~~~~~~~~

from context import Args

# Make sure we're useing the correct python and path!
print(sys.version)
print(os.path.dirname(sys.executable))

argDict = {'init_date': "09/1/2012",
            'precip': "precip_F2012-09-01_T2013-08-31.nc",
            'tmin': "tempmin_F2012-09-01_T2013-08-31.nc",
            'tmax': "tempmax_F2012-09-01_T2013-08-31.nc",
            'params': "wm_param_files_test.json",
            'network_file': "rivout.shp",
            'basin_shp': "subsout.shp"}

class Test_Args(object):
    def setUp(self):
        # Varaibles

        self.args = Args(argDict)

    def test_Args(self):
        self.args.init_date == argDict['init_date']
