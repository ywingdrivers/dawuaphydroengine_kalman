import nose
import numpy as np
import json
import utils
from dateutil.parser import parse
import datetime
# ~~~~~~~~~~~
# testdualensemblekalmanfilter.py
# Testing the Dual_Eensemble_Kalman_Filter class in kalman/kalmanengine.
# Created 8/2/2018
# ~~~~~~~~~~~

from context import Dual_Ensemble_Kalman_Filter, hydrovehicle, enter_folder, exit_folder, date_to_local_unix_timestamp, prepare_THIESSEN_matrix, prepare_H_matrix_streamflow, prepare_H_matrix_swe, Dawuaphydroengine_Kalman_Getters_And_Setters

class test_Dual_Ensemble_Kalman_Filter(object):
    def setUp(self):

        # Variables for testing Dual_State_Ensemble_Kalman_Filter - alot of this is modifed code from kalmanvehicle.py
        self.f = hydrovehicle.main # Link to hydrovehicle
        self.R_errors = {"swe": .1/0.039370078740158, "streamflow": 3}
        self.Q_errors = {"pp_temp_thres": 5, "ddf": .1}
        self.U_errors = {"tmin": 4, "tmax":4, "precip": 4}
        self.init_date = "9/1/2012"

        self.TIMESTEPS = 365 # We have this number of obserations
        self.INSTANCES = 12

        enter_folder("kalman_data")

        ## Parameters ##
        theta_param_dict = {} # Get the initial theta parameter values - geoTIFF files to numpy arrays
        par_states = {} # The name of each parameter's associated states
        random_rasterio_file = "" # Need this for the filter
        with open("test_param_file.json") as json_file:
            par_files = json.load(json_file)
            for key, value in par_files.items():
                theta_param_dict[key] = utils.RasterParameterIO(value[0], 1).array.flatten()
                par_states[key] = value[1]
                random_rasterio_file = value[0]

        ## U_Vars ##
        self.uparam_dict = {}
        with open("test_uvar_file.json") as json_file: # Get the initial uparam values - netCDF files to numpy arrays
            uparam_files = json.load(json_file)
            for key, value in uparam_files.items():
                netCDF = utils.RasterParameterIO(value)
                self.uparam_dict[key] = netCDF.array

        # We need an initial dictionary of our u parameters to pass to the model
        self.initial_uparam_dict = {}
        for key, array in self.uparam_dict.iteritems():
            self.initial_uparam_dict[key] = self.uparam_dict[key][0,:,:].flatten()

        ## Observations ##
        self.obs_dict = {} # Observed values
        obs_pos_dict = {} # Each node's lng/lat

        with open("test_obs_file.json") as json_file: # Get the initial uparam values - netCDF files to numpy arrays
            obs_files = json.load(json_file)

        # Parse SWE data
        with open(obs_files["swe"], 'r') as f:
            swe_obs_json = json.load(f)
            long_lat_list_swe = []

            # Link each row with a specific key (!! - this is really really important because there's no guarantee .items() will iterate over the dict the same way each time - !!)
            pos_list_swe = []
            for ind, (key, node) in enumerate(swe_obs_json.items()):
                pos_list_swe.append(key)

            # Loop through and parse SWE JSON file.
            for t in range(self.TIMESTEPS):
                the_day = (parse(self.init_date) + t * datetime.timedelta(seconds=86400)).strftime("%m/%d/%Y")

                unix_time = date_to_local_unix_timestamp(the_day, "America/Denver")

                obsList = []
                for key in pos_list_swe:

                    if unix_time in swe_obs_json[key]['values']:
                        obsList.append(swe_obs_json[key]['values'][unix_time]['values'] / 0.039370078740158)  # inches to mm

                    else:
                        print "MISSING SWE VALUE"
                        obsList.append(np.nan)

                    if t == 0: # On the first iteration we'll build a list of lat and long points! <3
                        # Append the long and lat of each node to an array of arrays
                        long_lat_list_swe.append([float(node[u'long']), float(node[u'lat'])])

                if t == 0:  # First iteration, create matrix
                    self.obs_dict["swe"] = np.array(obsList)
                else:  # Append to matrix
                    self.obs_dict["swe"] = np.vstack((self.obs_dict["swe"], obsList))


            obs_pos_dict["swe"] = np.array(long_lat_list_swe)
            swe_THIESSEN_matrix = prepare_THIESSEN_matrix(utils.RasterParameterIO("THIESSENRaster.tif", 1).array,
                                                              swe_obs_json)

        # Parse streamflow data
        with open(obs_files["streamflow"], 'r') as f:
            sf_obs_json = json.load(f)
            long_lat_list_sf = []

            # Link each row with a specific key (!! - this is really really important because there's no guarantee .items() will iterate over the dict the same way each time - !!)
            pos_list_sf = []
            for ind, (key, node) in enumerate(sf_obs_json.items()):
                pos_list_sf.append(key)

            # Loop through and parse Streamflow JSON file.
            for t in range(self.TIMESTEPS):
                the_day = (parse(self.init_date) + t * datetime.timedelta(seconds=86400)).strftime("%m/%d/%Y")

                unix_time = date_to_local_unix_timestamp(the_day, "America/Denver")

                obsList = []
                for key in pos_list_sf:

                    if unix_time in sf_obs_json[key]:
                        obsList.append(sf_obs_json[key][unix_time]['value'])  # no conversion nessesary, already converted.

                    else:
                        obsList.append(np.nan)

                    # Append the long and lat of each node to an array of arrays
                    long_lat_list_sf.append([float(node[u'long']), float(node[u'lat'])])

                if t == 0:  # First iteration, create matrix
                    self.obs_dict["streamflow"] = np.array(obsList)
                else:  # Append to matrix
                    self.obs_dict["streamflow"] = np.vstack((self.obs_dict["streamflow"], obsList))

            obs_pos_dict["streamflow"] = np.array(long_lat_list_sf)

        # Setup initial obs dict
        initial_obs_dict = {"swe": self.obs_dict["swe"][0,:],
                            "streamflow": self.obs_dict["streamflow"][0, :]}

        # Setup rasterIO object
        rasterIOObject = utils.RasterParameterIO(random_rasterio_file, 1)

        ##### Build H matricies #####
        H_dict = {}

        ### SWE - H matrix ###
        H_dict["swe"] = prepare_H_matrix_swe(obs_pos_dict["swe"], rasterIOObject)

        #### Streamflow - H matrix ###
        with open("streamflows.json", 'r') as f:
            sf_json = json.load(f)

        # Create streamflow H matrix
        H_dict["streamflow"] = prepare_H_matrix_streamflow(sf_json, sf_obs_json)

        # Initial states
        initial_state_dict = {}

        # SWE is 0 across the board
        initial_state_dict["swe"] = np.zeros_like(rasterIOObject.array).flatten()

        # Streamflow is set to 0 for now.
        initial_state_dict["streamflow"] = np.zeros(len(sf_json["nodes"]))

        # Get true DDF parameters by multiplying via swe H matrix.
        # theta_param_dict["ddf"] = theta_param_dict["ddf"].dot(H_dict["swe"].T)
        theta_param_dict["pp_temp_thres"] = theta_param_dict["pp_temp_thres"].dot(H_dict["swe"].T)


        init_args = {
            'init_date': self.init_date,
            'precip': "precipmod.tif",
            'tmin': "tminmod.tif",
            'tmax': "tmaxmod.tif",
            'params': "../../kalman_data/wm_param_files_test.json",
            'network_file': "../../kalman_data/rivout.shp",
            'basin_shp': "../../kalman_data/subsout.shp",
            'restart': False}

        exit_folder()

        # Set up our getters and setters contained in the 'Dawuaphydroengine_Kalman_Getters_And_Setters' class:
        dkgas = Dawuaphydroengine_Kalman_Getters_And_Setters(rasterIOObject, swe_THIESSEN_matrix)

        # Initiate Dual Ensemble Kalman Filter
        self.dekf = Dual_Ensemble_Kalman_Filter(self.f, init_args, theta_param_dict, self.initial_uparam_dict, initial_obs_dict, initial_state_dict, H_dict, par_states, required_files=["../../kalman_data/test_ddf.tif"], instances=self.INSTANCES, R_errors=self.R_errors, Q_errors=self.Q_errors, U_errors=self.U_errors, tparam_mins={"ddf": 1, "pp_temp_thres": -100}, tparam_maxs={"ddf": 15, "pp_temp_thres": 100}, unknown_placeholder_uparams=-32767, run_in_this_folder="test_dekfrun")

        # Set up our getters and setters.
        self.dekf.set_state_getter(dkgas.get_state_swe, "swe")
        self.dekf.set_state_getter(dkgas.get_state_streamflow, "streamflow")

        self.dekf.set_uparam_modifyer(dkgas.save_u_precip, "precip")
        self.dekf.set_uparam_modifyer(dkgas.save_u_tmin, "tmin")
        self.dekf.set_uparam_modifyer(dkgas.save_u_tmax, "tmax")

        # self.dekf.set_tparam_modifyer(dkgas.save_t_ddf, "ddf")
        self.dekf.set_tparam_modifyer(dkgas.save_t_pp_temp_thres, "pp_temp_thres")

        self.dekf.set_state_modifyer(dkgas.save_state_swe, "swe")
        self.dekf.set_state_modifyer(dkgas.save_state_streamflow, "streamflow")

        self.current_args = dict(init_args)

        #>> TESTING VARIABLES <<#
        self.correct_required_file = utils.RasterParameterIO("kalman_data/test_ddf.tif", 1).array

    def test_simulate_full(self):
        # First, check to see if the instance folders were prepared correctly

        # DDF
        # instance_10_tparam_file = utils.RasterParameterIO("test_dekfrun/10/test_ddf.tif", 1).array
        # instance_8_tparam_file = utils.RasterParameterIO("test_dekfrun/8/test_ddf.tif", 1).array
        # assert not np.array_equal(self.correct_required_file, instance_10_tparam_file)
        # assert not np.array_equal(instance_10_tparam_file, instance_8_tparam_file)

        self.dekf.simulate()

        # DDF
        # instance_10_tparam_file_perturbed = utils.RasterParameterIO("test_dekfrun/10/test_ddf.tif", 1).array
        # instance_6_tparam_file_perturbed = utils.RasterParameterIO("test_dekfrun/6/test_ddf.tif", 1).array
        # assert not np.array_equal(self.correct_required_file, instance_10_tparam_file_perturbed)
        # assert not np.array_equal(instance_6_tparam_file_perturbed, instance_10_tparam_file_perturbed)

        instance_10_uparam_file_perturbed = utils.RasterParameterIO("test_dekfrun/10/tmaxmod.tif", 1).array.flatten()
        instance_2_uparam_file_perturbed = utils.RasterParameterIO("test_dekfrun/2/tmaxmod.tif", 1).array.flatten()
        assert not np.array_equal(instance_10_uparam_file_perturbed, instance_2_uparam_file_perturbed)

    def test_simulate_noperturb(self):
        # First, check to see if the instance folders were prepared correctly
        self.dekf.prepare_members(perterb_tparams=False)
        instance_10_tparam_file = utils.RasterParameterIO("test_dekfrun/10/test_ddf.tif", 1).array
        np.testing.assert_array_equal(self.correct_required_file, instance_10_tparam_file)

        self.dekf.simulate(uparams_mode="unmodified", perturb_tparams=False)
        instance_10_tparam_file_perturbed = utils.RasterParameterIO("test_dekfrun/10/test_ddf.tif", 1).array
        np.testing.assert_array_equal(self.correct_required_file, instance_10_tparam_file_perturbed)

        instance_10_uparam_file_perturbed = utils.RasterParameterIO("test_dekfrun/10/tmaxmod.tif", 1).array.flatten()
        np.testing.assert_array_equal(instance_10_uparam_file_perturbed, self.initial_uparam_dict["tmax"])



    def test_update_full(self):

        cur_obs_dict = {"swe": self.obs_dict["swe"][0, :],
                        "streamflow": self.obs_dict["streamflow"][0, :]}

        self.dekf.update(obs_dict=cur_obs_dict)

        instance_10_tparam_file_perturbed = utils.RasterParameterIO("test_dekfrun/10/test_ddf.tif", 1).array


    def test_update_null(self):
        # Using timestep 40 because there are null values for streamflow
        CUR_TS = 40
        cur_obs_dict = {"swe": self.obs_dict["swe"][CUR_TS, :],
                        "streamflow": self.obs_dict["streamflow"][CUR_TS, :]}



        self.current_args['init_date'] = (parse(self.init_date) + CUR_TS * datetime.timedelta(seconds=86400)).strftime("%m/%d/%Y")
        self.current_args['restart'] = True

        self.dekf.simulate(arg_dict=self.current_args)
        self.dekf.update(obs_dict=cur_obs_dict, arg_dict=self.current_args)

        cur_pri_t, cur_post_t, cur_pri_s, cur_post_s, cur_innov_pr, cur_innov_st = self.dekf.get_state_and_param_info()
        cross_cov_param_dict, cross_cov_state_dict, cov_prediction_dict, K_param_dict, K_state_dict = self.dekf.get_cov_info()

        # Remove NAN values
        x = self.obs_dict["streamflow"][CUR_TS, :]
        no_nans = x[~np.isnan(x)]

        try:
            assert cov_prediction_dict["streamflow"].shape == (no_nans.size, no_nans.size)
        except AssertionError as e:
            e.args += ("Array shapes aren't equal ", cov_prediction_dict["streamflow"].shape, (no_nans.size, no_nans.size), "... so cov dicts are incorrectly calculated")
            raise

        try:
            assert cross_cov_state_dict["streamflow"].shape[1] == no_nans.size
        except AssertionError as e:
            e.args += ("Array shapes in spot 1 aren't equal ", cross_cov_state_dict["streamflow"].shape[1], no_nans.size, "... so cov dicts are incorrectly calculated")
            raise

        try:
            assert not np.isnan(cur_post_s["streamflow"]).any()
        except AssertionError as e:
            e.args += "Array contains nan values"
            raise

