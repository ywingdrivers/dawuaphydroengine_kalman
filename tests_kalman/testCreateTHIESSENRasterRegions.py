import nose
import numpy as np

# ~~~~~~~~~~~
# testCreateTHIESSENRasterRegions.py
# Testing the Create_THESSEN_Raster_Regions class from kalman scripts.
# Revisited 7/24/2018
# ~~~~~~~~~~~

from context import Create_THIESSEN_Raster_Regions, utils

class test_Create_THIESSEN_Raster_Regions(object):
    def setUp(self):
        self.pt = Create_THIESSEN_Raster_Regions('test_data/sweJSON.txt', 'test_data/precip_template.nc')
        self.raster = utils.RasterParameterIO("test_data/sweObsRaster.tif").array[0,:,:]

    def test_build_raster(self):
        raster = self.pt.build_raster("test_data/testTHIESSENRaster.tif")
        np.testing.assert_array_equal(self.raster, raster)
