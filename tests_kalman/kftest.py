import sys, os
import numpy as np
import pyximport
pyximport.install()
from context import kalmanvehicle
import argparse

# Make sure we're useing the correct python and path!
print(sys.version)
print(os.path.dirname(sys.executable))


class Print_K_F(object):
    def set(self, init_date, instances, timesteps, r_sf, r_swe, q_ddf, u_error):

        print "##############################"
        print "Setting Up Kalman Filter!"
        print "R SWE Obs: \t\t\t\t\t\t", r_swe
        print "R STR Obs: \t\t\t\t\t\t", r_sf
        print "Q  DDF: \t\t\t\t\t", q_ddf
        print "u_error (precip, tempmin, tempmax): \t\t\t\t", u_error
        print "Ensemble Instances: \t", instances
        print "Timesteps: \t\t\t\t", timesteps
        print "Starting: \t\t\t\t", init_date
        print "##############################\n\n"


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Test kalman filter')
    parser.add_argument('--dev', dest='dev', action='store_true')
    parser.add_argument('--generate_pickles', action='store_true', dest="generate_pickles")
    parser.add_argument('-t', action="store", dest="ts", type=int, default=365)
    parser.add_argument('-i', action="store", dest="ins", type=int, default=10)
    parser.add_argument('-rswe', action="store", dest="r_swe", type=float, default=.1/0.039370078740158)
    parser.add_argument('-rstr', action="store", dest="r_sf", type=float, default=3)
    parser.add_argument('-qddf', action="store", dest="q_ddf", type=float, default=1)
    parser.add_argument('-qthres', action="store", dest="q_pp_temp_thres", type=float, default=0.01)
    parser.add_argument('-qsoilb', action="store", dest="q_soil_beta", type=float, default=.05)
    parser.add_argument('-qsoilmax', action="store", dest="q_soil_max_wat", type=float, default=1)
    parser.add_argument('-qaet', action="store", dest="q_aet_lp_param", type=float, default=.1)
    parser.add_argument('-utemp', action="store", dest="u_error_temp", type=float, default=6.)
    parser.add_argument('-uprecip', action="store", dest="u_error_precip", type=float, default=10.)
    # parser.add_argument('-smnoise', action="store", dest="sm_noise", type=float, default=.97)
    # Old A Algorithm: (.97*3 - 1)/(2*.97)
    parser.add_argument('-sma', action="store", dest="sm_a", type=float, default=.47)
    parser.add_argument('-smh', action="store", dest="sm_h", type=float, default=(1-.47**2)**.5)
    parser.add_argument('-minvar', action="store", dest="min_var", type=float, default=0.01)
    parser.add_argument('-initdate', action ="store", dest="initdate", default="9/1/2012")
    parser.add_argument('--no_state_correction', action="store_false", dest="state_correction")
    parser.add_argument('--no_graphs', action="store_false", dest="print_graphs")
    parser.add_argument('--nrr_module_active', action="store_true", dest="nrr_module_active")
    parser.add_argument('--debug', action="store_true", dest="debug")

    args = parser.parse_args()

    param_file = "test_param_file.json"
    uvar_file = "test_uvar_file.json"
    obs_file = "test_obs_file.json"

    if args.state_correction == False:
        param_file = "test_param_file_nostatecorr.json"


    kalmanvehicle.main(param_file, uvar_file, obs_file, args.initdate, args.ts, data_folder="kalman_data/", instances=args.ins, R_errors={"swe": args.r_swe, "streamflow": args.r_sf}, Q_errors={"ddf": args.q_ddf, "pp_temp_thres": args.q_pp_temp_thres, "soil_beta": args.q_soil_beta, "aet_lp_param": args.q_aet_lp_param, "soil_max_wat": args.q_soil_max_wat}, U_errors={"tmin": args.u_error_temp, "tmax":args.u_error_temp, "precip": args.u_error_precip}, smoothing_params = {"a": args.sm_a, "h": args.sm_h}, min_var=args.min_var, generate_pickles = args.generate_pickles, unknown_placeholder_uparams=-32767, unknown_placeholder_obs=np.nan, run_in_this_folder="dekfrun", run_state_correction=args.state_correction, nrr_module_active=args.nrr_module_active, create_graphs=args.print_graphs, debug=args.debug)
