import nose
import numpy as np
import json
import utils
from dateutil.parser import parse
import datetime
# ~~~~~~~~~~~
# testdualensemblekalmanfilter.py
# Testing the Dual_Eensemble_Kalman_Filter class in kalman/kalmanengine.
# Created 8/2/2018
# ~~~~~~~~~~~

from context import Dawuaphydroengine_Kalman_Getters_And_Setters, prepare_THIESSEN_matrix, enter_folder, exit_folder

class test_Dawuaphydroengine_Kalman_Getters_And_Setters(object):
    def setUp(self):


        # Class needs a rasterIO object and a THIESSEN matrix object

        # rasterIO Object
        rasterIOObject_THIESSENRaster = utils.RasterParameterIO("kalman_data/THIESSENRaster.tif", 1)

        # THIESSEN matrix object
        with open("kalman_data/sweobs.json", 'r') as f:
            swe_obs_json = json.load(f)

        swe_THIESSEN_matrix = prepare_THIESSEN_matrix(rasterIOObject_THIESSENRaster.array,
                                                      swe_obs_json)
        # Initialize
        self.dkgas = Dawuaphydroengine_Kalman_Getters_And_Setters(rasterIOObject_THIESSENRaster, swe_THIESSEN_matrix)

        # Test variables
        self.test_arr = np.arange(124*363)



    def test_save_u_precip(self):
        enter_folder("test_dkgas")
        self.dkgas.save_u_precip(self.test_arr, None)
        exit_folder()

