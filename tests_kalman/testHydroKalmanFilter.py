import sys, os
import math
import matplotlib.pyplot as plt
import nose
import numpy as np
from .context import Kalman_Filter, Args, utils, hydrovehicle

# ~~~~~~~~~~~
# testHydroKalmanFilter.py
# Testing the old Kalman filter. Currently unused.
# Revisited 7/24/2018
# ~~~~~~~~~~~


# # Make sure we're useing the correct python and path!
# print(sys.version)
# print(os.path.dirname(sys.executable))
#
# class Test_Kalman_Filter(object):
#     def setUp(self):
#         # Varaibles
#         f = hydrovehicle.main
#         A = np.array([[2,-1]])
#         Q_state = 10
#         Q_param = .075 # Currently for prameter error variance
#         R = 1 # Observation error
#         u_error = .1 # U params error
#         instances = 10
#
#         argDict = {'init_date': "09/1/2012",
#                     'precip': "kalman_data/precip_F2012-09-01_T2013-08-31.nc",
#                     'tmin': "kalman_data/tempmin_F2012-09-01_T2013-08-31.nc",
#                     'tmax': "kalman_data/tempmax_F2012-09-01_T2013-08-31.nc",
#                     'params': "kalman_data/wm_param_files_test.json",
#                     'network_file': "kalman_data/rivout.shp",
#                     'basin_shp': "kalman_data/subsout.shp",
#                     'timestep': "0",
#                     'instance': "0",
#                     'restart': False}
#
#         self.args = Args(argDict)
#
#         self.kf = Kalman_Filter(f,A,Q_param,Q_state,R, u_error, instances)
#         self.NUM_TIMESTEPS = 50
#
#     def _get_measurement_(self):
#         return 0
#
#     def _plot_kalman_instance_(self,myStuff,myGlobalPriori,myMeasured):
#         print myStuff
#         print myGlobalPriori
#         print myMeasured
#
#         # Plot graph!
#         t = range(0, self.NUM_TIMESTEPS)
#         plt.plot(t, myStuff, 'b', label='Kalman Postiori') # Kalman Postiori Values
#         plt.plot(t,myGlobalPriori, 'c--', label='Piori (Pre Correction)') # Model pre-correction (priori)
#         plt.plot(t,myMeasured, 'r--', label='Measurements') # Measurements
#         plt.fill_between(t, myMeasured, myGlobalPriori, facecolor = "orange")
#         plt.xlabel('time')
#         plt.ylabel('value')
#         plt.title('Simple Kalman Filter')
#         plt.grid(True)
#         plt.legend()
#         plt.show()
#
#     def test_run_model(self):
#         self.kf.run_model(self.args)
#
#     def test_set_measurement_function(self):
#         self.kf.set_measurement_function(self._get_measurement_)
#
#     def test_run_kalman_filter(self):
#
#         # Run filter
#         myStuff = np.array(self.kf.start_filter(self.NUM_TIMESTEPS/2))
#         # myStuff = np.array(self.kf.update_filter(self.NUM_TIMESTEPS/2))
#         # Get all data
#         # myMeasured = self.kf.get_measured()
#         # myGlobalPriori = self.kf.get_priori_global()
#         #
#         # # Format data
#         # myStuff.flatten()
#         # myStuff = [val for sublist in myStuff for val in sublist]
#         # myStuff = [val for sublist in myStuff for val in sublist]
#         #
#         # t = range(0, myStuff.size)
#         # plt.plot(t, myStuff, 'b', label='Priori') # Kalman Postiori Values
#         # plt.xlabel('time')
#         # plt.ylabel('value')
#         # plt.title('Simple Kalman Filter')
#         # plt.grid(True)
#         # plt.legend()
#         # plt.show()
#
#
#         # Print paramenters
#
#     def test_run_ensemble_kalman_filter(self):
#
#         # Run filter
#         self.kf.start_filter_timesteps()
#
#
# if __name__ == '__main__':
#     tkf = Test_Kalman_Filter()
#     tkf.setUp()
#     tkf.test_run_ensemble_kalman_filter()
