import nose
import numpy as np
import os

# ~~~~~~~~~~~
# testksystemutils.py
# Testing the ksystemutils functions in kalman/kalmanutils.
# Created 7/31/2018
# ~~~~~~~~~~~

from context import enter_folder, exit_folder, create_folder

class test_ksystemutils(object):
    def setUp(self):


        # Variables for testing prepare THIESSEN matrix
        self.test_folder = "test_data"

    def test_folder_navigation(self):
        init_loc = os.getcwd()
        enter_folder(self.test_folder)
        assert init_loc + "/" + self.test_folder == os.getcwd()
        exit_folder()
        assert init_loc == os.getcwd()
