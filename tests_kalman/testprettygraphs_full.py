import nose
import numpy as np
import os

# ~~~~~~~~~~~
# testksystemutils.py
# Testing the ksystemutils functions in kalman/kalmanutils.
# Created 7/31/2018
# ~~~~~~~~~~~

from context import cross_cov, cross_cov_sum, calculate_K_matrix

class test_prettygraphs_full(object):
    def setUp(self):



        # Variables for testing cross cov functions
        self.test_state_array = np.arange(10*50*15).reshape((10,50,15))
        self.test_H_mat = np.identity(50)

        # Variables for testing K matrix generation
        self.cov_state_no_err = np.array([[0, 0], [0, 0]])
        self.cov_prediction_i = np.array([[1, 0], [0, 1]])

    def test_simple_filter(self):
        print "NOT IMPLIMENTED YET"
