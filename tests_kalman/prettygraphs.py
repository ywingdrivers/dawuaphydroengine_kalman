import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import pickle, os, errno


def main(export_path = "pretty_plots", xpr_dict = [], xpo_dict = [], pr_cross_cov_state = [], dict_en = [], xpo_dict_param = [], pr_cross_cov_param = [], y_obs_perturbed = [], y_obs = [], innovation_arr = [], H = [], pre_innovation_arr = [], uvars_arr = [],  load_pickle = False):
    plt.style.use('seaborn-darkgrid')

    print "Beginning..."

    # Get pickeled arrays
    PATH_TO_FILES = "final_params/"
    EXPORT_PATH = export_path

    plt.switch_backend('agg')

    # Create directory if it doesn't exist
    try:
        os.makedirs(EXPORT_PATH)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    #########################
    ## LOAD PICKLED STATES ##
    #########################

    if load_pickle == True:
        xpr_dict = pickle.load(open(PATH_TO_FILES+"xpr_dict.pickled", "rb"))
        xpo_dict = pickle.load(open(PATH_TO_FILES+"xpo_dict.pickled", "rb"))
        pr_cross_cov_state = pickle.load(open(PATH_TO_FILES+"pr_cross_cov_state.pickled", "rb"))

        dict_en = pickle.load(open(PATH_TO_FILES+"dict_en.pickled", "rb"))
        xpo_dict_param = pickle.load(open(PATH_TO_FILES+"xpo_dict_param.pickled", "rb"))
        pr_cross_cov_param = pickle.load(open(PATH_TO_FILES+"pr_cross_cov_param.pickled", "rb"))


        y_obs_perturbed = pickle.load(open(PATH_TO_FILES+"y_obs_perturbed.pickled", "rb"))
        y_obs = pickle.load(open(PATH_TO_FILES+"yobs.pickled", "rb"))

        innovation_arr = pickle.load(open(PATH_TO_FILES+"innovation_arr.pickled", "rb"))

        H = pickle.load(open(PATH_TO_FILES+"H.pickled", "rb"))

    print "####Pickles Loaded#####\n"

    font = {'family' : 'normal',
            'weight' : 'bold',
            'size'   : 14}

    matplotlib.rc('font', **font)

    pre_states_mean = np.mean(xpr_dict, axis=0)
    states_mean = np.mean(xpo_dict, axis=0)
    reduced_cov_matrix = np.einsum('ik,kjl->ijl', H, pr_cross_cov_state)
    predict_unc = reduced_cov_matrix.diagonal().T

    innovation = innovation_arr

    #<<>>#
    # Overall Swe: Averaged (over ensembles and space) posterior SWE values compared with observed SWE and prior SWE over time
    #<<>>#

    plt.figure(figsize=(10,10))
    plt.plot()
    plt.plot(np.mean(states_mean, axis=0), label='estimated SWE')
    plt.plot(np.mean(pre_states_mean, axis=0), linestyle='dashed', label='prior estimated SWE')
    plt.plot(np.mean(y_obs, axis=0), label='observations')
    plt.plot(np.mean(np.mean(y_obs_perturbed, axis=0), axis=0), linestyle='dotted', label='perturbed observations')
    plt.fill_between(np.arange(states_mean[0,:].size), np.mean(states_mean, axis=0)+np.sqrt(np.mean(predict_unc, axis=0)), np.mean(states_mean, axis=0)-np.sqrt(np.mean(predict_unc, axis=0)),
                     facecolor='grey', label="68% confidence interval")
    plt.legend()
    plt.xlabel('Days starting at 9/1/2012')
    plt.ylabel('Snow Water Equivalent (mm)')
    plt.savefig(EXPORT_PATH +"/overallSWE")
    print "OVERALLSWE CREATED"

    #<<>>#
    # Measurement Swe: Averaged (over ensembles and space) posterior SWE values compared with observed SWE and prior SWE over time
    #<<>>#

    plt.figure(figsize=(10,10))
    plt.plot()
    plt.plot(np.mean(H.dot(states_mean), axis=0), label='estimated SWE (observation points)')
    plt.plot(np.mean(H.dot(pre_states_mean), axis=0), linestyle='dashed', label='prior estimated SWE (observation points)')
    plt.plot(np.mean(y_obs, axis=0), label='observations')
    plt.plot(np.mean(np.mean(y_obs_perturbed, axis=0), axis=0), linestyle='dotted', label='perturbed observations')
    plt.fill_between(np.arange(states_mean[0,:].size), np.mean(states_mean, axis=0)+np.sqrt(np.mean(predict_unc, axis=0)), np.mean(states_mean, axis=0)-np.sqrt(np.mean(predict_unc, axis=0)),
                     facecolor='grey', label="68% confidence interval")
    plt.legend()
    plt.xlabel('Days starting at 9/1/2012')
    plt.ylabel('Snow Water Equivalent (mm)')
    plt.savefig(EXPORT_PATH +"/measurementswe")
    print "MEASUREMENT SWE CREATED"



    #<<>>#
    # All Ensembles Swe: All ensembles posterior SWE over time, averaged over space
    #<<>>#

    ensembles_mean = np.mean(xpo_dict, axis=1)
    ensembles_mean_pri = np.mean(xpr_dict, axis=1)

    # plt.figure(figsize=(10,10))
    # for i in range(0, ensembles_mean.shape[0]):
    #     plt.plot(ensembles_mean[i], label='Ensemble ' + str(i))
    #     plt.plot(ensembles_mean_pri[i], linestyle='dashed', label='Ensemble' + str(i) + ' Prior')
    #
    # plt.legend()
    # plt.xlabel('Days starting at 9/1/2012')
    # plt.ylabel('Snow Water Equivalent (mm)')
    # plt.savefig(EXPORT_PATH +"/allCorrectedSWE")
    # print "allCorrectedSWE CREATED"

    f, ax = plt.subplots(1, 2, figsize=(20, 10))

    ax.flat[0].plot(np.mean(y_obs, axis=0), color='red', linestyle="dotted", label='Observations')
    ax.flat[1].plot(np.mean(y_obs, axis=0), color='red', label='Observations')

    for i in range(0, ensembles_mean.shape[0]):
        ax.flat[0].plot(np.mean(H.dot(xpo_dict[i]), axis=0), label='Ensemble ' + str(i))
        ax.flat[1].plot(np.mean(H.dot(xpr_dict[i]), axis=0), linestyle='dashed', label='Ensemble' + str(i) + ' Prior')

    ax.flat[0].legend()
    ax.flat[1].legend()
    ax.flat[0].set_xlabel('Days starting at 9/1/2012')
    ax.flat[1].set_xlabel('Days starting at 9/1/2012')
    ax.flat[0].set_ylabel('Snow Water Equivalent (mm)')
    ax.flat[1].set_ylabel('Snow Water Equivalent (mm)')
    plt.savefig(EXPORT_PATH +"/allCorrectedSWE")
    print "ALLENSEMBLESSWE CREATED"

    #<<>>#
    # Only Ensemble 0 SWE: SWE estemation in ensemble 0 only
    #<<>>#

    plt.figure(figsize=(10,10))
    plt.plot(np.mean(H.dot(xpo_dict[0]), axis=0), label='Ensemble 0')
    plt.plot(np.mean(H.dot(xpo_dict[0]), axis=0), linestyle='dashed', label='Ensemble 0 Prior')
    plt.plot(np.mean(y_obs_perturbed[:,0,:], axis=0), label='Perturbed Observations')
    plt.plot(np.mean(y_obs, axis=0), linestyle="dotted", label="Observations")

    plt.legend()
    plt.xlabel('Days starting at 9/1/2012')
    plt.ylabel('Snow Water Equivalent (mm)')
    plt.savefig(EXPORT_PATH +"/onlyEnsemble0SWE")

    print "ONLYENSEMBLE0SWE CREATED"

    #<<>>#
    # Only Position 0 Corrected SWE: Only SWE at position 0 posterior SWE over time, averaged over space
    #<<>>#

    plt.figure(figsize=(10,10))
    plt.plot()
    plt.plot(H.dot(states_mean)[0,:], label='estimated SWE')
    plt.plot(H.dot(pre_states_mean)[0,:], linestyle='dashed', label='prior estimated SWE')
    plt.plot(np.mean(y_obs_perturbed, axis=1)[0,:], label='perturbed observations')
    plt.plot(y_obs[0,:], linestyle='dotted', label='true observations')


    plt.legend()
    plt.xlabel('Days starting at 9/1/2012')
    plt.ylabel('Snow Water Equivalent (mm)')
    plt.savefig(EXPORT_PATH +"/onlyNode0SWE")
    print "ONLYNODE0SWE CREATED"

    #<<>>#
    # Only Position 40 Corrected SWE: Same as above, different position
    #<<>>#

    plt.figure(figsize=(10,10))
    plt.plot()
    plt.plot(H.dot(states_mean)[40,:], label='estimated SWE')
    plt.plot(H.dot(pre_states_mean)[40,:], linestyle='dashed', label='prior estimated SWE')
    plt.plot(np.mean(y_obs_perturbed, axis=1)[40,:], label='perturbed observations')
    plt.plot(y_obs[40,:], linestyle='dotted', label='true observations')

    plt.legend()
    plt.xlabel('Days starting at 9/1/2012')
    plt.ylabel('Snow Water Equivalent (mm)')
    plt.savefig(EXPORT_PATH +"/onlyNode40SWE")
    print "ONLYNODE40SWE CREATED"

    #<<>>#
    # Innovation: Observation minus prior, averaged over all ensembles
    #<<>>#


    plt.figure()
    plt.plot(np.mean(np.mean(pre_innovation_arr,axis=0), axis=0), color="red", label="during parameter correction")
    plt.plot(np.mean(np.mean(innovation_arr,axis=0), axis=0), color="blue", linestyle='dashed', label="during state correction")
    plt.ylabel('innovation (mm)')
    plt.legend()
    plt.savefig(EXPORT_PATH +"/innovation")
    print "INNOVATION CREATED"

    #<<>>#
    # Innovation Ensemble 0: Observation minus prior for ensemble 0
    #<<>>#


    plt.figure()
    plt.plot(np.mean(innovation_arr,axis=0)[0,:], color="blue", linestyle='dashed', label="during state correction")
    plt.plot(np.mean(pre_innovation_arr, axis=0)[0,:], color="red", label="during parameter correction")
    plt.ylabel('innovation (mm)')
    plt.legend()
    plt.savefig(EXPORT_PATH +"/innovationens0")
    print "INNOVATIONENS0 CREATED"



    #<<>>#
    # Precipitation: The variance in precipitation per ensemble
    #<<>>#



    nan_uvars = uvars_arr

    nan_uvars[nan_uvars < -1000] = np.nan

    this_array = nan_uvars[:, :, :, 0]
    plt.figure()
    for i in range(uvars_arr.shape[0]):
        plt.plot(np.nanmean(this_array,axis=1)[i,:], label="E "+ str(i))
    plt.ylabel('precipitation')
    plt.legend()
    plt.savefig(EXPORT_PATH +"/precip")
    print "PRECIP CREATED"

    #<<>>#
    # Mintemp: The min temp
    #<<>>#

    this_array = nan_uvars[:, :, :, 1]

    plt.figure()
    for i in range(uvars_arr.shape[0]):
        plt.plot(np.nanmean(this_array,axis=1)[i,:], label="E "+ str(i))
    plt.ylabel('Min Temp in Kelvin')
    plt.savefig(EXPORT_PATH +"/mintemp")
    print "MINTEMP CREATED"

    #<<>>#
    # Maxtemp: The max temp
    #<<>>#

    this_array = nan_uvars[:, :, :, 2]

    plt.figure()
    for i in range(uvars_arr.shape[0]):
        plt.plot(np.nanmean(this_array,axis=1)[i,:], label="E "+ str(i))
    plt.ylabel('Min Temp in Kelvin')
    plt.savefig(EXPORT_PATH +"/maxtemp")
    print "MAXTEMP CREATED"



    font = {'family' : 'normal',
            'weight' : 'bold',
            'size'   : 14}

    matplotlib.rc('font', **font)

    pre_states_mean = H.dot(np.mean(dict_en, axis=0))[:,1:]
    states_mean = np.mean(xpo_dict_param, axis=0)[:,1:]
    predict_unc = pr_cross_cov_param.diagonal().T

    plt.figure(figsize=(10,10))
    plt.plot()
    plt.plot(np.mean(states_mean, axis=0), label='corrected DDF')
    plt.fill_between(np.arange(states_mean[0,:].size), np.mean(states_mean, axis=0)+np.sqrt(np.mean(predict_unc, axis=0)), np.mean(states_mean, axis=0)-np.sqrt(np.mean(predict_unc, axis=0)),
                     facecolor='grey', label="68% confidence interval")
    plt.plot(np.mean(pre_states_mean, axis=0), label='sampled DDF', linestyle='dashed')
    plt.legend()
    plt.xlabel('Days starting at 9/1/2012')
    plt.ylabel('DDF')
    plt.savefig(EXPORT_PATH +"/overallDDF")
    print "OVERALLDDF CREATED"


    #<<>>#
    # Only Position 0 Corrected DDF: Only DDF at position 0 posterior DDF over time, averaged over space
    #<<>>#

    plt.figure(figsize=(10,10))
    plt.plot()
    plt.plot(states_mean[0,:], label='estimated DDF')
    plt.plot(pre_states_mean[0,:], linestyle='dashed', label='prior estimated DDF')
    # plt.plot(np.mean(y_obs_perturbed, axis=1)[0,:], label='perturbed observations')
    # plt.plot(y_obs[0,:], linestyle='dotted', label='true observations')


    plt.legend()
    plt.xlabel('Days starting at 9/1/2012')
    plt.ylabel('DDF')
    plt.savefig(EXPORT_PATH +"/onlyNode0DDF")
    print "ONLYNODE0DDF CREATED"

    #<<>>#
    # Only Position 40 Corrected DDF: Only DDF at position 0 posterior DDF over time, averaged over space
    #<<>>#

    plt.figure(figsize=(10,10))
    plt.plot()
    plt.plot(states_mean[40,:], label='estimated DDF')
    plt.plot(pre_states_mean[40,:], linestyle='dashed', label='prior estimated DDF')
    # plt.plot(np.mean(y_obs_perturbed, axis=1)[0,:], label='perturbed observations')
    # plt.plot(y_obs[0,:], linestyle='dotted', label='true observations')


    plt.legend()
    plt.xlabel('Days starting at 9/1/2012')
    plt.ylabel('DDF')
    plt.savefig(EXPORT_PATH +"/onlyNode40DDF")
    print "ONLYNODE40DDF CREATED"

    ensembles_mean = np.mean(xpo_dict_param, axis=1)
    ensembles_mean_pri = np.mean(dict_en, axis=1)

    f, ax = plt.subplots(1, 2, figsize=(20, 10))

    # ax.flat[0].plot(np.mean(y_obs, axis=0), color='red', linestyle="dotted", label='Observations')
    # ax.flat[1].plot(np.mean(y_obs, axis=0), color='red', label='Observations')

    for i in range(0, ensembles_mean.shape[0]):
        ax.flat[0].plot(ensembles_mean[i], label='DDF Ensemble ' + str(i))
        ax.flat[1].plot(ensembles_mean_pri[i], linestyle='dashed', label='DDF Ensemble' + str(i) + ' Prior')

    ax.flat[0].legend()
    ax.flat[1].legend()
    ax.flat[0].set_xlabel('Days starting at 9/1/2012')
    ax.flat[1].set_xlabel('Days starting at 9/1/2012')
    ax.flat[0].set_ylabel('DDF - Corrected')
    ax.flat[1].set_ylabel('DDF - Sampled')
    plt.savefig(EXPORT_PATH +"/allDDF")


    print "ALLDDF CREATED"


    font = {'family' : 'normal',
            'weight' : 'bold',
            'size'   : 18}
    pre_states_mean_H = pre_states_mean
    matplotlib.rc('font', **font)

    label = "DDF Nodes "

    f, ax = plt.subplots(5,2,figsize=(20,20))

    for i in range(pre_states_mean_H.shape[0]/10):
        begin = i * 10
        end = i * 10 + 10

        all_days = pre_states_mean_H[begin:end,:]
        for j in range(all_days.shape[0]):
            ax.flat[i].plot(all_days[j, :], label = "Node " + str(j))
        ax.flat[i].set_ylabel('Nodes ' + str(begin) + ' to ' + str(end))
        ax.flat[i].set_xlabel('Days starting at 9/1/2012')

    #ax.flat[0].legend()
    f.savefig(EXPORT_PATH +"/DFFnodes")
    print "DDFNODES CREATED"

if __name__ == '__main__':
    main()