import nose
import numpy as np
import os

# ~~~~~~~~~~~
# testksystemutils.py
# Testing the ksystemutils functions in kalman/kalmanutils.
# Created 7/31/2018
# ~~~~~~~~~~~

from context import cross_cov, cross_cov_sum, calculate_K_matrix

class test_klinalgutils(object):
    def setUp(self):


        # Variables for testing cross cov functions
        self.test_state_mat = np.random.normal(np.arange(10*20).reshape((10,20)), 10)
        self.test_H_mat = np.array([[0, 0, 1, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 1, 0, 0]])

        # Variables for testing K matrix generation
        self.cov_state_no_err = np.array([[0, 0], [0, 0]])
        self.cov_prediction_i = np.array([[1, 0], [0, 1]])

    def test_cross_cov(self):

        # Test against standard cov function
        cov_mat = cross_cov(self.test_H_mat.dot(self.test_state_mat).T, self.test_H_mat.dot(self.test_state_mat).T)
        cov_mat_np = np.cov(self.test_state_mat)

        np.testing.assert_array_almost_equal(self.test_H_mat.dot(cov_mat_np).dot(self.test_H_mat.T), cov_mat)

    def test_cross_cov_sum(self):

        # Test against standard cov function
        cov_mat = cross_cov_sum(self.test_H_mat.dot(self.test_state_mat).T, self.test_H_mat.dot(self.test_state_mat).T)
        cov_mat_np = np.cov(self.test_state_mat)

        np.testing.assert_array_almost_equal(self.test_H_mat.dot(cov_mat_np).dot(self.test_H_mat.T), cov_mat)

    def test_cross_cov(self):

        # Test against standard cov function
        cov_mat = cross_cov(self.test_H_mat.dot(self.test_state_mat).T, self.test_H_mat.dot(self.test_state_mat).T)
        cov_mat_np = np.cov(self.test_state_mat)

        np.testing.assert_array_almost_equal(self.test_H_mat.dot(cov_mat_np).dot(self.test_H_mat.T), cov_mat)

    def test_calculate_K_matrix(self):

        K = calculate_K_matrix(self.cov_state_no_err, self.cov_prediction_i, .25)
        assert K.min() <= 0
        assert K.max() <= .00001

        K = calculate_K_matrix(self.cov_prediction_i, self.cov_prediction_i, .000001)

        assert K.min() <= 1.0001
        assert K.max() <= 9.9999

