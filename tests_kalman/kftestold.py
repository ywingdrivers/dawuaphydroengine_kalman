import sys, os
import matplotlib.pyplot as plt
import numpy as np
from context import Kalman_Filter, Args, utils, hydrovehicle
import argparse
import prettygraphs

# Make sure we're useing the correct python and path!
print(sys.version)
print(os.path.dirname(sys.executable))


class Test_Kalman_Filter(object):
    def setUp(self, init_date, instances=10, timesteps=365, R = .1/0.039370078740158, Q_param = .3, u_error=8., generate_pickles=False):
        # Varaibles
        f = hydrovehicle.main
        A = np.array([[2,-1]])
        #Q_param = 2 # parameter error variance
        Q_state = 25 # State error variance
        #R = .1/0.039370078740158 # inches to mm # Observation error
        #R = 0.1 # For development
        #u_error = 2 # U params error
        # instances = 90
        #
        # argDict = {'init_date': "09/1/2012",
        #             'precip': "kalman_data/precip_F2012-09-01_T2013-08-31.nc",
        #             'tmin': "kalman_data/tempmin_F2012-09-01_T2013-08-31.nc",
        #             'tmax': "kalman_data/tempmax_F2012-09-01_T2013-08-31.nc",
        #             'params': "kalman_data/wm_param_files_test.json",
        #             'network_file': "kalman_data/rivout.shp",
        #             'basin_shp': "kalman_data/subsout.shp",z
        #             'timestep': "0",
        #             'instance': "0",
        #             'restart': False}
        #
        # self.args = Args(argDict)
        print "##############################"
        print "Setting Up Kalman Filter!"
        print "R: \t\t\t\t\t\t", R
        print "Q_param: \t\t\t\t", Q_param
        print "u_error: \t\t\t\t", u_error
        print "Ensemble Instances: \t", instances
        print "Timesteps: \t\t\t\t", timesteps
        print "Starting: \t\t\t\t", init_date
        print "##############################\n\n"

        self.kf = Kalman_Filter(f,Q_param,R, u_error, instances, timesteps, init_date, generate_pickles=generate_pickles)

    def _get_measurement_(self):
        return 0

    def _plot_kalman_instance_(self,myStuff,myGlobalPriori,myMeasured):
        print myStuff
        print myGlobalPriori
        print myMeasured

        # Plot graph!
        t = range(0, self.NUM_TIMESTEPS)
        plt.plot(t, myStuff, 'b', label='Kalman Postiori') # Kalman Postiori Values
        plt.plot(t,myGlobalPriori, 'c--', label='Piori (Pre Correction)') # Model pre-correction (priori)
        plt.plot(t,myMeasured, 'r--', label='Measurements') # Measurements
        plt.fill_between(t, myMeasured, myGlobalPriori, facecolor = "orange")
        plt.xlabel('time')
        plt.ylabel('value')
        plt.title('Simple Kalman Filter')
        plt.grid(True)
        plt.legend()
        plt.show()

    def test_run_dev(self):

        tkf.test_run_ensemble_kalman_filter()

    def test_run_ensemble_kalman_filter(self):

        # Run filter
        return self.kf.start_filter_timesteps()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test kalman filter')
    parser.add_argument('--dev', dest='dev', action='store_true')
    parser.add_argument('--generate_pickles', action='store_true', dest="generate_pickles")
    parser.add_argument('-t', action="store", dest="ts", type=int, default=365)
    parser.add_argument('-i', action="store", dest="ins", type=int, default=10)
    parser.add_argument('-r', action="store", dest="r", type=float, default=.1/0.039370078740158)
    parser.add_argument('-qparam', action="store", dest="qparam", type=float, default=.3)
    parser.add_argument('-u', action="store", dest="u_error", type=float, default=8.)
    parser.add_argument('-initdate', action ="store", dest="initdate", default="9/1/2012")

    args = parser.parse_args()

    print args.dev

    if args.dev == True:
        print "THIS"
        if args.ts == 365:
            ts = 30 # Change default to the
        else:
            ts = args.ts

        tkf = Test_Kalman_Filter()
        tkf.setUp("12/25/2012", timesteps=ts, instances=args.ins, R=args.r, Q_param=args.qparam, u_error=args.u_error, generate_pickles = args.generate_pickles)
        xpr_dict, xpo_dict, pr_cross_cov_state, dict_en, xpo_dict_param, pr_cross_cov_param, y_obs_perturbed, y_obs, innovation_arr, H, pre_innovation_arr, uvars_arr = tkf.test_run_ensemble_kalman_filter()

    else:

        tkf = Test_Kalman_Filter()
        tkf.setUp(args.initdate, timesteps=args.ts, instances=args.ins, R=args.r, Q_param=args.qparam, u_error=args.u_error, generate_pickles = args.generate_pickles)
        xpr_dict, xpo_dict, pr_cross_cov_state, dict_en, xpo_dict_param, pr_cross_cov_param, y_obs_perturbed, y_obs, innovation_arr, H, pre_innovation_arr, uvars_arr = tkf.test_run_ensemble_kalman_filter()

    # Create graphs!
    prettygraphs.main("pretty_plots", xpr_dict, xpo_dict, pr_cross_cov_state, dict_en, xpo_dict_param, pr_cross_cov_param, y_obs_perturbed, y_obs, innovation_arr, H, pre_innovation_arr, uvars_arr)