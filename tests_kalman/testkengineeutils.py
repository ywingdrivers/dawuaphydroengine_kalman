import nose
import numpy as np

# ~~~~~~~~~~~
# testkengineutils.py
# Testing the kengineutils functions in kalman/kalmanutils.
# Created 7/31/2018
# ~~~~~~~~~~~

from context import add_noise_to_array, add_noise_to_array_instances, add_scalar_to_array

class test_kengineutils(object):
    def setUp(self):


        # Variables for testing prepare THIESSEN matrix
        self.test_noise_matrix = np.array([0,1,2,3])


    def test_add_noise_to_array(self):

        noise_matrix = add_noise_to_array(np.random.normal, self.test_noise_matrix, filter_unknowns=2, clip=1, scale=.5)

        # Test filter_unknowns
        np.testing.assert_array_equal(noise_matrix[2], self.test_noise_matrix[2])

        # Test clipping
        assert noise_matrix.min() >= 1


    def test_add_noise_to_array_instances(self):

        noise_matrix = add_noise_to_array_instances(np.random.normal, self.test_noise_matrix, 5, filter_unknowns=3, clip=2, scale=.5)

        # Test shape
        assert noise_matrix.shape == (self.test_noise_matrix.shape[0],5)

        # Test filter_unknowns
        np.testing.assert_array_equal(noise_matrix[3,1], self.test_noise_matrix[3])

        # Test clipping
        assert noise_matrix.min() >= 2

    def test_add_scalar_to_array(self):

        noise_matrix = add_scalar_to_array(np.random.normal, self.test_noise_matrix, clip=1, filter_unknowns=2, scale=.25)

        difference = noise_matrix[3] - self.test_noise_matrix[3]

        # Make sure the noise is uniform
        assert difference == noise_matrix[2] - self.test_noise_matrix[2]

        # Test filter_unknowns
        np.testing.assert_array_equal(noise_matrix[2], self.test_noise_matrix[2])

        # Test clipping
        assert noise_matrix.min() >= 1