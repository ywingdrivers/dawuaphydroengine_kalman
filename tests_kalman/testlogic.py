import nose
import numpy as np

# ~~~~~~~~~~~
# testksystemutils.py
# Testing the ksystemutils functions in kalman/kalmanutils.
# Created 7/31/2018
# ~~~~~~~~~~~

class test_logic(object):
    def setUp(self):


        # Variables for testing ddf filter activation logic
        self.zeros_array = np.zeros(10)
        self.ones_array = np.ones(10)

    def test_logic_ddf_filter_activation(self):
        # Goal - turn off DDF filter unless EVERY node is melting.

        # Old code
        # ((obs_dict["swe"][t, :] - obs_dict["swe"][t-1, :]) > 0).all():

        # ZEROS ARE CURRENT TIMESTEP (ALL NODES MELT)
        assert ((self.zeros_array - self.ones_array).max() >= 0) == False

        # ZEROS ARE PAST TIMESTEP (ALL NODES GAIN)"
        assert ((self.ones_array - self.zeros_array).max() >= 0) == True

        # ALL BUT ONE NODE NODE MELTS
        self.zeros_array[2] = 2
        assert ((self.zeros_array - self.ones_array).max() >= 0) == True

        # ALL BUT ONE NODE NODE GAINS
        self.zeros_array[2] = 0
        self.ones_array[2] = -1
        assert ((self.ones_array - self.zeros_array).max() >= 0) == True

        # NODES ARE EQUAL
        assert ((self.zeros_array - self.zeros_array).max() >= 0) == True

        # Winner:
        # ((obs_dict["swe"][t, :] - obs_dict["swe"][t-1, :]).max() >= 0):

    def test_logic_ddf_filter_activation_2(self):
        # Goal - turn off DDF filter unless ONE node is melting.

        # Old code
        # ((obs_dict["swe"][t, :] - obs_dict["swe"][t-1, :]) > 0).all():

        # ZEROS ARE CURRENT TIMESTEP (ALL NODES MELT)
        assert ((self.zeros_array - self.ones_array) >= 0).all() == False

        # ZEROS ARE PAST TIMESTEP (ALL NODES GAIN)"
        assert ((self.ones_array - self.zeros_array) >= 0).all() == True

        # ALL BUT ONE NODE NODE MELTS
        self.zeros_array[2] = 2
        assert ((self.zeros_array - self.ones_array) >= 0).all() == False

        # ALL BUT ONE NODE NODE GAINS
        self.zeros_array[2] = 0
        self.ones_array[2] = -1
        assert ((self.ones_array - self.zeros_array) >= 0).all() == False

        # NODES ARE EQUAL
        assert ((self.zeros_array - self.zeros_array) >= 0).all() == True

        # Winner:
        # ((obs_dict["swe"][t, :] - obs_dict["swe"][t-1, :]) >= 0).all():