import nose
import numpy as np
import math

# ~~~~~~~~~~~
# testksystemutils.py
# Testing the ksystemutils functions in kalman/kalmanutils.
# Created 7/31/2018
# ~~~~~~~~~~~

from context import cross_cov, cross_cov_sum, calculate_K_matrix, Dual_Ensemble_Kalman_Filter, Args, update_dict, main_prettygraphs, graph_initialization_full, enter_folder, exit_folder

class test_simplefilter(object):
    def setUp(self):


        # Setup instance numbers, timesteps, correct_param
        self.TIMESTEPS = 100
        self.INSTANCES = 100
        self.CORRECT_PARAM = np.array([4,1])
        self.tparam_list = self.CORRECT_PARAM

        self.NOISE_PARAM = 1.5 # Amount of noise to place on observations
        self.WRONG_PARAM = np.array([3,3]) # The initial parameters before correction

        #Setup uparams
        self.uparams = np.arange(self.TIMESTEPS)/3.2 # The uparams
        self.uparam_list = np.zeros(self.INSTANCES) # Holds current UPARAMS

        # Variables for model
        self.initial_state = np.zeros(10)
        self.test_state_mat = np.random.normal(np.arange(10*2).reshape((10,2)), 10)
        #self.test_H_mat = np.array([[0, 0, 1, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 1, 0, 0]])
        self.state_list = np.tile(self.initial_state,(self.INSTANCES,1)) # Can be manipulated by getters/setters

        # Set self.tparam_list to the correct value
        self.tparam_list = np.tile(self.CORRECT_PARAM,(self.INSTANCES, 1))

        # Setup actual correct state data by running model with 'correct' parameter
        self.correct_info = np.zeros((10,self.TIMESTEPS))
        for t in range(self.TIMESTEPS):
            if t == 0:
                self.correct_info[:, t] = 0
            else:
                self._test_model_(Args({"i":0}))
                self.correct_info[:,t] = self.state_list[0, :]


        # Observations - noise added to real info
        self.observations = np.random.normal(self.correct_info, self.NOISE_PARAM)

        # Set self.tparam_list to an incorrect value
        self.tparam_list = np.tile(self.WRONG_PARAM,(self.INSTANCES, 1))

        # Parameter dictionaries (initial ones for KF initialization)
        self.arguments = {}
        self.tparams_dict = {"test_param": self.WRONG_PARAM}
        self.uparams_dict = {"uparams": self.uparams}
        self.init_obs_dict = {"test_state": self.observations[:,0]}
        self.init_state_dict = {"test_state": np.zeros(10)}
        self.H_dict = {"test_state": np.identity(10)}

    def run_filter_tests(self):
        name_list = []
        result_list = []
        for u in range(1,5):
            for q in range(2,6):
                print "Beginning Q" + str(q) + "U" + str(u)
                name_list.append([u,q])
                result_list.append(self._test_simple_filter_(u,q, graph_results=False))
        result_list = np.array(result_list)
        name_list = np.array(name_list)
        print "BEST RESULT"
        winner = min(result_list, key=lambda x: abs((x[0] - self.CORRECT_PARAM[0]) + (x[1] - self.CORRECT_PARAM[1])))
        print winner
        print "U"
        print name_list[np.where(np.all(result_list==winner,axis=1))][:,0]
        print "Q"
        print name_list[np.where(np.all(result_list==winner,axis=1))][:,1]

    def test_simple_filter(self):
        self._test_simple_filter_(2.5,3.5, graph_results=True)

    def _test_simple_filter_(self,u_par, q_par, r_par = 1.5, graph_results = False):
        # Set up our Dual Ensemble Kalman Filter
        self.testdekf = Dual_Ensemble_Kalman_Filter(self._test_model_,self.arguments,self.tparams_dict,
                                               self.uparams_dict, self.init_obs_dict, self.init_state_dict,
                                               self.H_dict, {"test_param": ["test_state"]}, Q_errors={"test_param": q_par},
                                               R_errors={"test_state": r_par}, U_errors={"uparams": u_par}, run_in_this_folder="simpledekf", send_instance_number_to_model=True, instances=self.INSTANCES)

        # Set up our getters and setters.
        self.testdekf.set_state_getter(self._get_state_, "test_state")
        self.testdekf.set_uparam_modifyer(self._set_uparams_, "uparams")
        self.testdekf.set_tparam_modifyer(self._set_tparams_, "test_param")
        self.testdekf.set_state_modifyer(self._set_state_, "test_state")

        # Prepare members
        self.testdekf.prepare_members()



        # Graph things
        pri_t, post_t, pri_s, post_s, innov_pr, innov_st, pred_cov = graph_initialization_full(self.INSTANCES, self.TIMESTEPS, self.tparams_dict, self.init_state_dict, self.init_obs_dict)

        # Kalman loop
        for t in range(self.TIMESTEPS):


            print "Beginning ts " + str(t)
            cur_uparam_dict = {"uparams": np.array(self.uparams[t])}
            cur_obs_dict = {"test_state": np.array(self.observations[:,t], copy=True)}

            self.testdekf.simulate(uparam_dict=cur_uparam_dict) # Simulate step
            self.testdekf.update(obs_dict=cur_obs_dict, state_filter_active=False) # Update step

            # Obtain info
            cur_pri_t, cur_post_t, cur_pri_s, cur_post_s, cur_innov_pr, cur_innov_st = self.testdekf.get_state_and_param_info()
            cur_pred_cov = self.testdekf.get_pred_cov_info()

            # Update each graph dictionary
            pri_t = update_dict(cur_pri_t, pri_t, t)
            post_t = update_dict(cur_post_t, post_t, t)
            pri_s = update_dict(cur_pri_s, pri_s, t)
            post_s = update_dict(cur_post_s, post_s, t)
            innov_pr = update_dict(cur_innov_pr, innov_pr, t)
            innov_st = update_dict(cur_innov_st, innov_st, t)
            pred_cov = update_dict(cur_pred_cov, pred_cov, t)

        if graph_results:
            print "CORRECT PARAMETERS:"
            print self.CORRECT_PARAM

            print "GUESSED PARAMETERS:"
            print cur_post_t["test_param"].mean(axis=0)
            # Call graphing function
            enter_folder("testgraphs-no_state_corr")
            main_prettygraphs(pri_t, post_t, pri_s, post_s, innov_pr, innov_st, {"test_state":self.observations.T}, self.H_dict, pred_cov, xlabel_time='Timesteps', ylabels={"test_state": "Test State", "test_param": "Test Param"}, plot_keys=[["test_state", "test_param"]], export_path="prettygraphs_test_simple_filterQ" + str(q_par) + "U" + str(u_par), nodes_to_plot=[0,1])
            exit_folder()

        return cur_post_t["test_param"].mean(axis=0)


    def _test_model_(self, args):
        i = args.i
        self.state_list[i, :] = (np.sin(self.state_list[i,:].reshape(5,2) ) + (5 * self.tparam_list[i,:]) - self.uparam_list[i]).flatten()

    def _set_state_(self, arr, i):
        self.state_list[i,:] = arr

    def _get_state_(self, i):
        return self.state_list[i,:]

    def _set_uparams_(self, arr, i):
        self.uparam_list[i] = arr

    def _set_tparams_(self, arr, i):
        self.tparam_list[i,:] = arr

if __name__ == '__main__':

    testkf = test_simplefilter()
    testkf.setUp()
    testkf.test_simple_filter()

