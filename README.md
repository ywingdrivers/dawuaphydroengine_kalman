# README #

### Summary ###

**dawuaphydroengine_kalman** is a Dual State Ensemble Kalman Filter built to
operate on the **dawuaphydroengine** project.

*Current Version: 0.9.1 - planned improvements are at the bottom of this readme!*

#### What's New ####

#####0.9.1 #####
+ Small parameter tweaks - in particlar, hyper-parameters a and h have better default values.

#####0.9.0 #####
+ First official commit in a while - code is tested and working, though more testing is still being done.
+ Many improvements, including...
    + Masking matrices now are used to deactivate different observation points.
    + interpolated parameter rasters
    + A lot of things
+ Updated libraries
    + in particular no longer uses RasterIO < 1.0.
+ Refactored code - in particular kalmanvehicle is more generalized.

#####0.5.0 #####
+ Complete overhaul of the code - folder structure, variable names, etc.
    + Most notably, the functionality of the previous *hydroKalmanFilter.py* has been seperated into three modules: kalmanengine, kalmanvehicle, and kalmanutils.
        + **kalmanvehicle** - Contains a Dual Ensemble Kalman Filter class
        + **kalmanengine** - Contains an implimentation of the Dual Ensemble Kalman Filter for **dawuaphydroengine**
        + **kalmanutils** - Contains utilities for the **kalmanengine** module and also utilized by **kalmanvehicle**.
+ **Streamflow** has been added as a state (currently witout a parameter) to test the new modular functions of the filter.
+ Test functions have been created for **kalmanutils** functions.
+ Blank observation values (stored as np.nan's)  are now handled by the filter. They are removed from the update calculations and both the H_dict and covariance arrays are duly modified.

#####0.1.5 #####
+ Added confidence intervals to state graphs.

#####0.1.2 #####
+ Major bug fixed where parameter update function calculated innovation incorrectly.

#####0.1.1 #####
+ Major bug fixed where parameter cov. matrix was not being created correctly.
+ GIT clean up - **kalman_scripts** merged with master, **develop** is clone of original repository, other branches deleted.

#####0.1.0 #####
+ Added README
+ Ensemble Kalman Filter is now operational.

### Dependencies ###

1. Python 2.7.*x*
2. Lots O Libraries (will write this out soon)

### Setup ###

1. Download this repository
2. *Coming soon!*


### Run - There are now three different ways to run this program!###

1. Open a terminal and navigate to the tests_kalman directory.
2. Run kftest.py

### Troubleshooting ###
+ *Coming soon!*


### Upcoming Changes ###

##### Top Priority #####
+ Make improvements to DSEnKF design.
    + Smoothing will take time into consideration
    + Covarience changes (?)
    + Code efficiency

##### Medium Priority #####
+ Refactor nosetests (right now it's dated and broken)
+ Finish debug mode on kftests.py
+ Fix cython

##### Low Priority #####

+ Add functionality to HydroModelBrowser to show Kalman results.

### Contact ###

*Contact William Cook at william.cook@umontana.edu with any questions you may have*
